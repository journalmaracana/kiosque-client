import React, { useState, useEffect } from "react";

import {
  Button,
  Input,
  Col,
  Row,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

import {
  FaFacebookF,
  FaYoutube,
  FaInstagram,
  FaTwitter,
  FaEnvelope,
} from "react-icons/fa";
import { RiMapPin5Line } from "react-icons/ri";
import { FiSend } from "react-icons/fi";
import Link from "next/link";
import ArticlePreviewSmall from "../articlesPreview/smallArticle";
import axios from "axios";
import { API_LINK } from "../../utils/constant";
import { calculdatePublié } from "../../utils/functions";
import Preloader from "../ui/Prealoder";

import "./Sidebar.module.css";

export const Testimony = (props) => {
  return (
    <div className="shadow testimony mt-5 rounded-15 bg-white">
      <div className="testimony-title bg-blue text-center text-white p-2">
        <b>{"sidebar.testimony.title"}</b>
      </div>
      <div className="testimony-description p-2 text-blue">
        <b>{"sidebar.testimony.description"}</b>
      </div>
      <div className="testimony-button text-right p-2">
        <Button type="button" className="btn-sm bg-blue rounded-15 border-0">
          {"sidebar.testimony.button"}
        </Button>
      </div>
    </div>
  );
};

export const PharmaRegister = (props) => {
  return (
    <Link href="/register/pharmacien">
      <div className="shadow pharma-register mt-5 rounded-15 overflow-hidden bg-white">
        <h4 className="text-center text-blue py-3 mb-0">
          {" "}
          {"sidebar.pharmacien"}{" "}
        </h4>
        <div className="pharma-image">
          <img alt="pharma" src={"/images/slide2.jpg"} width="100%" />
        </div>
        <div className="bg-blue">
          <h4 className="text-center text-white mb-0 py-3">
            {" "}
            {"sidebar.pharmacien.register"}{" "}
          </h4>
        </div>
      </div>
    </Link>
  );
};

export const Newsletter = (props) => {
  return (
    <div className="shadow bg-darkblue sidebar-newsletter text-center text-white mt-5 p-3">
      <h4> {"sidebar.newsletter.title"} </h4>
      <p> {"sidebar.newsletter.description"} </p>
      <div className="newsletter-input">
        <Input type="email" name="nemail" placeholder="example@pharmacie.com" />
      </div>
      <div className="newsletter-button mt-2">
        <Button className="bg-blue rounded-pill border-0">
          {" "}
          {"buttons.validate"}{" "}
        </Button>
      </div>
    </div>
  );
};

export const Followus = (props) => {
  return (
    <div className="bg-blue sidebar-followus mt-1 p-3">
      <h4 className="text-center text-white"> {"sidebar.followus.title"} </h4>
      <div className="d-flex sidebar-followus-icons">
        <div className="head-icon rounded-pill m-2">
          <a href="https://facebook.com">
            <FaFacebookF />
          </a>
        </div>
        <div className="head-icon rounded-pill m-2">
          <a href="https://facebook.com">
            <FaYoutube />
          </a>
        </div>
        <div className="head-icon rounded-pill m-2">
          <a href="https://facebook.com">
            <FaInstagram />
          </a>
        </div>
        <div className="head-icon rounded-pill m-2">
          <a href="https://facebook.com">
            <FaTwitter />
          </a>
        </div>
        <div className="head-icon rounded-pill m-2">
          <a href="https://facebook.com">
            <FaEnvelope />
          </a>
        </div>
      </div>
    </div>
  );
};

export const BloodView = (props) => {
  return (
    <div className="blood-view shadow p-2 rounded-15 bg-white mb-3">
      <Row>
        <Col xs="3">
          <h1 className="text-center text-blue"> AB+ </h1>
        </Col>
        <Col xs="9" className="text-center">
          <h6 className="text-center text-blue mb-0">
            Malade Abdelmoumen salma
          </h6>
          <p className="text-muted text-center mb-0">
            Hospital beni messouse - Alger
          </p>
          <a
            href="tel:0553101208"
            className="text-center font-weight-bold text-darkblue"
          >
            Tel 0553101208
          </a>
        </Col>
      </Row>
    </div>
  );
};

export const MedicinesView = (props) => {
  return (
    <div className="blood-view shadow p-2 rounded-15 bg-white mb-3">
      <Row>
        <Col xs="3">
          <img
            alt=""
            src={"/images/product.png"}
            width="100%"
            className="rounded-15 blood-view-image"
          />
        </Col>
        <Col xs="9" className="text-center">
          <h6 className="text-center text-blue mb-0">
            Depakine Chrono - 500 mg
          </h6>
          <p className="text-muted text-center mb-0">Lieu retrait:</p>
          <b className="text-center text-darkblue"> Bir Khadem - ALGER </b>
        </Col>
      </Row>
    </div>
  );
};

export const Donations = (props) => {
  const [isBlood, setType] = useState(true);

  return (
    <div className="sidebar-donations shadow rounded-15 bg-white p-2 mt-4">
      <h5 className="text-center text-blue py-2">
        {" "}
        {"sidebar.donations.title"}{" "}
      </h5>
      <div
        className="sidebar-donations-select d-flex"
        style={{ justifyContent: "space-between" }}
      >
        <div style={{ width: "48%" }}>
          <Button
            onClick={() => setType(true)}
            className="btn-block rounded-15"
            style={{
              backgroundColor: isBlood ? "var(--ph-blue)" : "transparent",
              color: isBlood ? "#fff" : "var(--ph-blue)",
            }}
          >
            {" "}
            {"sidebar.donations.blood"}{" "}
          </Button>
        </div>
        <div style={{ width: "48%" }}>
          <Button
            onClick={() => setType(false)}
            className="btn-block rounded-15"
            style={{
              backgroundColor: !isBlood ? "var(--ph-blue)" : "transparent",
              color: !isBlood ? "#fff" : "var(--ph-blue)",
            }}
          >
            {" "}
            {"sidebar.donations.medicines"}{" "}
          </Button>
        </div>
      </div>
      <br />
      <div className="sidebar-donations-content">
        <div
          className="sidebar-donations-content-blood"
          style={{ display: isBlood ? "block" : "none" }}
        >
          {[...Array(6)].map((data, key) => {
            return <BloodView key={key} />;
          })}
        </div>

        <div
          className="sidebar-donations-content-medicines"
          style={{ display: !isBlood ? "block" : "none" }}
        >
          {[...Array(6)].map((data, key) => {
            return <MedicinesView key={key} />;
          })}
        </div>
      </div>
    </div>
  );
};

export const Search = (props) => {
  return (
    <div>
      <div className="sidebar-search rounded-15 bg-blue p-3 mt-5">
        <h4 className="text-center text-white"> {"sidebar.search.title"} </h4>
        <div className="sidebar-search-input">
          <Input
            type="text"
            name="keyword"
            className="rounded-15 font-weight-bold"
            placeholder={"sidebar.search.input.placeholder"}
          />
        </div>
        <Row className="sidebar-search-selects mt-2">
          <Col xs="6">
            <Input
              name=""
              type="select"
              className="rounded-15 font-weight-bold"
            >
              <option value="">Wilaya</option>
            </Input>
          </Col>
          <Col xs="6">
            <Input
              name=""
              type="select"
              className="rounded-15 font-weight-bold"
            >
              <option value="">Ville</option>
            </Input>
          </Col>
        </Row>
        <div className="sidebar-search-submit mt-2 text-center">
          <Button
            color="white"
            className="bg-white rounded-pill font-weight-bold text-blue"
          >
            {"header.search"}
          </Button>
        </div>
      </div>
    </div>
  );
};

export const Donate = (props) => {
  return (
    <div className="bg-darkblue rounded-15 mt-5 p-3">
      <h4 className="text-center text-white"> {"sidebar.donate.title"} </h4>
      <div className="sidebar-donate-buttons text-center">
        <Link href="/login">
          <Button
            color="blue"
            className="rounded-pill bg-blue font-weight-bold text-white mt-2"
          >
            {"sidebar.donate.button1"}
          </Button>
        </Link>
        <br />
        <Button
          color="blue"
          className="rounded-pill bg-blue font-weight-bold text-white mt-3"
        >
          {"sidebar.donate.button2"}
        </Button>
      </div>
    </div>
  );
};

export const Recruturs = (props) => {
  return (
    <div className="sidebar-recruturs mt-5 rounded-15 overflow-hidden shadow bg-white">
      <div className="sidebar-recruturs-image">
        <img alt="" src={"/images/slide2.jpg"} width="100%" />
      </div>
      <div className="sidebar-recruturs-buttons p-3 text-center">
        <Button color="white" className="rounded-15 font-weight-bold text-blue">
          {" "}
          {"sidebar.recruture.button1"}{" "}
        </Button>
        <Button
          color="white"
          className="rounded-15 font-weight-bold text-blue mt-2"
        >
          {" "}
          {"sidebar.recruture.button2"}{" "}
        </Button>
      </div>
    </div>
  );
};

export const Candidats = (props) => {
  return (
    <div className="sidebar-recruturs mt-5 rounded-15 overflow-hidden shadow bg-white">
      <div className="sidebar-recruturs-image">
        <img alt="" src={"/images/slide2.jpg"} width="100%" />
      </div>
      <div className="sidebar-recruturs-buttons p-3 text-center">
        <Button color="white" className="rounded-15 font-weight-bold text-blue">
          {" "}
          {"sidebar.candidats.button1"}{" "}
        </Button>
        <Button
          color="white"
          className="rounded-15 font-weight-bold text-blue mt-2"
        >
          {" "}
          {"sidebar.candidats.button2"}{" "}
        </Button>
      </div>
    </div>
  );
};

export const PhSearch = (props) => {
  const [activeTab, setTab] = useState(1);

  return (
    <div className="sidebar-ph-search mt-5 rounded-15 overflow-hidden">
      <div className="sidebar-ph-header">
        <Button
          onClick={() => setTab(1)}
          color="white"
          className="text-center"
          style={{
            backgroundColor: activeTab === 1 ? "var(--ph-blue)" : "#fff",
            color: activeTab === 1 ? "#fff" : "var(--ph-blue)",
          }}
        >
          {" "}
          {"sidebar.phsearch.button1"}{" "}
        </Button>
        <Button
          onClick={() => setTab(2)}
          color="white"
          className="text-center"
          style={{
            backgroundColor: activeTab === 2 ? "var(--ph-blue)" : "#fff",
            color: activeTab === 2 ? "#fff" : "var(--ph-blue)",
          }}
        >
          {" "}
          {"sidebar.phsearch.button2"}{" "}
        </Button>
      </div>
      <div className="sidebar-ph-content bg-blue p-3">
        <h6 className="text-white text-center">{"sidebar.phsearch.title"}</h6>
        <Row>
          <Col xs="2" className="text-center pr-0">
            <RiMapPin5Line size={28} color="#fff" />
          </Col>
          <Col xs="10" className="pl-0">
            <InputGroup
              style={{ overflow: "hidden" }}
              className="input-group-sm"
            >
              <Input name="" type="email" />
              <InputGroupAddon addonType="append">
                <InputGroupText
                  className="text-blue border-0"
                  style={{ backgroundColor: "#ddd" }}
                >
                  {" "}
                  <FiSend />{" "}
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </Row>
        <Row className="sidebar-ph-search-description text-left text-white">
          <Col xs="2"></Col>
          <Col xs="10" className="pl-0">
            {"sidebar.phsearch.description"}
          </Col>
        </Row>
        <div className="text-right">
          <Button
            color="darkblue"
            className="btn-sm rounded-15 bg-darkblue text-white"
          >
            {" "}
            {"header.search"}{" "}
          </Button>
        </div>
      </div>
    </div>
  );
};

export const SideBarAct = (props) => {
  const [populaire, setPopulaire] = useState([]);
  const [spinnerP, setSpinnerP] = useState(true);

  Preloader;
  useEffect(async () => {
    const result = await axios.get(API_LINK + "v1/articles-count/language/FR");
    setPopulaire(result.data);
    setSpinnerP(false);
  }, []);

  const [journalistes, setJournalistes] = useState([]);
  const [spinnerJ, setSpinnerJ] = useState(true);

  useEffect(async () => {
    const result = await axios.get(API_LINK + "v1/Auteur");
    setJournalistes(result.data);
    setSpinnerJ(false);
  }, []);

  return (
    <div>
      <h2> Les plus populaire</h2>
      {populaire.length ? (
        populaire.map((pop) => {
          return (
            <ArticlePreviewSmall
              link={"/articles/" + pop.category[0].label + "/" + pop.title}
              image={API_LINK + pop.image}
              category={pop.category.length ? pop.category[0].label : ""}
              title={pop.title}
              date={calculdatePublié(pop.date)}
              pop
              ID={pop._id}
            />
          );
        })
      ) : spinnerP ? (
        <div className="text-center">
          <Preloader />
        </div>
      ) : (
        <div className="text-danger text-center font-weight-bold">
          {" "}
          {"Information indisponible"}{" "}
        </div>
      )}

      <hr />
      <h2> Plus de Journalistes</h2>

      {journalistes.length ? (
        journalistes.map((journaliste) => {
          return (
            <ArticlePreviewSmall
              link=""
              image=""
              title={journaliste.lastname + " " + journaliste.name}
              nbrArt="99 Articles "
            />
          );
        })
      ) : spinnerJ ? (
        <div className="text-center">
          <Preloader />
        </div>
      ) : (
        <div className="text-danger text-center font-weight-bold">
          {" "}
          {"Information indisponible"}{" "}
        </div>
      )}
    </div>
  );
};

export const SideBarArt = (props) => {
  const [journalistes, setJournalistes] = useState([]);
  useEffect(async () => {
    const result = await axios.get(API_LINK + "v1/Auteur");
    setJournalistes(result.data);
  }, []);

  return (
    <div>
      <h2> Dernières infos</h2>
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />
      <ArticlePreviewSmall
        link={"/Actualite/"}
        image=""
        category="Categorie info"
        title="Dernières infos"
        date="il Ya 4h"
        pop
      />

      <hr />
      <h2> Plus de Journalistes</h2>

      {journalistes.length ? (
        journalistes.map((journaliste) => {
          return (
            <ArticlePreviewSmall
              link=""
              image=""
              title={journaliste.lastname + " " + journaliste.name}
              nbrArt="99 Articles "
            />
          );
        })
      ) : (
        <div className="text-danger text-center font-weight-bold">
          {" "}
          {"Information indisponible"}{" "}
        </div>
      )}
    </div>
  );
};
