import React, { Component } from "react";
import { Row, Col, Button, Modal, ModalBody, Container } from "reactstrap";
import { FaFileAlt, FaMapMarker, FaShare, FaStar } from "react-icons/fa";

import "./Job.module.css";

export class JobViewModal extends Component {
  state = {
    modalOpen: this.props.modalOpen,
    // for language
    langLoaded: false,
  };

  componentDidMount() {
    // load language
  }

  render() {
    // state short
    const s = this.state;
    // props short
    const p = this.props;

    return (
      <Modal isOpen={p.modalOpen} external={p.externalCloseBtn} size="lg">
        <ModalBody className="bg-ddd rounded-15 pb-4">
          <Row>
            <Col md={p.isDashboard ? "12" : "8"}>
              <div className="bg-white p-2 rounded-15 shadow-sm pb-5">
                {!p.isDashboard ? (
                  <div className="text-right">
                    <Button
                      color="blue"
                      outline
                      className="rounded-pill border-2 border-blue text-blue"
                      size="sm"
                    >
                      {" "}
                      <FaShare /> {"article.share"}{" "}
                    </Button>
                  </div>
                ) : (
                  ""
                )}
                <h5 className="text-blue text-center">
                  {" "}
                  {"add_cv.preview.company_name"}{" "}
                </h5>
                <p className="text-ccc text-center">
                  {" "}
                  {"add_cv.preview.activity_secteur"}{" "}
                </p>
                <div className="viewjob-image text-center">
                  <img src={"/images/slide2.jpg"} width="60%" />
                </div>
                <p className="text-ccc text-center mt-1">
                  {" "}
                  {"add_cv.preview.company_size"}{" "}
                </p>
                <hr />
                <h6 className="text-center text-muted">
                  {" "}
                  {"add_cv.preview.offers_critere"}{" "}
                </h6>
                <br />
                <Container>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.exp.post_title"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.exp.post_place"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"home.job.contract_type"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.preview.emploi_type"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.preview.school_level"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.preview.poste_level"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"jobs.filter.exp"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <div className="viewjob-info">
                    <b className="text-blue font-size-14">
                      {" "}
                      {"add_cv.preview.poste_description"} :{" "}
                    </b>{" "}
                    <span className="text-ccc">Text</span>
                  </div>
                  <br />
                  <div className="viewjob-salary mt-2">
                    <h6 className="text-center text-blue">
                      {" "}
                      {"add_cv.preview.salary"}{" "}
                    </h6>
                    <p className="text-center">
                      <span className="text-ccc">
                        {"add_cv.preview.entre"}{" "}
                        <b className="text-blue">35 000 DA</b>{" "}
                        {"add_cv.preview.et"}{" "}
                        <b className="text-blue">5 0000 DA</b>
                      </span>
                    </p>
                  </div>
                  <Container
                    className="viewjob-adventages shadow-sm rounded-15 mt-3 py-2"
                    style={{ width: "70%" }}
                  >
                    <h6 className="text-center text-blue">
                      {"add_cv.preview.adventages"}
                    </h6>
                    <p className="text-ccc"> - text textx txt</p>
                    <p className="text-ccc"> - text textx txt</p>
                    <p className="text-ccc"> - text textx txt</p>
                    <p className="text-ccc"> - text textx txt</p>
                  </Container>
                </Container>
              </div>
            </Col>
            {!p.isDashboard ? (
              <Col md="4">
                <Container>
                  <Button
                    color="blue"
                    className="text-blue rounded-15 bg-white border-blue border-2 mt-3"
                    outline
                    block
                  >
                    {" "}
                    <FaStar style={{ marginTop: "-3px" }} />{" "}
                    {"add_cv.preview.sidebutton1"}{" "}
                  </Button>
                  <br />
                  <Button
                    color="blue"
                    className="text-blue rounded-15 bg-white border-blue border-2"
                    outline
                    block
                  >
                    {" "}
                    {"add_cv.preview.sidebutton2"}{" "}
                  </Button>
                </Container>
              </Col>
            ) : (
              ""
            )}
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}

class JobView extends Component {
  state = {
    modalOpen: false,
  };

  render() {
    const s = this.state;

    const externalCloseBtn = (
      <button
        className="close"
        style={{
          position: "fixed",
          top: "15px",
          right: "15px",
          color: "white",
        }}
        onClick={() => this.setState({ modalOpen: false })}
      >
        &times;
      </button>
    );

    return (
      <div
        className={`job-view shadow-sm p-3 bg-white rounded-15 mt-4 ${this.props.className}`}
        onClick={() => this.setState({ modalOpen: true })}
      >
        <Row>
          <Col md="3">
            <div className="job-view-image rounded-15">
              <img
                src={"/images/slide2.jpg"}
                alt="job"
                width="100%"
                style={{ height: "110px" }}
              />
            </div>
          </Col>
          <Col md="9">
            <div className="job-view-title">Médecin du Travail</div>
            <div className="job-view-company">GROUPE IFRI</div>
            <div className="job-view-expbtn">
              <Button className="btn-sm">
                Confirmé / Expérimenté ( 3 À 5 Ans )
              </Button>
            </div>
            <div className="job-view-button">
              <Button
                color="blue"
                className="rounded-15 bg-blue btn-sm text-white"
              >
                Postuler
              </Button>
            </div>
            <Row className="mt-2">
              <Col md="2" xs="12"></Col>
              <Col xs="6" md="6" className="text-right">
                <div className="job-view-file">
                  <b className="text-blue">
                    <FaFileAlt /> Type de contract:{" "}
                  </b>{" "}
                  <b className="text-muted">CDI</b>
                </div>
              </Col>
              <Col xs="4" md="4" className="text-right">
                <div className="job-view-location">
                  <b className="text-blue">
                    <FaMapMarker />
                  </b>{" "}
                  <b className="text-muted">Bejaia, Algérie </b>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <JobViewModal
          modalOpen={s.modalOpen}
          externalCloseBtn={externalCloseBtn}
        />
      </div>
    );
  }
}

export default JobView;
