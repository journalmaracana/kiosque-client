import { Form, FormGroup, Label, Input, Row, Col, Button } from "reactstrap";
import styles from "./RegisterForm.module.css";
import { getYears, getDays, getMonth } from "../../utils/functions";
import { PHONE_CODE, PAYS_OPTIONS } from "../../utils/constant";
import Select from "react-select";

export const PersonalInformation = (props) => {
  if (props.editeur) {
    return (
      <FormGroup>
        <Label className="text-blue font-weight-bold">{"Editeur"} :</Label>
        <Input
          type="text"
          name="editor"
          value={props.editor}
          onChange={props.handleChange}
        />
        <span style={{ color: "red" }}>
          {props.validator.message("editeur", props.editor, "required")}
        </span>
      </FormGroup>
    );
  } else {
    const { sexe, name, lastname, pseudo, day, month, year } = props.state;
    return (
      <>
        <FormGroup>
          <Label className="text-blue font-weight-bold"> {"Cevelity"} : </Label>
          <Row className={styles["register-gender-select"]}>
            <Col xs="6">
              <Button
                onClick={() => props.handleChangeSexe("monsieur")}
                type="button"
                block
                outline
                className={`${sexe === "monsieur" ? "active" : ""}`}
              >
                {"Monsieur"}
              </Button>
            </Col>
            <Col xs="6">
              <Button
                onClick={() => handleChangeSexe("monsieur")}
                type="button"
                block
                outline
                onClick={() => props.handleChangeSexe("madame")}
              >
                {"Madame"}
              </Button>
            </Col>
          </Row>
          <span style={{ color: "red" }}>
            {props.validator.message("sexe", sexe, "required")}
          </span>
        </FormGroup>
        <FormGroup>
          <Label className="text-blue font-weight-bold">{"Nom"} :</Label>
          <Input
            type="text"
            name="lastname"
            value={lastname}
            onChange={props.handleChange}
          />
          <span style={{ color: "red" }}>
            {props.validator.message("nom", lastname, "required")}
          </span>
        </FormGroup>
        <FormGroup>
          <Label className="text-blue font-weight-bold"> {"Prenom"} : </Label>
          <Input
            type="text"
            name="name"
            value={name}
            onChange={props.handleChange}
          />
          <span style={{ color: "red" }}>
            {props.validator.message("prénom", name, "required")}
          </span>
        </FormGroup>
        <FormGroup>
          <Label className="text-blue font-weight-bold">{"Pseudo"} :</Label>
          <Input
            type="text"
            name="pseudo"
            value={pseudo}
            onChange={props.handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label className="text-blue font-weight-bold">
            {" "}
            {"Date de naissance"} :{" "}
          </Label>
          <Row>
            <Col xs="4">
              <Select
                className="basic-single"
                classNamePrefix="select"
                placeholder={"Jour"}
                isSearchable
                name="day"
                id="day"
                options={getDays(month, year)}
                value={getDays(month, year).filter(
                  ({ value }) => value === day
                )}
                onChange={(e) => props.handleChangeDate(e, "day")}
              />
              <span style={{ color: "red" }}>
                {props.validator.message(
                  "jour",
                  getDays(month, year).filter(({ value }) => value === day),
                  "required"
                )}
              </span>
            </Col>
            <Col xs="4">
              <Select
                className="basic-single"
                classNamePrefix="select"
                placeholder={"Mois"}
                isSearchable
                name="month"
                id="month"
                options={getMonth()}
                value={getMonth().filter(({ value }) => value === month)}
                onChange={(e) => props.handleChangeDate(e, "month")}
              />
              <span style={{ color: "red" }}>
                {props.validator.message("mois", month, "required")}
              </span>
            </Col>
            <Col xs="4">
              <Select
                className="basic-single"
                classNamePrefix="select"
                placeholder={"Année"}
                isSearchable
                name="year"
                id="year"
                options={getYears()}
                value={getYears().filter(({ value }) => value === year)}
                onChange={(e) => props.handleChangeDate(e, "year")}
              />
              <span style={{ color: "red" }}>
                {props.validator.message("année", year, "required")}
              </span>
            </Col>
          </Row>
        </FormGroup>
      </>
    );
  }
};
export const TitleForm = (props) => {
  return (
    <h3 className={`text-center text-darkblue mt-4 ${props.style}`}>
      {" "}
      {props.title}
    </h3>
  );
};

export const IdentifierInformation = (props) => {
  const { mail, password, repassword } = props.state;
  return (
    <>
      <FormGroup>
        <Label className="text-blue font-weight-bold">Email : </Label>
        <Input
          type="email"
          name="mail"
          onChange={props.handleChange}
          value={mail}
        />
        <span style={{ color: "red" }}>
          {props.validator.message("email", mail, "required|email")}
        </span>
      </FormGroup>
      <FormGroup>
        <Label className="text-blue font-weight-bold">Mot de passe : </Label>
        <Input
          type="password"
          name="password"
          value={password}
          onChange={props.handleChange}
        />

        <span style={{ color: "red" }}>
          {props.validator.message("mot de passe ", password, [
            "required",
            {
              regex: `^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$#%*+@!?&])[A-Za-z\\d$#%*+@!?&]{12,}$`,
            },
          ])}
        </span>
      </FormGroup>
      <FormGroup>
        <Label className="text-blue font-weight-bold">Confirmation : </Label>
        <Input
          type="password"
          name="repassword"
          onChange={props.handleChange}
          value={repassword}
        />
        <span style={{ color: "red" }}>
          {props.validator.message(
            "confirmation",
            repassword,
            "required|in:" + password + ""
          )}
        </span>
      </FormGroup>
    </>
  );
};
export const EditeurInformation = (props) => {
  const {
    site,
    adress,
    codeFix,
    phoneFix,
    codeFax,
    phoneFax,
    codeMobile,
    phoneMobile,
  } = props.state;
  return (
    <>
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Site"} : </Label>
        <Input
          type="text"
          name="site"
          value={site}
          onChange={props.handleChange}
          placeholder="Example: https://maracana.com"
        />
        <span style={{ color: "red" }}>
          {props.validator.message("site", site, "url")}
        </span>
      </FormGroup>

      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Fixe"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder="+123"
              isSearchable
              name="codeFix"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value === codeFix)}
              onChange={(e) => props.handleChangeDate(e, "codeFix")}
            />
          </Col>
          <Col xs="9">
            <Input
              type="tel"
              name="phoneFix"
              value={phoneFix}
              onChange={props.handleChange}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("Fixe", phoneFix, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Fax"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeFax"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeFax)}
              onChange={(e) => props.handleChangeDate(e, "codeFax")}
            />
          </Col>
          <Col xs="9">
            <Input
              type="tel"
              name="phoneFax"
              value={phoneFax}
              onChange={props.handleChange}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("Fax", phoneFax, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Mobile"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeMobile"
              id="codeMobile"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeMobile)}
              onChange={(e) => props.handleChangeDate(e, "codeMobile")}
            />
          </Col>
          <Col xs="9">
            <Input
              type="tel"
              name="phoneMobile"
              value={phoneMobile}
              onChange={props.handleChange}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("mobile", phoneMobile, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>

      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Adress"} : </Label>
        <Input
          type="text"
          name="adress"
          value={adress}
          onChange={props.handleChange}
        />
        <span style={{ color: "red" }}>
          {props.validator.message("adress", adress, "required")}
        </span>
      </FormGroup>
    </>
  );
};
