import React from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import styles from "./Auth.module.css";

export const RegisterSelect = (props) => {
  return (
    <Row>
      {props.data.map((data, key) => {
        return (
          <Col xs="6" key={key}>
            <Link href={data.to}>
              <div
                className={`${styles["register-select-elem"]} m-2 p-2 text-center rounded-15`}
              >
                <div className="register-select-elem-icon">{data.icon}</div>
                <div className="register-select-elem-label text-darkblue">
                  <b>{data.name}</b>
                </div>
              </div>
            </Link>
          </Col>
        );
      })}
    </Row>
  );
};

export const RegsterNavIcon = (props) => {
  return (
    <div className={`${styles["register-nav-icon"]} position-absolute `}>
      <Link href={props.to}>{props.icon}</Link>
    </div>
  );
};
