import React from "react";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import Link from "next/link";

import styles from "./Auth.module.css";

function LoginForm(props) {
  return (
    <Form className={styles["login-form"]}>
      <FormGroup>
        <Label className="text-blue"> {"Email"} : </Label>
        <Input
          name="mail"
          type="email"
          onChange={(e) => props.setMail(e.target.value)}
        />
      </FormGroup>
      <FormGroup>
        <Label className="text-blue"> {"Mot de passe"} : </Label>
        <Input
          name="password"
          type="password"
          onChange={(e) => props.setPassword(e.target.value)}
        />
      </FormGroup>
      <FormGroup className="text-center">
        <Button
          onClick={() => {
            props.onSubmit();
          }}
          type="button"
          color="blue"
          className="text-white bg-blue rounded-12 font-weight-bold"
        >
          {" "}
          {"Connetcer"}{" "}
        </Button>
        <br />
        <span className="text-blue"> {"auth.register"}! </span>
      </FormGroup>
      <FormGroup className="pb-3">
        <Link href="/password_recovery">
          <span
            className={`${styles["login-form-fp"]} float-right text-darkblue `}
          >
            {" "}
            {"Mot de passe oublié"} !{" "}
          </span>
        </Link>
      </FormGroup>
    </Form>
  );
}

export default LoginForm;
