import React, { Component } from "react";
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Container,
  Row,
  Col,
  Button,
} from "reactstrap";
import Link from "next/link";
import styles from "../../styles/Live.module.css";
import Select from "react-select";
import { AiOutlineStar } from "react-icons/ai";
import { AiOutlineShareAlt } from "react-icons/ai";
import moment from "moment";
import Axios from "../../utils/axios_config";
import { isEmpty } from "../../utils/functions";
import { API_LINK } from "../../utils/constant";

class PressNumPreview extends Component {
  constructor() {
    super();
    this.customStyles = {
      option: (provided, state) => ({
        ...provided,
        borderBottom: "1px dotted pink",
        color: state.isSelected ? "white" : "blue",
        //padding: 20,
      }),
      singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = "opacity 300ms";

        return { ...provided, opacity, transition };
      },
    };
    this.state = {
      press: {},
    };
  }
  componentDidMount = () => {
    this.setState({
      press: this.props.press,
    });
  };
  // getPressByRegionAndPress = (idPub, idRegion) => {
  //   Axios.get("/pressByPubAndRegion/" + idPub + "/" + idRegion)
  //     .then((response) => {
  //       this.setState({
  //         press: response.data,
  //       });
  //     })
  //     .catch((err) => console.log(err));
  // };

  handleChangeSelect = (e) => {
    if (e) {
      this.props.getPressesByRegionAndPub(
        this.props.namePub,
        e.value,
        "Select"
      );
    }
  };

  render() {
    // const { press } = this.props;
    const { press } = this.state;
    return (
      <>
        {!isEmpty(press) && press ? (
          <div className={`${styles["cadre"]} position-relative p-4`}>
            <div className="">
              <div className={`${styles["journal-apercu"]} text-center`}>
                <h1>
                  <p>{press.namePub?.name}</p>
                </h1>
                <p className="text-dark">{press.namePub?.periodicite}</p>
                <p className="text-dark">Premium</p>
                <p className="text-dark">
                  Editeur :{" "}
                  {press.namePub?.user?.name +
                    " " +
                    press.namePub?.user?.lastname}{" "}
                  - Langue : {press.namePub?.langue} - Page :{" "}
                  {press.namePub?.page}
                </p>
                {!(
                  press.namePub?.region.length == 1 &&
                  press.namePub?.region[0].entitled == "nationnal"
                ) ? (
                  <>
                    <p className="text-dark">Présentation </p>{" "}
                    <Select
                      defaultValue={[
                        {
                          label: press.region.entitled,
                          value: press.region._id,
                        },
                      ]}
                      options={
                        press.namePub?.region.length
                          ? press.namePub.region.map((region) => {
                              return {
                                label: region.entitled,
                                value: region._id,
                              };
                            })
                          : []
                      }
                      onChange={this.handleChangeSelect}
                      styles={this.customStyles}
                      isClearable
                    />
                  </>
                ) : null}

                <p className="text-dark mt-1">
                  Daté du {moment(press.parution).format("DD-MM-YYYY")}
                </p>
                <div className="w-100 position-relative text-center">
                  <div className={styles["journal-apercu-overlay"]}></div>
                  <img
                    src={API_LINK + press.laUne}
                    width="70%"
                    style={{ width: "65%!important", height: "auto" }}
                  />
                </div>
                <Row style={{ width: "50%", margin: "auto" }} className="my-2">
                  <Button color="dark" className="button font-size-14" block>
                    {" "}
                    {"Lire ce numéro"}{" "}
                  </Button>
                  {/* <Button color="dark" className="button font-size-14" block>
                  {" "}
                  {"Accéder aux archives"}{" "}
                </Button> */}
                </Row>
                <br />
                <Row>
                  <Col>
                    {" "}
                    <AiOutlineStar
                      size={30}
                      className="ml-2"
                      color="#ccc"
                    />{" "}
                    Ajouter à mes favoris
                  </Col>
                </Row>
                <Row className="mt-2">
                  <Col>
                    <AiOutlineShareAlt
                      size={30}
                      className="ml-2"
                      color="#ccc"
                    />{" "}
                    Partager
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        ) : (
          <div className="text-danger text-center font-weight-bold mt-4">
            {" "}
            {"Information indisponible"}{" "}
          </div>
        )}
      </>
    );
  }
}

export default PressNumPreview;
