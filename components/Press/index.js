import React, { Component } from "react";

import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Container,
  Row,
  Col,
  Button,
} from "reactstrap";
import Link from "next/link";
import styles from "../../styles/Live.module.css";
import { TiThList } from "react-icons/ti";

class PressPreview extends Component {
  render() {
    const p = this.props;
    return (
      // styles["journal-apercu-overlay"]
      // className={`${styles["sidebar-newspaper-buttons"]} text-dark text-center`}

      <div className={`${styles["cadre"]} position-relative p-4`}>
        <div className="">
          <div className={`${styles["journal-apercu"]} text-center`}>
            <h3
              className={`${styles["sidebar-newspaper-title"]} text-dark py-3`}
            >
              <small>{p.Titre}</small>
            </h3>
            <p className="text-dark">{this.props.date}</p>

            <div className="row margin0"></div>
            <div className="w-100 position-relative text-center">
              <div className={styles["journal-apercu-overlay"]}></div>
              <img
                src={p.imageURL}
                width="70%"
                style={{ width: "65%!important", height: "auto" }}
              />
            </div>
            <Row style={{ width: "80%", margin: "auto" }} className="my-2">
              <Button color="dark" className="button font-size-14" block>
                {" "}
                {"Lire ce numéro"}{" "}
              </Button>
              <Link
                href={{
                  pathname: this.props.link ? this.props.link : "/",
                  query: { idPress: this.props.idPress },
                }}
                as={this.props.link ? this.props.link : "/"}
              >
                {/* <Link to={pathname: '/component', state: {id: '#id'}}> */}
                <Button color="dark" className="button font-size-14" block>
                  {" "}
                  {"Accéder aux archives"}{" "}
                </Button>
              </Link>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default PressPreview;
