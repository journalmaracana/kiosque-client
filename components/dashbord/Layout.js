import DashboardHeader from "./Header";
import DashboardNavigation from "./Navigation";

function DashLayout(props) {
  return (
    <div className="mydashboard">
      <DashboardHeader />
      <DashboardNavigation {...props}/>
      <div>{props.children}</div>
    </div>
  );
}

export default DashLayout;
