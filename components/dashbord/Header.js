import { Container, Row, Col } from "reactstrap";
import { TiPlusOutline } from "react-icons/ti";
import { FaRegEnvelope } from "react-icons/fa";
import styles from "./Header.module.css";
import Image from "next/image";

function Header(props) {
  return (
    <div
      className={`${styles["dashboard-header"]} fixed-top w-100 shadow-sm bg-white p-1`}
    >
      <Container fluid>
        <Row>
          <Col md="9" xs="4">
            <div className={`${styles["ph-header-logo"]} d-flex`}>
              <TiPlusOutline size={80} />
              <h4 className="mt-2 text-blue">
                Numérique <br /> Pharma
              </h4>
            </div>
          </Col>
          <Col md="1" xs="4">
            <FaRegEnvelope className="text-blue mt-4" size={30} />
          </Col>
          <Col md="2" xs="4">
            <Row className="mt-3">
              <Col xs="8" className="text-right">
                <b className="text-blue">SEBA Cyriel</b>
                <br />
                <small className="text-blue">Pharmacie</small>
              </Col>
              <Col xs="4">
                <div className="dashboard-header-profilepic overflow-hidden rounded-pill">
                  <Image
                    src="/images/user.png"
                    alt="Picture of the author"
                    width={500}
                    height={500}
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Header;
