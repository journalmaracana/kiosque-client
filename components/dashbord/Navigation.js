import React, { Component } from "react";
import Link from "next/link";
import { Container } from "reactstrap";
import { TiHomeOutline } from "react-icons/ti";
import { GoSettings } from "react-icons/go";
import { MdWork } from "react-icons/md";
import { BsClock } from "react-icons/bs";
import {
  AiOutlineUser,
  AiOutlineShopping,
  AiOutlineLogout,
  AiOutlineUserDelete,
} from "react-icons/ai";
import { FaRegEnvelope, FaStoreAlt, FaRegBookmark } from "react-icons/fa";
import styles from "./Navigation.module.css";
const elems = [
  {
    icon: <TiHomeOutline size={30} />,
    to: "/dashboard/home",
    text: "Tableau de bord",
  },
  {
    icon: <AiOutlineUser size={30} />,
    to: "/dashboard/profile",
    text: "compte",
  },
  {
    icon: <FaRegBookmark size={30} />,
    to: "/dashbord/presse",
    text: "presse",
  },
  {
    icon: <AiOutlineShopping size={30} />,
    to: "/dashbord/livres",
    text: "livres",
  },
  {
    icon: <FaStoreAlt size={30} />,
    to: "/dashbord/articles",
    text: "articles",
  },

  {
    icon: <FaRegBookmark size={30} />,
    to: "/dashboard/forfait",
    text: "Mon forfait",
  },
  {
    icon: <FaRegEnvelope size={30} />,
    to: "/dashboard/inbox",
    text: "Messagerie",
  },
  {
    icon: <GoSettings size={30} />,
    to: "/dashbord/pays",
    text: "Pays",
  },
  {
    icon: <GoSettings size={30} />,
    to: "/dashboard/settings",
    text: "Paramètres",
  },
  {
    icon: <AiOutlineUserDelete size={30} />,
    to: "/dashbord/compte",
    text: "Supprimer mon compte",
  },
  {
    icon: <AiOutlineLogout size={30} />,
    to: "/dashboard/settings",
    text: "Se deconnecter",
  },
];
class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "",
      route: props
    };
  }

  componentDidMount() {
    // load language

    window.scrollTo(0, 0);
  }

  render() {
    const s = this.state;
    return (
      <div
        className={`${styles["dashboard-navigation"]} bg-darkblue float-left`}
      >
        <Container>
          {elems.map((data, key) => {
              //console.log('inside',this.state.route)
              return (
           
              <Link
                href={data.to}
                key={key}
                
                onClick={() =>
                  this.setState({
                    route:this.props,
                    activeTab: data.to.split("/")[
                      data.to.split("/").length - 1
                    ],
                  })
                }
              >
                <div
                  className={`dashboard-navigation-elem text-center mb-3 mt-4 ${
                    s.activeTab ===
                    data.to.split("/")[data.to.split("/").length - 1]
                      ? "active bg-white text-blue rounded-12 py-2 px-1"
                      : "text-white"
                  }`}
                >
                  <div className="dashboard-navigation-elem-icon">
                    {data.icon}
                  </div>
                  <div
                    className={`${styles["dashboard-navigation-elem-label"]}`}
                  >
                    {data.text}
                  </div>
                </div>
              </Link>
            );
          })}
        </Container>
      </div>
    );
  }
}

export default Navigation;
