import React, { Component } from 'react'
import styles from '../../styles/TagsInput.module.css'

import { Input } from 'reactstrap';;

export const TagsInput = (props) => {
	return(<div className={styles.container}>
		<div className="input-tag" onClick={() => document.getElementById('taginput').focus()}>
     
      <ul >
        {props.tags.map((tag, i) => (
          <li key={tag} className="input-tag__tags li button" >
            {tag}
            <button type="button" onClick={() => { props.removeTag(i); }}>x</button>
          </li>
        ))}
        {!props.career ? <li > <Input className="input-tag__tags" type="text" id='taginput' name='taginput' onKeyDown={props.inputKeyDown} /> </li> : null}
      </ul>
    </div></div>
	);
}
