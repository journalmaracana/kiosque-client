import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import Link from "next/link";
import styles from "./Article.module.css";

class Article extends Component {
  render() {
    const p = this.props;

    return (
      <div
        className={`article shadow-sm rounded-15 bg-white p-3 ${p.className}`}
      >
        <Link href={"p.to"}>
          <Row>
            <Col xs={p.lg ? "0" : "5"} md="5">
              <div className="article-image rounded-15">
                <img
                  alt="article"
                  src={"/images/slide1.png"}
                  width="100%"
                  style={{ minHeight: p.sm ? "100px" : "200px" }}
                />
              </div>
            </Col>
            <Col xs={p.lg ? "0" : "7"} md="7">
              <div className="article-title text-blue">
                <b style={{ fontSize: p.sm ? "18px" : "22px" }}> {p.title} </b>
              </div>
              <div className="article-description text-muted">
                {p.description}
              </div>
            </Col>
          </Row>
        </Link>
      </div>
    );
  }
}

export default Article;
