import React from 'react';
import { Modal, ModalBody, Col, Row, Button } from 'reactstrap';

export const DeleteModal = (props) => {
	return(
		<Modal isOpen={true} className="mt-5 pt-5">
			<ModalBody className="px-5 py-4">
				<div className="text-center font-size-18">{"Voulez vous vraiment supprimer cette ligne?!"}</div>
				<br />
				<Row>
					<Col xs="6">
						<Button color="light" block className="rounded-12 shadow-sm" onClick={() => props.toggle()}> {"Fermer"} </Button>
					</Col>
					<Col xs="6">
						<Button color="danger" block className="rounded-12 shadow-sm"> {"Supprimer"} </Button>
					</Col>
				</Row>
			</ModalBody>
		</Modal>
	);
}