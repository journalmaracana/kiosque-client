import React, { Component } from "react";

import "./Donation.module.css";

class DonationView extends Component {
  render() {
    return (
      <div className="donation-view shadow rounded-15 p-2 bg-white">
        <div className="donation-view-image rounded-15">
          <img
            src={"/images/slide2.jpg"}
            alt="don"
            width="100%"
            height="120px"
          />
        </div>
        <div className="text-center donation-view-title text-blue">
          Stryker Secure ll Hospital Bed
        </div>
        <div className="text-center donation-view-location text-darkblue mt-2">
          Alger, Algérie
        </div>
      </div>
    );
  }
}

export default DonationView;
