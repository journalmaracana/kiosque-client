import React, { Component } from "react";
import { Button } from "reactstrap";
import "./Product.module.css";

class Product extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // state short
    const s = this.state;

    return (
      <div className="ph-product shadow rounded p-2 bg-white">
        <div className="ph-product-image">
          <img
            alt="product"
            className="rounded"
            src={"/images/product.png"}
            width="100%"
          />
        </div>
        <div className="ph-product-title text-center text-blue">
          BIOPTIMUM MAGNESIUM 300+
        </div>
        <div className="ph-product-price text-darkblue text-center">
          <b>250,00 DZD</b>
        </div>
        <div className="ph-product-button">
          <Button type="button" className="btn-sm btn-block mt-1 bg-blue">
            {" "}
            {"home.products.button"}{" "}
          </Button>
        </div>
      </div>
    );
  }
}

export default Product;
