import React, { Component } from "react";
import Link from "next/link";

import { Row, Col } from "reactstrap";

import styles from "../../../styles/smallArticle.module.css";

class ArticlePreviewSmall extends Component {
  render = () => {
    return (
      <div className={styles["home-news-sm"]}>
        <Link href={{ pathname: this.props.link }}>
          <Row className="mt-2">
            <Col md="5">
              <div className={styles["home-news-img"]}>
                <img
                  src={this.props.image}
                  alt=""
                  width="100%"
                  height="120px"
                />

                {this.props.pop ? (
                  <span className="bg-red text-dark">
                    {this.props.category}
                  </span>
                ) : null}
              </div>
            </Col>
            <Col md="7">
              <h6 className={styles["home-news-title"]}>
                <Link href={{ pathname: this.props.link }} className="text-dark">
                  {this.props.title}
                </Link>
              </h6>
              <span className="bg-red text-dark">
                {this.props.pop ? this.props.date : this.props.nbrArt}
              </span>
            </Col>
          </Row>
        </Link>
      </div>
    );
  };
}

export default ArticlePreviewSmall;
