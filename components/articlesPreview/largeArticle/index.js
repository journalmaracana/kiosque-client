import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'reactstrap';

import './index.css';

class ArticlePreviewLarge extends Component {
  render = () => {
    return(
      <div className="home-news-bg">
        <Row>
          <Col md="6">
            <div className="home-news-bg-img">
              <Link to={this.props.link}>
                <img src={this.props.image} alt="news image" width="100%" height="260px" />
              </Link>
              <span className="bg-red text-white">{this.props.category}</span>
            </div>
          </Col>
          <Col md="6">
            <h6 className="home-news-bg-title mt-4">
              <Link to={this.props.link} className="text-dark" style={{fontSize: '19px'}}>{this.props.title}</Link>
            </h6>
            <br />
            <div className="home-news-bg-description">
              <p style={{textAlign:'justify', fontSize: '14px!important'}}>
                {this.props.description}
              </p>
            </div>
            <div className="home-news-bg-button text-right">
              <Link to={this.props.link} className="btn bg-red text-white btn-sm">LIRE LA SUITE</Link>
              <div className="c"></div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ArticlePreviewLarge;