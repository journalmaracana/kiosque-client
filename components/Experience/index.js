import React, { Component } from "react";
import { Button } from "reactstrap";

import {
  FaFacebookF,
  FaYoutube,
  FaInstagram,
  FaTwitter,
  FaEnvelope,
} from "react-icons/fa";

import "./Exp.module.css";

class ExpView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // for language
      langLoaded: false,
    };
  }

  componentDidMount() {
    // load language
  }

  render() {
    // state short
    const s = this.state;

    return (
      <div className="exp-view shadow-sm p-3 rounded-15 mt-4 bg-white">
        <div
          className="exp-view-name float-right text-blue"
          style={{ marginTop: "-8px" }}
        >
          {" "}
          Pierre Barthélémy{" "}
        </div>
        <h5 className="exp-view-title text-blue">
          Accident vasculaire cérébral ?
        </h5>
        <p className="text-muted">
          Le Lorem Ipsum est simplement du faux texte employé dans la
          composition et la mise en page avant impression. Le Lorem Ipsum est le
          faux texte standard de l'imprimerie depuis les années 1500
        </p>
        <div className="exp-view-title">
          <Button className="btn-sm rounded-pill bg-blue border-0">
            {" "}
            {"home.exp.button"}{" "}
          </Button>
        </div>
        <div className="exo-view-social bg-blue">
          <div className="d-flex sidebar-followus-icons">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaFacebookF />
              </a>
            </div>
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaYoutube />
              </a>
            </div>
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaInstagram />
              </a>
            </div>
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaTwitter />
              </a>
            </div>
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaEnvelope />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ExpView;
