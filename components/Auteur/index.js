import React, { Component } from "react";
import { Col, Row } from "reactstrap";
import Link from 'next/link';
import styles from "../../styles/mediumArticle.module.css";

class AuteurPreview extends Component {
  render = () => {
    return (
      <div className={styles["home-news-sm"]}>
        {/* <Link href={{ pathname: this.props.link }}
         className="petit-article"> */}
          <Col md="12" className="Intervieux padding0 border-actualite">
            <Row
              className={`${styles["article-apercu-row"]} article-apercu-row pt-3`}
            >
              <Col md="4" className="padding0">
                <div className={styles["home-news-img"]}>
                  <img src={this.props.image} width="100%" />
                </div>
              </Col>
              <Col md="8">
                <div className={`${styles["text-darkblue"]} pt-3`}>
                  <h3>
                    <b>{this.props.title}</b>
                  </h3>
                  <p className="text-dark">{this.props.Articles + ' Articles'}</p>
                  <p className="text-dark">{this.props.Livres + ' Livres'}</p>
                  <p className="text-dark">{this.props.Followers + ' Followers'}</p>
                </div>
              </Col>
            </Row>
          </Col>
        {/* </Link> */}
      </div>
    );
  };
}

export default AuteurPreview;
