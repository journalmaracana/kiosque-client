import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Input,
  FormGroup,
} from "reactstrap";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";

function index(props) {
  const { label, show, inputName, inputValue } = props;

  return (
    <FormGroup>
      <Label className="font-weight-bold">{label + " :"}</Label>
      <InputGroup>
        <Input
          type={show ? "text" : "password"}
          name={inputName}
          value={inputValue}
          onChange={props.handelChange}
        />
        <InputGroupAddon addonType="append">
          <span onClick={props.clicked}>
            <InputGroupText>
              {" "}
              {show ? <VisibilityIcon /> : <VisibilityOffIcon />}{" "}
            </InputGroupText>
          </span>
        </InputGroupAddon>
      </InputGroup>
      {props.children}
      <span style={{ color: "red" }}>{props.validator}</span>
    </FormGroup>
  );
}

export default index;
