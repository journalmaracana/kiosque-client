import React, { Component } from "react";
import { Input } from "reactstrap";
import "./SearchInput.module.css";
import { AiOutlineSearch } from "react-icons/ai";

class SearchInput extends Component {
  render() {
    return (
      <form className="ph-header-form mt-2">
        <Input
          type="text"
          className="rounded-pill input-search"
          placeholder={this.props.placeholder}
        />
        <AiOutlineSearch size={25} />
      </form>
    );
  }
}
export default SearchInput;
