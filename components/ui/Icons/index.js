import { Row, Col } from "reactstrap";
import {
  FaFacebookF,
  FaYoutube,
  FaInstagram,
  FaTwitter,
  FaEnvelope,
} from "react-icons/fa";

import "./Icons.module.css";

function HeaderIcons(props) {
  return (
    <div>
      <div className="pre-head-icons"></div>
      <div className="head-icons">
        <Row>
          <Col xs="2" className="ml-2">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaFacebookF />
              </a>
            </div>
          </Col>
          <Col xs="2">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaYoutube />
              </a>
            </div>
          </Col>
          <Col xs="2">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaInstagram />
              </a>
            </div>
          </Col>
          <Col xs="2">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaTwitter />
              </a>
            </div>
          </Col>
          <Col xs="2">
            <div className="head-icon rounded-pill m-2">
              <a href="https://facebook.com">
                <FaEnvelope />
              </a>
            </div>
          </Col>
        </Row>
      </div>
      <div className="af-head-icons"></div>
      {/*
        <div className="head-icons-bottom-left"></div>
        <div className="head-icons-bottom"></div>
        <div className="head-icons-bottom-right"></div>
        */}
    </div>
  );
}

export default HeaderIcons;
