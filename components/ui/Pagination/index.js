import React, { Component } from 'react';
import { PaginationItem, PaginationLink, Pagination } from 'reactstrap';

class PaginationButtons extends Component {
  render() {
    let currentPage = this.props.currentPage;

    return (
      <Pagination>
        <PaginationItem disabled={currentPage <= 1}>
          <PaginationLink first onClick={e => this.props.handleClick(1)}> {'<<'} </PaginationLink>
        </PaginationItem>
        <PaginationItem disabled={currentPage <= 1}>
          <PaginationLink onClick={e => this.props.handleClick(currentPage - 1)}> {'<'} </PaginationLink>
        </PaginationItem>

        {currentPage > 1 ?
        <PaginationItem>
          <PaginationLink onClick={e => this.props.handleClick(currentPage - 1)}>
            {currentPage - 1}
          </PaginationLink>
        </PaginationItem>
        :
        ""
        }

        <PaginationItem active={true}>
          <PaginationLink onClick={e => this.props.handleClick(currentPage)}>
            {currentPage}
          </PaginationLink>
        </PaginationItem>

        {currentPage < this.props.pagesCount ?
        <PaginationItem>
          <PaginationLink onClick={e => this.props.handleClick(currentPage + 1)}>
            {currentPage + 1}
          </PaginationLink>
        </PaginationItem>
        :
        ""
        }

        <PaginationItem disabled={currentPage >= this.props.pagesCount}>
          <PaginationLink onClick={e => this.props.handleClick(currentPage + 1)}> {'>'} </PaginationLink>
        </PaginationItem>
        <PaginationItem disabled={currentPage >= this.props.pagesCount}>
          <PaginationLink onClick={e => this.props.handleClick(this.props.pagesCount)}> {'>>'} </PaginationLink>
        </PaginationItem>
      </Pagination>
    );
  }
}

export default PaginationButtons;