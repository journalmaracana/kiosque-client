import Swal from "sweetalert2";
//----------------- Alert Delete--------------------
export const deleteConfirmationAlert = (msg) => {
  return Swal.fire({
    title: "Confirmation",
    text: msg,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#008242",
    cancelButtonColor: "#EF004B",
    confirmButtonText: "Oui",
    cancelButtonText: "Annuler",
  });
};
export const alert = (title, message, icon) => {
  return Swal.fire({
    position: "center",
    title,
    text: message,
    icon,
    showConfirmButton: true,
  });
};
