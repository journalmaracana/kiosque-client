import { TiPlusOutline } from "react-icons/ti";

import styles from "./Containers.module.css";
import { Container } from "reactstrap";

export const FormContainer = (props) => {
  return (
    <div
      className={`${styles["container-form"]} shadow-sm  rounded-15 bg-white p-3`}
      style={props.style}
    >
      <div
        className={`${styles["container-form-icon"]} bg-white text-blue text-center rounded-pill`}
      >
        {props.image ? props.image : <TiPlusOutline size={90} />}
        <div className="font-weight-bold"> {props.title} </div>
      </div>
      {props.children}
    </div>
  );
};

export const MainContainer = (props) => {
  return (
    <div className={styles["container-main"]} style={props.style}>
      <div
        className="container-main-opacity"
        onClick={() =>
          (document.getElementsByClassName("container-main")[0].style.display =
            "none")
        }
      ></div>
      {props.children}
    </div>
  );
};

export const MediumContainer = (props) => {
  return (
    <Container className="container-md" style={props.style}>
      {props.children}
    </Container>
  );
};

export const SmallContainer = (props) => {
  return (
    <Container style={{ width: "50%" }} className={props.className}>
      {props.children}
    </Container>
  );
};
