import React, { Component } from 'react';

class PhTable extends Component {
	render() {
		const p = this.props;

		return(
			<table width="100%" className="ph-table mt-2">
				<thead>
					<tr className="shadow-sm">
						{p.headData.map((data, key) => {
							return(
								<th key={key}> {data} </th>
							);
						})}
					</tr>
				</thead>
				<tbody>
					{p.bodyData.map((data, key) => {
						return(
							<tr className="shadow-sm" key={key}>
								{data.map((d, k) => {
									return(
										<td key={k}> {d} </td>
									);
								})}
							</tr>
						);
					})}
				</tbody>
			</table>
		);
	}
}

export default PhTable;