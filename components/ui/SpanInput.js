export const SpanInput = (props) => {
  return (
    <span
      style={{
        top: "2.6rem",
        cursor: "pointer",
        fontSize: "0.7em",
        fontWeight: "bold",
        color: "#23468c",
        ...props.style,
      }}
      className="position-absolute "
      onClick={props.clicked}
      hidden={props.hidden}
    >
      {props.text}
    </span>
  );
};
