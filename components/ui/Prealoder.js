import React from "react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import styles from "./Preloader.module.css";

const Preloader = () => {
  return (
    <div className={styles["preloader"]}>
      <div className={styles["preloader-icon"]}>
        <AiOutlineLoading3Quarters size={40} className="ld ld-spin" />
      </div>
    </div>
  );
};

export default Preloader;
