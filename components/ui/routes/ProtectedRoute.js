import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Preloader from "../Prealoder";

function ProtectedRoute(props) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    if (!props.isAuth) {
      router.replace("/login");
    }
    setLoading(false);
  }, [props.isAuth]);
  if (loading) {
    return <Preloader />;
  }
  return <div>{props.children}</div>;
}

export default ProtectedRoute;
