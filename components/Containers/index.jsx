import React from 'react';
import { TiPlusOutline } from 'react-icons/ti';

import { Container } from 'reactstrap';
import styles from '../../styles/login.module.css'
export const FormContainer = (props) => {
  return(
    <div className={styles.container}>

    <div className="shadow-sm container-form rounded-15 bg-white p-3" style={props.style}>
      <div className="container-form-icon bg-white text-blue text-center rounded-pill">
        <TiPlusOutline size={90} />
        <div className="font-weight-bold"> {props.title} </div>
      </div>
      {props.children}
    </div>
    </div>
  );
}

export const MainContainer = (props) => {
  return(
    <div className={styles.container}>

    <div className="container-main" style={props.style}>
      <div className="container-main-opacity" onClick={() => document.getElementsByClassName('container-main')[0].style.display = 'none'}></div>
      {props.children}
    </div>
    </div>
  );
}

export const MediumContainer = (props) => {
  return(
    <div className={styles.container}>

    <Container className="container-md" style={props.style}>
      {props.children}
    </Container>
    </div>
  );
}

export const SmallContainer = (props) => {
  return(
    <div className={styles.container}>

    <Container style={{width: '50%'}} className={props.className}>
      {props.children}
    </Container>
    </div>
  );
}