import React, { Component, useState } from "react";
import { Row, Col, Input, Button } from "reactstrap";
import Link from "next/link";
import { AiOutlineStar } from "react-icons/ai";
import { FiMinus, FiMoreVertical } from "react-icons/fi";
import { MdClose, MdReply, MdSend } from "react-icons/md";
import { BsArrowLeft } from "react-icons/bs";

export const MessageView = (props) => {
  return (
    <div className="dashboard-inbox-message mt-2">
      <Row className="mx-0">
        <Col xs="4" className="pl-4">
          <h6 className="mb-0">
            <Input type="checkbox" />
            <AiOutlineStar className="ml-2" color="#ccc" />
            <Link href="/dashboard/inbox?openid=123">
              <a className="text-blue ml-1">SebaCyriel</a>
            </Link>
          </h6>
        </Col>
        <Col xs="1">
          <div className="bg-success rounded-pill font-size-12 text-white text-center">
            {" "}
            {"dashboard.inbox.new"}{" "}
          </div>
        </Col>
        <Col xs="5">
          <div className="dashboard-inbox-message-subject">
            <b>Sujet: </b> J'ai besoin d'un médicament
          </div>
        </Col>
        <Col xs="2" className="text-right dashboard-inbox-message-label px-0">
          <Link
            href="/dashboard/inbox?openid=123"
            className="text-blue font-weight-bold pr-2"
          >
            <a> 10:05</a>
          </Link>
        </Col>
      </Row>
    </div>
  );
};

export const NotificationView = (props) => {
  return (
    <div className="dashboard-inbox-message mt-2">
      <Row className="mx-0">
        <Col xs="4" className="pl-4">
          <h6 className="mb-0">
            <Input type="checkbox" />
            <Link href="/dashboard/notification/id">
              <a className="text-blue ml-3">Seba Cyriel</a>
            </Link>
          </h6>
        </Col>
        <Col xs="1">
          <div className="bg-success rounded-pill font-size-12 text-white text-center">
            {" "}
            {"dashboard.inbox.new"}{" "}
          </div>
        </Col>
        <Col xs="5">
          <div className="dashboard-inbox-message-subject">
            content for notification.
          </div>
        </Col>
        <Col xs="2" className="text-right dashboard-inbox-message-label px-0">
          <Link
            href="/dashboard/notification/id"
            className="text-blue font-weight-bold pr-2"
          >
            <a>10:05</a>
          </Link>
        </Col>
      </Row>
    </div>
  );
};

export const NewMessageView = (props) => {
  const [size, setSize] = useState(true);
  const [view, setView] = useState(props.hidden);

  return (
    <div
      className="position-fixed inbox-send-message shadow bg-white"
      style={{ display: props.hidden ? "none" : "block" }}
    >
      <div className="bg-blue text-white p-2">
        <Row>
          <Col xs="9">
            <span className="font-size-14 font-weight-bold">
              {"dashboard.inbox.new_message"}
            </span>
          </Col>
          <Col xs="3" className="text-right">
            <FiMinus onClick={() => setSize(!size)} />
            &nbsp;&nbsp;
            <MdClose onClick={() => setView(!view)} />
          </Col>
        </Row>
      </div>
      <div className="px-3" style={{ display: size ? "block" : "none" }}>
        <div>
          <Input type="text" size="sm" />
        </div>
        <div>
          <Input type="text" size="sm" />
        </div>
        <div>
          <Input type="textarea" rows="8" />
        </div>
        <div className="text-right mb-2">
          <Button color="blue" size="sm">
            {" "}
            {"buttons.send"}{" "}
          </Button>
        </div>
      </div>
    </div>
  );
};

export const ReadMessageView = (props) => {
  return (
    <div className="p-2">
      <h4 className="text-blue">
        <BsArrowLeft
          className="read-message-return"
          size={25}
          color="black"
          onClick={() => props.returnFunction()}
        />{" "}
        Sujet de message
      </h4>
      <div className="read-message-space">
        <div className="mt-3">
          <div className="read-message-profilepic float-left">
            <img
              src={"/images/user.png"}
              width="40px"
              height="40px"
              className="rounded-pill"
            />
          </div>
          <h5 className="text-blue pt-2 float-left ml-2">Seba Cyriel</h5>
          <div className="float-right">
            <AiOutlineStar size={25} className="text-blue" />
            &nbsp;&nbsp;
            <MdReply size={28} className="text-blue" />
            &nbsp;&nbsp;
            <FiMoreVertical size={25} className="text-blue" />
          </div>
          <div className="clearfix"></div>
        </div>
        <div className="read-message-text p-2">
          <p>
            Le Lorem Ipsum est simplement du faux texte employé dans la
            composition et la mise en page avant impression. Le Lorem Ipsum est
            le faux texte standard de l'imprimerie depuis les années 1500
          </p>
        </div>
        <div className="read-message-time text-blue font-size-12">
          3 May 2020, 23:58
        </div>
      </div>
      <div className="read-message-reply-space border rounded-12">
        <Input type="textarea" className="border-0" rows="1" />
        <MdSend className="mt-2 mr-2 text-muted" size={22} />
      </div>
    </div>
  );
};
