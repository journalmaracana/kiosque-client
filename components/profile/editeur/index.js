import React, { Component } from "react";
import { EditeurProfileCmp } from "../profileForm";
import { Form, FormGroup, Button } from "reactstrap";
import { FormContainer } from "../../ui/Containers";
import styles from "../../Auth/RegisterForm.module.css";
import Head from "next/head";
import Axios from "../../../utils/axios_config";
import SimpleReactValidator from "simple-react-validator";
import { VALIDATOR_MESSAGES } from "../../../utils/constants";
import { checkData } from "../../../utils/functions";
import { alert } from "../../ui/alertSwal/alertSwal";
import Preloader from "../../ui/Prealoder";
import { ImagePicker } from "../utilis";
import { API_LINK } from "../../../utils/constant";
export default class EditeurProfile extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  state = {
    user: {},
    editor: "",
    editorAR: "",
    mail: "",
    site: "",
    adress: "",
    type: "Editeur",
    codeFix: "",
    phoneFix: "",
    codeFax: "",
    phoneFax: "",
    codeMobile: "",
    phoneMobile: "",
    contact: "",
    telContact: "",
    codeContact: "",
    editorCtrl: false,
    editorARCtrl: false,
    mailCtrl: false,
    siteCtrl: false,
    adressCtrl: false,
    fixCtrl: false,
    faxCtrl: false,
    mobileCtrl: false,
    contactCtrl: false,
    telContactCtrl: false,
    loading: false,
  };
  componentDidMount = () => {
    this.fetchUser("6087fff26f628640e75e28fd");
  };
  updateState = (data) => {
    this.setState({
      user: { ...data },
      ...data,
      phoneFix: data.phoneFix ? data.phoneFix.split("-")[1] : "",
      codeFix: data.phoneFix ? data.phoneFix.split("-")[0] : "",
      phoneFax: data.phoneFax ? data.phoneFax.split("-")[1] : "",
      codeFax: data.phoneFax ? data.phoneFax.split("-")[0] : "",
      phoneMobile: data.phonemobile ? data.phonemobile.split("-")[1] : "",
      codeMobile: data.phonemobile ? data.phonemobile.split("-")[0] : "",
      telContact: data.telContact ? data.telContact.split("-")[1] : "",
      codeContact: data.telContact ? data.telContact.split("-")[0] : "",
      image: data.image ? data.image : "",

      editorCtrl: checkData(data.editor),
      editorARCtrl: checkData(data.editorAR),
      mailCtrl: checkData(data.mail),
      adressCtrl: checkData(data.adress),
      siteCtrl: checkData(data.site),
      contactCtrl: checkData(data.contact),
      fixCtrl: checkData(data.phoneFix),
      faxCtrl: checkData(data.phoneFax),
      mobileCtrl: checkData(data.phonemobile),
      telContactCtrl: checkData(data.telContact),

      loading: false,
    });
  };
  fetchUser = (id) => {
    this.setState({
      loading: true,
    });
    Axios.get("/user/" + id)
      .then(({ data }) => {
        this.updateState(data);
      })
      .catch((err) => {
        alert("Error", "Oops..! " + err, "error");
        this.setState({
          loading: false,
        });
      });
  };
  onSubmit = () => {
    if (this.validator.allValid()) {
      this.setState({
        loading: true,
      });
      const {
        mail,
        editorAR,
        editor,
        site,
        adress,
        pays,
        ville,
        type,
        codeFix,
        phoneFix,
        codeFax,
        phoneFax,
        codeMobile,
        phoneMobile,
        contact,
        telContact,
        codeContact,
      } = this.state;
      const user = {
        editor,
        editorAR,
        mail,
        site,
        adress,
        pays,
        ville,
        contact,
        type,
        phoneFix: codeFix + "-" + phoneFix,
        phoneFax: codeFax + "-" + phoneFax,
        phonemobile: codeMobile + "-" + phoneMobile,
        telContact: codeContact + "-" + telContact,
      };

      const userData = new FormData();
      for (const [key, value] of Object.entries(user)) {
        userData.append(key, value);
      }
      Axios.put("editeuser/6087fff26f628640e75e28fd", userData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
        .then((res) => {
          alert(
            "Confirmation",
            "cotre compte a été modifier avec succes",
            "success"
          );
          this.updateState(res.data);
        })
        .catch((err) => {
          alert("Error", "Oops..! " + err, "error");
          this.setState({
            loading: false,
          });
        });
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };
  handelChangeSpanInputCtrl = (id) => {
    this.setState({
      [id]: false,
    });
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleChangeSelect = (e, id) => {
    this.setState({
      [id]: e.value,
    });
  };

  handleChangeImage = (imagePath) => {
    this.setState({
      image: imagePath,
    });
  };

  render() {
    if (this.state.loading) {
      return <Preloader />;
    }
    return (
      <div>
        <Head>
          <title>profile editeur</title>
        </Head>
        <FormContainer
          style={{ width: "60%" }}
          title="editeur"
          image={
            <ImagePicker
              handleChangeImage={this.handleChangeImage}
              src={API_LINK + this.state.image}
              user={this.state.user}
            />
          }
        >
          <Form className={`${styles["register-form"]} mt-5`}>
            <EditeurProfileCmp
              validator={this.validator}
              data={this.state}
              handleChange={this.handleChange}
              spanInputCtrl={this.handelChangeSpanInputCtrl}
              handleChangeSelect={this.handleChangeSelect}
            />
            <FormGroup className="text-center">
              <Button
                color="blue"
                className="rounded-12 px-5 font-weight-bold"
                onClick={this.onSubmit}
              >
                {"Enregistrer"}{" "}
              </Button>
            </FormGroup>
          </Form>
        </FormContainer>
      </div>
    );
  }
}
