import React from "react";
import { Form, FormGroup, Button, Label, Row, Col, Input } from "reactstrap";
import styles from "../Auth/RegisterForm.module.css";
import { CountryCmp, ProfileInput } from "./utilis";
import { SpanInput } from "../ui/SpanInput";
import Select from "react-select";
import { getYears, getDays, getMonth } from "../../utils/functions";
import { PHONE_CODE, PAYS_OPTIONS } from "../../utils/constant";
export function ProfileCmp(props) {
  console.log("peops", props.data);
  const {
    name,
    lastname,
    nameAR,
    lastnameAR,
    mail,
    pseudo,
    pseudoAR,
    sexe,
    year,
    day,
    month,
    type,
    phoneMobile,
    codeMobile,
    ville,
    pays,
    facebook,
    twitter,
    instagram,
    nameCtrl,
    lastnameCtrl,
    nameARCtrl,
    lastnameARCtrl,
    pseudoCtrl,
    pseudoARCtrl,
    phoneCtrl,
    mailCtrl,
    fbCtrl,
    instaCtl,
    twitterCtrl,
    description,
    descriptionCtrl,
  } = props.data;

  return (
    <>
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Cevelity"} : </Label>
        <Row className={styles["register-gender-select"]}>
          <Col xs="6">
            <Button
              onClick={() => props.handleChangeSexe("monsieur")}
              type="button"
              block
              outline
              className={`${sexe === "monsieur" ? "active" : ""}`}
            >
              {"Monsieur"}
            </Button>
          </Col>
          <Col xs="6">
            <Button
              onClick={() => handleChangeSexe("monsieur")}
              type="button"
              block
              outline
              className={`${sexe === "madame" ? "active" : ""}`}
              onClick={() => props.handleChangeSexe("madame")}
            >
              {"Madame"}
            </Button>
          </Col>
        </Row>
        <span style={{ color: "red" }}>
          {props.validator.message("sexe", sexe, "required")}
        </span>
      </FormGroup>
      <Row>
        <Col xs="6">
          <ProfileInput
            key="lastname"
            label={"Nom :"}
            inputName="lastname"
            inputValue={lastname}
            handleChange={props.handleChange}
            spanInputValue={lastnameCtrl}
            spanInputCtrl={() => props.spanInputCtrl("lastnameCtrl")}
            validator={props.validator.message("nom", lastname, "required")}
          />
        </Col>
        <Col xs="6">
          <ProfileInput
            key="name"
            label={"Prenom :"}
            inputName="name"
            inputValue={name}
            handleChange={props.handleChange}
            spanInputValue={nameCtrl}
            spanInputCtrl={() => props.spanInputCtrl("nameCtrl")}
            validator={props.validator.message("prenom", name, "required")}
          />
        </Col>
      </Row>

      <Row>
        <Col xs="6">
          <ProfileInput
            key="lastnameAR"
            label={": اللقب"}
            inputName="lastnameAR"
            inputValue={lastnameAR}
            handleChange={props.handleChange}
            spanInputValue={lastnameARCtrl}
            spanInputCtrl={() => props.spanInputCtrl("lastnameARCtrl")}
            //validator={props.validator.message("nom", lastnameAR, "required")}
          />
        </Col>
        <Col x6="6">
          <ProfileInput
            key="nameAR"
            label={": الإ سم"}
            inputName="nameAR"
            inputValue={nameAR}
            handleChange={props.handleChange}
            spanInputValue={nameARCtrl}
            spanInputCtrl={() => props.spanInputCtrl("nameARCtrl")}
            validator={props.validator.message("prenom", nameAR, "required")}
          />
        </Col>
      </Row>
      <Row>
        <Col xs="6">
          <ProfileInput
            key="pseudo"
            label={
              type === "Auteur " ? "Nom d'auteur :" : "Nom d'utilisateur :"
            }
            inputName="pseudo"
            inputValue={pseudo}
            handleChange={props.handleChange}
            spanInputValue={pseudoCtrl}
            spanInputCtrl={() => props.spanInputCtrl("pseudoCtrl")}
            // validator={props.validator.message(
            //   type === "Auteur " ? "Nom d'auteur :" : "Pseudo :",
            //   pseudo,
            //   "required"
            // )}
          />
        </Col>
        <Col xs="6">
          <ProfileInput
            key="pseudoAR"
            label={type === "Auteur" ? ": إسم المؤلف : " : "إسم المستخدم "}
            inputName="pseudoAR"
            inputValue={pseudoAR}
            handleChange={props.handleChange}
            spanInputValue={pseudoARCtrl}
            spanInputCtrl={() => props.spanInputCtrl("pseudoARCtrl")}
            // validator={props.validator.message(
            //   type === "Auteur" ? ": إسم المؤلف : " : "إسم المستخدم ",
            //   pseudoAR,
            //   "required"
            // )}
          />
        </Col>
      </Row>

      <FormGroup>
        <Label className="text-blue font-weight-bold">
          {" "}
          {"Date de naissance"} :{" "}
        </Label>
        <Row>
          <Col xs="4">
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder={"Jour"}
              isSearchable
              name="day"
              id="day"
              options={getDays(month, year)}
              value={getDays(month, year).filter(({ value }) => value == day)}
              onChange={(e) => props.handleChangeSelect(e, "day")}
            />
            <span style={{ color: "red" }}>
              {props.validator.message(
                "jour",
                getDays(month, year).filter(({ value }) => value == day),
                "required"
              )}
            </span>
          </Col>
          <Col xs="4">
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder={"Mois"}
              isSearchable
              name="month"
              id="month"
              options={getMonth()}
              value={getMonth().filter(({ value }) => value === month)}
              onChange={(e) => props.handleChangeSelect(e, "month")}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("mois", month, "required")}
            </span>
          </Col>
          <Col xs="4">
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder={"Année"}
              isSearchable
              name="year"
              id="year"
              options={getYears()}
              value={getYears().filter(({ value }) => value == year)}
              onChange={(e) => props.handleChangeSelect(e, "year")}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("année", year, "required")}
            </span>
          </Col>
        </Row>
      </FormGroup>
      {type === "Auteur" ? (
        <ProfileInput
          key="description"
          label={"Description :"}
          inputName="description"
          inputValue={description}
          inputType={"textarea"}
          handleChange={props.handleChange}
          spanInputValue={descriptionCtrl}
          spanInputCtrl={() => props.spanInputCtrl("descriptionCtrl")}
          // validator={props.validator.message("description", description, "required")}
        />
      ) : null}

      <ProfileInput
        key="mail"
        label={"Email :"}
        inputName="mail"
        inputValue={mail}
        handleChange={props.handleChange}
        spanInputValue={mailCtrl}
        spanInputCtrl={() => props.spanInputCtrl("mailCtrl")}
        validator={props.validator.message("Email", mail, "required|email")}
      />
      {type === "Auteur" ? (
        <>
          <ProfileInput
            key="fb"
            label={"Facebook :"}
            inputName="facebook"
            inputValue={facebook}
            handleChange={props.handleChange}
            spanInputValue={fbCtrl}
            spanInputCtrl={() => props.spanInputCtrl("fbCtrl")}
            validator={props.validator.message("Facebook", facebook, "url")}
          />
          <ProfileInput
            key="twitter"
            label={"Twitter :"}
            inputName="twitter"
            inputValue={twitter}
            handleChange={props.handleChange}
            spanInputValue={twitterCtrl}
            spanInputCtrl={() => props.spanInputCtrl("twitterCtrl")}
            validator={props.validator.message("Twitter", twitter, "url")}
          />
          <ProfileInput
            key="instagram"
            label={"Instagram :"}
            inputName="instagram"
            inputValue={instagram}
            handleChange={props.handleChange}
            spanInputValue={instaCtl}
            spanInputCtrl={() => props.spanInputCtrl("instaCtl")}
            validator={props.validator.message("Instagram", instagram, "url")}
          />
        </>
      ) : null}
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Mobile"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeMobile"
              id="codeMobile"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeMobile)}
              onChange={(e) => props.handleChangeSelect(e, "codeMobile")}
            />
          </Col>
          <Col xs="9" className="position-relative">
            <Input
              type="tel"
              name="phoneMobile"
              value={phoneMobile}
              onChange={props.handleChange}
              disabled={phoneCtrl}
            />
            <SpanInput
              key="phoneCtrl"
              style={{ right: "1.8rem", top: "0.6rem" }}
              clicked={() => props.spanInputCtrl("phoneCtrl")}
              text={"Modifier"}
              hidden={!phoneCtrl}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("mobile", phoneMobile, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>

      <CountryCmp
        pays={pays}
        ville={ville}
        handleChange={props.handleChangeSelect}
      />
    </>
  );
}
export function EditeurProfileCmp(props) {
  const {
    mail,
    mailCtrl,
    editorAR,
    editorARCtrl,
    editor,
    editorCtrl,
    site,
    siteCtrl,
    adress,
    adressCtrl,
    pays,
    ville,
    codeFix,
    phoneFix,
    fixCtrl,
    codeFax,
    phoneFax,
    faxCtrl,
    codeMobile,
    phoneMobile,
    mobileCtrl,
    contact,
    contactCtrl,
    telContact,
    telContactCtrl,
    codeContact,
  } = props.data;
  return (
    <>
      <ProfileInput
        key="editor"
        label={"Nom de l'editeur :"}
        inputName="editor"
        inputValue={editor}
        handleChange={props.handleChange}
        spanInputValue={editorCtrl}
        spanInputCtrl={() => props.spanInputCtrl("editorCtrl")}
        validator={props.validator.message("Nom editeur", editor, "required")}
      />
      <ProfileInput
        key="editorAR"
        label={": اسم المحرر "}
        inputName="editorAR"
        inputValue={editorAR}
        handleChange={props.handleChange}
        spanInputValue={editorARCtrl}
        spanInputCtrl={() => props.spanInputCtrl("editorARCtrl")}
        // validator={props.validator.message("اسم المحرر", editorAR, "required")}
      />
      <ProfileInput
        key="site"
        label={"site :"}
        inputName="site"
        inputValue={site}
        handleChange={props.handleChange}
        spanInputValue={siteCtrl}
        spanInputCtrl={() => props.spanInputCtrl("siteCtrl")}
        validator={props.validator.message("site", site, "url")}
      />
      <ProfileInput
        key="adress"
        label={"Adress:"}
        inputName="adress"
        inputValue={adress}
        handleChange={props.handleChange}
        spanInputValue={adressCtrl}
        spanInputCtrl={() => props.spanInputCtrl("adressCtrl")}
        validator={props.validator.message("Adress", adress, "required")}
      />
      <CountryCmp
        pays={pays}
        ville={ville}
        handleChange={props.handleChangeSelect}
      />
      <ProfileInput
        key="mail"
        label={"Email :"}
        inputName="mail"
        inputValue={mail}
        handleChange={props.handleChange}
        spanInputValue={mailCtrl}
        spanInputCtrl={() => props.spanInputCtrl("mailCtrl")}
        validator={props.validator.message("Email", mail, "required|email")}
      />
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Fix "} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeFix"
              id="codeFix"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeFix)}
              onChange={(e) => props.handleChangeSelect(e, "codeFix")}
            />
          </Col>
          <Col xs="9" className="position-relative">
            <Input
              type="tel"
              name="phoneFix"
              value={phoneFix}
              onChange={props.handleChange}
              disabled={fixCtrl}
            />
            <SpanInput
              key="fixCtrl"
              style={{ right: "1.8rem", top: "0.6rem" }}
              clicked={() => props.spanInputCtrl("fixCtrl")}
              text={"Modifier"}
              hidden={!fixCtrl}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("Fix", phoneFix, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>
      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Fax"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeFax"
              id="codeFax"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeFax)}
              onChange={(e) => props.handleChangeSelect(e, "codeFax")}
            />
          </Col>
          <Col xs="9" className="position-relative">
            <Input
              type="tel"
              name="phoneFax"
              value={phoneFax}
              onChange={props.handleChange}
              disabled={faxCtrl}
            />
            <SpanInput
              key="faxCtrl"
              style={{ right: "1.8rem", top: "0.6rem" }}
              clicked={() => props.spanInputCtrl("faxCtrl")}
              text={"Modifier"}
              hidden={!faxCtrl}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("Fax", phoneFax, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>

      <FormGroup>
        <Label className="text-blue font-weight-bold"> {"Mobile"} : </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeMobile"
              id="codeMobile"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeMobile)}
              onChange={(e) => props.handleChangeSelect(e, "codeMobile")}
            />
          </Col>
          <Col xs="9" className="position-relative">
            <Input
              type="tel"
              name="phoneMobile"
              value={phoneMobile}
              onChange={props.handleChange}
              disabled={mobileCtrl}
            />
            <SpanInput
              key="mobileCtrl"
              style={{ right: "1.8rem", top: "0.6" }}
              clicked={() => props.spanInputCtrl("mobileCtrl")}
              text={"Modifier"}
              hidden={!mobileCtrl}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("mobile", phoneMobile, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>

      <ProfileInput
        key="contact"
        label={"Contact :"}
        inputName="contact"
        inputValue={contact}
        handleChange={props.handleChange}
        spanInputValue={contactCtrl}
        spanInputCtrl={() => props.spanInputCtrl("contactCtrl")}
        //validator={props.validator.message("contact", contact, "required")}
      />
      <FormGroup>
        <Label className="text-blue font-weight-bold">
          {" "}
          {"Telephone contact"} :{" "}
        </Label>
        <Row>
          <Col xs="3">
            <Select
              className="basic-single"
              classNamePrefix="select"
              isSearchable
              name="codeContact"
              id="codeContact"
              options={PHONE_CODE}
              value={PHONE_CODE.filter(({ value }) => value == codeContact)}
              onChange={(e) => props.handleChangeSelect(e, "codeContact")}
            />
          </Col>
          <Col xs="9" className="position-relative">
            <Input
              type="tel"
              name="telContact"
              value={telContact}
              onChange={props.handleChange}
              disabled={telContactCtrl}
            />
            <SpanInput
              key="telContactCtrl"
              style={{ right: "1.8rem", top: "0.6" }}
              clicked={() => props.spanInputCtrl("telContactCtrl")}
              text={"Modifier"}
              hidden={!telContactCtrl}
            />
            <span style={{ color: "red" }}>
              {props.validator.message("mobile", telContact, "phone")}
            </span>
          </Col>
        </Row>
      </FormGroup>
    </>
  );
}
