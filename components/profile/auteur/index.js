import React, { Component } from "react";
import { ProfileCmp } from "../profileForm";
import { Form, FormGroup, Button } from "reactstrap";
import { FormContainer } from "../../ui/Containers";
import styles from "../../Auth/RegisterForm.module.css";
import Head from "next/head";
import Axios from "../../../utils/axios_config";
import SimpleReactValidator from "simple-react-validator";
import { VALIDATOR_MESSAGES } from "../../../utils/constants";
import { checkData } from "../../../utils/functions";
import { alert } from "../../ui/alertSwal/alertSwal";
import Preloader from "../../ui/Prealoder";
import { ImagePicker } from "../utilis";
import { API_LINK } from "../../../utils/constant";

export default class AuteurProfile extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  state = {
    user: {},
    lastname: "",
    name: "",
    lastname: "",
    mail: "",
    pseudo: "",
    pseudoAR: "",
    sexe: "",
    year: "",
    day: "",
    month: "",
    type: "Auteur",
    adress: "",
    facebook: "",
    twitter: "",
    instagram: "",

    phoneMobile: "",
    codeMobile: "",
    ville: "",
    pays: "",
    image: "",

    nameCtrl: false,
    lastnameCtrl: false,

    nameARCtrl: false,
    lastnameARCtrl: false,
    pseudoCtrl: false,
    pseudoARCtrl: false,
    adressCtrl: false,
    phoneCtrl: false,
    mailCtrl: false,
    fbCtrl: false,
    instaCtl: false,
    twitterCtrl: false,
    description: "",
    descriptionCtrl: false,

    loading: false,
  };
  componentDidMount = () => {
    this.fetchUser("6087d0da6f628640e75e28ee");
  };

  updateState = (data) => {
    this.setState({
      user: { ...data },
      ...data,
      year: data.date ? data.date.substring(0, 4) : "",
      day: data.date ? data.date.substring(8, 10) : "",
      month: data.date ? data.date.substring(5, 7) : "",
      phoneMobile: data.phonemobile ? data.phonemobile.split("-")[1] : "",
      codeMobile: data.phonemobile ? data.phonemobile.split("-")[0] : "",
      image: data.image ? data.image : "",
      nameCtrl: checkData(data.name),
      lastnameCtrl: checkData(data.lastname),
      nameARCtrl: checkData(data.nameAR),
      lastnameARCtrl: checkData(data.lastnameAR),
      pseudoCtrl: checkData(data.pseudo),
      pseudoARCtrl: checkData(data.pseudoAR),
      adressCtrl: checkData(data.adress),
      phoneCtrl: checkData(data.phone),
      mailCtrl: checkData(data.mail),
      fbCtrl: checkData(data.facebook),
      twitterCtrl: checkData(data.twitter),
      instaCtl: checkData(data.instagram),
      descriptionCtrl: checkData(data.description),
      loading: false,
    });
  };
  fetchUser = (id) => {
    this.setState({
      loading: true,
    });
    Axios.get("/user/" + id)
      .then(({ data }) => {
        this.updateState(data);
      })
      .catch((err) => {
        alert("Error", "Oops..! " + err, "error");
        this.setState({
          loading: false,
        });
      });
  };
  onSubmit = () => {
    if (this.validator.allValid()) {
      this.setState({
        loading: true,
      });
      const {
        name,
        lastname,
        nameAR,
        lastnameAR,
        mail,
        pseudo,
        pseudoAR,
        sexe,
        year,
        day,
        month,
        type,
        phoneMobile,
        codeMobile,
        ville,
        pays,
        image,
        facebook,
        twitter,
        instagram,
        description,
      } = this.state;
      const user = {
        name,
        lastname,
        nameAR,
        lastnameAR,
        mail,
        pseudo,
        pseudoAR,
        sexe,
        image,
        date: year + "-" + month + "-" + day,
        phonemobile: codeMobile + "-" + phoneMobile,
        ville,
        pays,
        type,
        facebook,
        twitter,
        instagram,
        description,
      };

      const userData = new FormData();
      for (const [key, value] of Object.entries(user)) {
        userData.append(key, value);
      }
      Axios.put("editeuser/6087d0da6f628640e75e28ee", userData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
        .then((res) => {
          alert(
            "Confirmation",
            "cotre compte a été modifier avec succes",
            "success"
          );
          this.updateState(res.data);
        })
        .catch((err) => {
          alert("Error", "Oops..! " + err, "error");
          this.setState({
            loading: false,
          });
        });
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };
  handelChangeSpanInputCtrl = (id) => {
    this.setState({
      [id]: false,
    });
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleChangeSelect = (e, id) => {
    this.setState({
      [id]: e.value,
    });
  };
  handleChangeSexe = (sexe) => {
    this.setState({
      sexe,
    });
  };
  handleChangeImage = (imagePath) => {
    this.setState({
      image: imagePath,
    });
  };

  render() {
    if (this.state.loading) {
      return <Preloader />;
    }
    return (
      <div>
        <Head>
          <title>profile auteur</title>
        </Head>
        <FormContainer
          style={{ width: "60%" }}
          title="AUTEUR"
          image={
            <ImagePicker
              handleChangeImage={this.handleChangeImage}
              src={API_LINK + this.state.image}
              user={this.state.user}
            />
          }
        >
          <Form className={`${styles["register-form"]} mt-5`}>
            <ProfileCmp
              validator={this.validator}
              data={this.state}
              handleChange={this.handleChange}
              spanInputCtrl={this.handelChangeSpanInputCtrl}
              handleChangeSelect={this.handleChangeSelect}
              handleChangeSexe={this.handleChangeSexe}
            />
            <FormGroup className="text-center">
              <Button
                color="blue"
                className="rounded-12 px-5 font-weight-bold"
                onClick={this.onSubmit}
              >
                {"Enregistrer"}{" "}
              </Button>
            </FormGroup>
          </Form>
        </FormContainer>
      </div>
    );
  }
}
