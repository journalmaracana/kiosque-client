import React, { useState, useCallback, useEffect } from "react";
import { Col, Row, FormGroup, Label, Input } from "reactstrap";
import Select from "react-select";
import { SpanInput } from "../ui/SpanInput";
import Image from "next/image";
import { TiCamera } from "react-icons/ti";
import { API_LINK } from "../..//utils/constant";

export const CountryCmp = (props) => {
  const { pays, ville } = props;
  return (
    <Row>
      <Col xs="6">
        <FormGroup>
          <Label className="text-blue font-weight-bold"> {"Pays"} : </Label>

          <Select
            className="basic-single"
            classNamePrefix="select"
            isSearchable
            name="pays"
            id="pays"
          />
        </FormGroup>
      </Col>
      <Col xs="6">
        <FormGroup>
          <Label className="text-blue font-weight-bold"> {"Ville"} : </Label>
          <Select
            className="basic-single"
            classNamePrefix="select"
            isSearchable
            name="ville"
            id="ville"
          />
        </FormGroup>
      </Col>
    </Row>
  );
};
export const ImagePicker = (props) => {
  const [pickedImage, setPickedImage] = useState("");
  const { src, user } = props;

  useEffect(() => {
    if (src === API_LINK) {
      setPickedImage(null);
    } else {
      setPickedImage(src);
    }
  }, [user]);
  const handleChange = (value, files) => {
    if (files) {
      generateBase64FromImage(files[0])
        .then((b64) => {
          setPickedImage(b64);
          props.handleChangeImage(files[0]);
        })
        .catch((e) => {
          setPickedImage(null);
        });
    }
  };
  // a modifier
  const ProfilePictureClick = () => {
    const elm = document.getElementById("profile_pic_input");
    elm.click();
  };

  const generateBase64FromImage = (imageFile) => {
    const reader = new FileReader();
    const promise = new Promise((resolve, reject) => {
      reader.onload = (e) => resolve(e.target.result);
      reader.onerror = (err) => reject(err);
    });

    reader.readAsDataURL(imageFile);
    return promise;
  };

  return (
    <div className="position-relative mx-auto rounded-pill overflow-hidden">
      <img
        style={{ height: "100px", width: "100px" }}
        src={pickedImage ? pickedImage : "/images/user.png"}
        alt="profile_picture"
      />

      <div onClick={() => ProfilePictureClick()}>
        <Input
          type="file"
          name="profile_pic"
          id="profile_pic_input"
          className="d-none"
          onChange={(e) => handleChange(e.target.value, e.target.files)}
          accept="image/*"
        />
        <TiCamera />
      </div>
    </div>
  );
};
export const ProfileInput = (props) => {
  const { inputName, inputValue, spanInputValue, label, inputType } = props;
  return (
    <FormGroup className={"position-relative"}>
      <Label className="text-blue font-weight-bold"> {label} </Label>

      <Input
        type={inputType ? inputType : "text"}
        name={inputName}
        value={inputValue}
        onChange={props.handleChange}
        disabled={spanInputValue}
      />
      <SpanInput
        style={{ right: "1rem" }}
        clicked={props.spanInputCtrl}
        text={"Modifier"}
        hidden={!spanInputValue}
      />
      <span style={{ color: "red" }}>{props.validator}</span>
    </FormGroup>
  );
};
