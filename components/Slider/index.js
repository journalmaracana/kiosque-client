import React, { Component } from "react";
import { Zoom } from "react-slideshow-image";

import "./Slider.module.css";

const images = ["/images/slide1.png", "/images/slide2.jpg"];

const zoomOutProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  scale: 0.4,
  arrows: true,
};

class Slider extends Component {
  render() {
    return (
      <div className="ph-slider">
        <Zoom {...zoomOutProperties}>
          {images.map((each, index) => (
            <img
              key={index}
              style={{ width: "100%", height: "460px" }}
              src={each}
              alt="Slide"
            />
          ))}
        </Zoom>
      </div>
    );
  }
}

export default Slider;
