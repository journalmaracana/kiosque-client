import React, { Component } from "react";

import {
  Pagination,
  PaginationItem,
  PaginationLink,
  Container,
  Row,
  Col,
  Button,
} from "reactstrap";
import Link from "next/link";
import styles from "../../styles/Live.module.css";

class LivrePreview extends Component {
  render() {
    const p = this.props;
    return (
      // styles["journal-apercu-overlay"]
      // className={`${styles["sidebar-newspaper-buttons"]} text-dark text-center`}

      <div
        className={`${styles["sidebar-newspaper-container sidebar-journal"]} position-relative p-4`}
      >
        <div className="">
          <div className={`${styles["journal-apercu"]} text-center`}>
            <h3
              className={`${styles["sidebar-newspaper-title"]} text-dark py-3`}
            >
              <small>{p.NomAuteur}</small>
            </h3>
            <h3
              className={`${styles["sidebar-newspaper-title"]} text-dark py-3`}
            >
              <small>{p.NomLivre}</small>
            </h3>
            <div className="row margin0"></div>
            <div className="w-100 position-relative text-center">
              <div className={styles["journal-apercu-overlay"]}></div>
              <img
                src={p.imageURL}
                width="70%"
                style={{ width: "65%!important", height: "auto" }}
              />
            </div>
            <h3
              className={`${styles["sidebar-newspaper-title"]} text-dark py-3`}
            >
              <small>{p.Cat}</small>
            </h3>
            <div
              className={`${styles["buttons-row"]} row margin0 position-absolute`}
            >
              <div className="col padding0"></div>
              <div className="col padding0"></div>
            </div>
            <div className="text-dark text-center sidebar-newspaper-buttons">
              <div
                style={{ width: "80%", margin: "auto" }}
                className="my-2"
              ></div>
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LivrePreview;
