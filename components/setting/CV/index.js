import React, { Component } from 'react';
import { Container, Row, Col, Button, Input, FormGroup } from 'reactstrap';

class SettingsCV extends Component {
	render() {
		const {lang} = this.props;

		return(
			<div>
				<h2 className="text-center text-blue mt-3"> CONFIDENTIALITE DES CV </h2>
				<br />
				<Container>
					<Row>
						<Col md="6" className="offset-md-3">
							<FormGroup>
								<Input type="radio" name="view" /> Public
								<p className="font-size-14 text-muted">
									Votre CV sera visible par tout utilisateur, conformément à nos conditions générales d'utilisation. Votre numéro de télephone et adresse email ne sont transmis qu'aux employeurs auxquels vous envoyez une candidature ou à qui vous répondez. Votre adresse postale n'est visible que par vous.
								</p>
							</FormGroup>
							<FormGroup>
								<Input type="radio" name="view" /> Privé
								<p className="font-size-14 text-muted">
									Votre CV n'est pas visible. Les employeurs ne pouvent pas trouver votre CV mais vous pouvez le joindre à votre condidature quand vous postulez à une offre d'emploi.
								</p>
							</FormGroup>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default SettingsCV;