import React, { Component } from "react";
import { Container, Row, Col, Button } from "reactstrap";
//import { getCookie, setCookie } from "../../../utils/functions";
import { AiOutlineCheck } from "react-icons/ai";

class SettingsDarkMode extends Component {
  setMode(mode) {
    if (mode === "dark") {
      setCookie("mode", "dark", 10);
      var cssId = "modeCss"; // you could encode the css path itself to generate id..
      if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName("head")[0];
        var link = document.createElement("link");
        var link2 = document.createElement("link");
        link.id = cssId;
        link.rel = "stylesheet";
        link.type = "text/css";
        link.href = "/assets/css/dark.css";
        link.media = "all";

        head.appendChild(link);
      }
    } else {
      if (document.getElementById("modeCss")) {
        setCookie("mode", "light", 10);
        document.getElementById("modeCss").remove();
      }
    }
  }

  render() {
    return (
      <div>
        <h2 className="text-center text-blue mt-3"> MODE NUIT </h2>
        <br />
        {/* <Container>
          <Row>
            <Col md="4" className="offset-md-4">
              <div
                onClick={() => this.setMode("dark")}
                className="dashboard-settings-mode-elem border-ddd border-1 py-1 px-2 mt-2"
              >
                Activé{" "}
                {getCookie("mode") === "dark" ? (
                  <span className="float-right text-blue">
                    {" "}
                    <AiOutlineCheck />{" "}
                  </span>
                ) : (
                  ""
                )}{" "}
              </div>
              <div
                onClick={() => this.setMode("light")}
                className="dashboard-settings-mode-elem border-ddd border-1 py-1 px-2 mt-2"
              >
                Désactivé{" "}
                {getCookie("mode") !== "dark" ? (
                  <span className="float-right text-blue">
                    {" "}
                    <AiOutlineCheck />{" "}
                  </span>
                ) : (
                  ""
                )}{" "}
              </div>
              <div className="dashboard-settings-mode-elem border-ddd border-1 py-1 px-2 mt-2">
                Utiliser le paramètre système
                <p className="text-muted font-size-10">
                  Utilisez le paramètre pour l'apparence du système de votre
                  appareil pour le mode lumineaux, sombre, ou automatiq
                </p>
              </div>
            </Col>
          </Row>
        </Container> */}
      </div>
    );
  }
}

export default SettingsDarkMode;
