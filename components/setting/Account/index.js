import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";

class SettingsAccount extends Component {
  render() {
    const { lang } = this.props;

    return (
      <div>
        <h2 className="text-center text-blue mt-3"> COMPTE </h2>
        <br />
        <Container>
          <Row>
            <Col md="6" className="offset-md-3">
              <Form>
                <FormGroup className="position-relative">
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {lang.t("auth.email")}:{" "}
                  </Label>
                  <Input
                    type="text"
                    value="m**************@gmail.com"
                    className="bg-ddd"
                  />
                  <span className="font-size-12 text-blue position-absolute input-span">
                    Modifier
                  </span>
                </FormGroup>
                <FormGroup className="position-relative">
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {lang.t("auth.password")}:{" "}
                  </Label>
                  <Input
                    type="password"
                    value="**********"
                    className="bg-ddd"
                  />
                  <span className="font-size-12 text-blue position-absolute input-span">
                    Modifier
                  </span>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default SettingsAccount;
