import React from "react";
import styles from "./Settings.module.css";
import {
  AiOutlineArrowRight,
  AiOutlineCheck,
  AiOutlineArrowLeft,
} from "react-icons/ai";
import Link from "next/link";

export function SettingElm(props) {
  const { text } = props;
  return (
    <div
      onClick={() => props.clicked(text)}
      className={`${styles["dashboard-settings-mode-elem"]} border-ddd border-1 py-1 px-2 mt-2 text-muted font-weight-bold`}
    >
      {text}
      <span className="float-right">
        {" "}
        <AiOutlineArrowRight />{" "}
      </span>
    </div>
  );
}

export function ConfidentialElm(props) {
  const { link, text } = props;
  console.log("link", link);
  return (
    <>
      <div>
        <Link href={"settings" + link}>
          <div
            className={`${styles["dashboard-settings-mode-elem"]} border-ddd border-1 py-1 px-2 mt-2 text-muted font-weight-bold`}
          >
            {text}
            <span className="float-right">
              {" "}
              <AiOutlineCheck className="text-blue" />{" "}
            </span>
          </div>
        </Link>
      </div>
    </>
  );
}
