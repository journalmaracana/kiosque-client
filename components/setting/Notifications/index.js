import React, { Component } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import {
  AiOutlineArrowRight,
  AiOutlineCheck,
  AiOutlineArrowLeft,
} from "react-icons/ai";
import { SettingElm } from "../Settings";

class SettingsNotifications extends Component {
  state = {
    activeTab: null,
    activeTitle: null,
  };
  settingItems = [
    "paramètres et confidentialité",
    "Mode nuit",
    "Contactez Nous",
    "Se déconnecter",
  ];
  confidentialItems = ["Paramètres", "Mot de passe", "lange"];
  render() {
    const s = this.state;

    return (
      <div>
        <br />
        <Container>
          <Row>
            <Col md="6" className="offset-md-3">
              <div>
                {this.settingItems.map((item, index) => (
                  <SettingElm key={index} text={item} />
                ))}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default SettingsNotifications;
