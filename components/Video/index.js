import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import {
  FaFacebookF,
  FaYoutube,
  FaInstagram,
  FaTwitter,
  FaEnvelope,
} from "react-icons/fa";

import "./Video.module.css";

class VideoView extends Component {
  render() {
    return (
      <div className="shadow rounded-15 video-view p-2 mt-4 bg-white">
        <div className="video-view-icon text-blue">
          {" "}
          <FaYoutube size={30} />{" "}
        </div>
        <Row>
          <Col md="4">
            <div className="video-view-image">
              <img alt="video" src={"/images/slide2.jpg"} width="100%" />
              <span className="bg-dark rounded text-white px-1">2:31</span>
            </div>
          </Col>
          <Col md="8">
            <h5 className="text-blue"> Accident vasculaire cérébral ? </h5>
            <div className="video-view-infos">
              <b className="text-blue"> Maracana Foot </b>{" "}
              <span className="text-muted"> - 15k Views - 6 Months ago </span>
            </div>
            <div className="video-view-description text-muted">
              Le Lorem Ipsum est simplement du faux texte employé dans la
              composition et la mise en page avant impression.
            </div>
            <div className="video-view-social bg-blue">
              <div className="d-flex sidebar-followus-icons">
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaFacebookF />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaYoutube />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaInstagram />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaTwitter />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaEnvelope />
                  </a>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default VideoView;
