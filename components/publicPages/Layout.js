import Header from "./Header";
import Navbar from "./Navbar";
import Footer from "./Footer";

import styles from "./Layout.module.css";
function Layout(props) {
  console.log("router", props.router);
  return (
    <div className={styles["layout-ads"]}>
      <div className={styles["layout-ads-top"]}></div>
      <div className={styles["layout-ads-left"]}></div>
      <div className={styles["layout-ads-center"]}>
        <div className={`mypage ${styles["layout-ads-container"]}`}>
          <Header {...props} />
          <Navbar />
          <div>{props.children}</div>
          <Footer />
        </div>
      </div>
      <div className={styles["layout-ads-right"]}></div>
      <div className="clearfix"></div>
    </div>
  );
}

export default Layout;
