import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  InputGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  Button,
} from "reactstrap";

import {
  FaFacebookF,
  FaYoutube,
  FaInstagram,
  FaTwitter,
  FaEnvelope,
  FaSignInAlt,
  FaAppStore,
  FaGooglePlay,
} from "react-icons/fa";
import { AiOutlineCaretRight, AiOutlineCaretLeft } from "react-icons/ai";
import Link from "next/link";
//import { setCookie, getCookie } from "../../utils/functions";

import styles from "./Footer.module.css";

function Footer(props) {
  const setLanguage = (v) => {
    setCookie("lang", v.target.value, 10);
    window.location.reload();
  };
  return (
    <div className="ph-footer mt-5 text-white">
      <Row className="m-0">
        <Col md="7" className="bg-blue">
          <Container>
            <Row>
              <Col md="4" className="pt-5 px-4">
                <h5>Numérique Pharma</h5>
                <ul>
                  <li>contact@pharma.com</li>
                  <li>+213 23 37 70 00</li>
                  <li>10 Rue Balzac, El-Biar</li>
                  <li>Alger, Algérie</li>
                </ul>
              </Col>
              <Col md="4" className="pt-5 px-4">
                <h5> {"footer.links"} </h5>
                <ul>
                  <li> {"footer.about"} </li>
                  <li> {"footer.terms"} </li>
                  <li> {"footer.privacy_policy"} </li>
                  <li> {"footer.contact_us"} </li>
                </ul>
              </Col>
              <Col md="4" className="pt-5 px-4">
                <h5> {"footer.account"} </h5>
                <ul>
                  <li>
                    {" "}
                    <Link href="/login">
                      <a className="text-white">Se connecter</a>
                    </Link>{" "}
                  </li>
                  <li>
                    {" "}
                    <Link href="/register">
                      <a className="text-white">S’inscrire</a>
                    </Link>{" "}
                  </li>
                  <li> {"footer.forget_password"} </li>
                </ul>
              </Col>
            </Row>
            <hr className="bg-white mb-0" />
            <Row>
              <Col lg="4" md="6">
                <Form>
                  <FormGroup>
                    <Label> {"footer.newsletter.title"} </Label>
                    <InputGroup
                      className="rounded-15 input-group-sm"
                      style={{ overflow: "hidden" }}
                    >
                      <Input name="" type="email" />
                      <InputGroupAddon addonType="append">
                        <InputGroupText className="bg-darkblue text-white border-0">
                          {" "}
                          <FaSignInAlt />{" "}
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                </Form>
              </Col>
              <Col lg="4" md="0"></Col>
              <Col lg="4" md="6" className="text-right d-flex">
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaFacebookF />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaYoutube />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaInstagram />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaTwitter />
                  </a>
                </div>
                <div className="head-icon rounded-pill m-2">
                  <a href="https://facebook.com">
                    <FaEnvelope />
                  </a>
                </div>
              </Col>
            </Row>
            <Row>
              <Col md="4"></Col>
              <Col md="4" className="text-center">
                <b className={`${styles["copyright_label"]}`}>
                  Developper par l'équipe Maracanafoot
                </b>
                <p className={`${styles["copyright_label"]}`}>
                  copyright &copy; 2020
                </p>
              </Col>
              <Col md="1"></Col>
              <Col md="3">
                <Input
                  type="select"
                  name="lang"
                  onChange={(v) => setLanguage(v)}
                  className="form-control-sm bg-blue text-white"
                  style={{ borderWidth: "2px" }}
                  // defaultValue={getCookie("lang") === "ar" ? "ar" : "fr"}
                >
                  <option value="fr">Français</option>
                  <option value="ar">العربية</option>
                </Input>
              </Col>
            </Row>
          </Container>
        </Col>
        <Col md="5" className="bg-darkblue">
          <h5 className="text-center pt-5"> {"footer.app.title"} </h5>
          <Row>
            <Col xs="1">
              <AiOutlineCaretLeft size={35} className="mt-5" />
            </Col>
            <Col xs="3">
              <img alt="phone" src={"/images/iphone.png"} width="140%" />
            </Col>
            <Col xs="6">
              <h5>Maracana Foot</h5>
              <p>
                app description here, app description here app description here
              </p>
              <div className={`${styles["footer-app-buttons"]} text-center`}>
                <div className="footer-app-buttons-title">Disponible Sur</div>
                <Button>
                  {" "}
                  <FaAppStore /> App Store{" "}
                </Button>
                <br />
                <Button className="mt-2">
                  {" "}
                  <FaGooglePlay /> Google Play{" "}
                </Button>
              </div>
            </Col>
            <Col xs="1">
              <AiOutlineCaretRight size={35} className="mt-5" />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default Footer;
