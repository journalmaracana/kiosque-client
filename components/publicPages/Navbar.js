import React from "react";
import Link from "next/link";
import { AiOutlineMenu } from "react-icons/ai";
import styles from "./Navbar.module.css";

function Navigation(props) {
  const toggleBar = () => {
    let elems = document.getElementsByClassName("ph-navbar-elem");
    for (let i = 0; i < elems.length; i++) {
      const element = elems[i];
      if (element.style.display === "" || element.style.display === "none") {
        element.style.display = "block";
      } else {
        element.style.display = "none";
      }
    }
  };
  return (
    <div className={styles["ph-navbar"]}>
      <div
        className={`${styles["ph-navbar-elem"]} ${styles["bars"]} position-relative `}
        onClick={() => toggleBar()}
      >
        <AiOutlineMenu />
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative active`}>
        <Link href="/presse">PRESSE</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/Actualite">ACTUALITÉ</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/">SPORTS</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/">PRESSE SPÉCIALISÉE</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/health_threads">TECHNOLOGIE</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/jobs">AUTO & MOTO</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/appels_temoignages">FÉMININ</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/appels_temoignages">CUISINE</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/appels_temoignages">TOUTES CATÉGORIES</Link>
      </div>
      <div className={`${styles["ph-navbar-elem"]} position-relative`}>
        <Link href="/appels_temoignages">LIVRES</Link>
      </div>
    </div>
  );
}

export default Navigation;
