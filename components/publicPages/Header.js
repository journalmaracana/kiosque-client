import React, { Component } from "react";
import { Row, Container, Col, Button } from "reactstrap";
import HeaderIcons from "../ui/Icons";
import SearchInput from "../ui/Search";
import { TiPlusOutline } from "react-icons/ti";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../store/actions/Auth";
//import Preloader from "../Preloader";

import styles from "./Header.module.css";
import Link from "next/link";

function Header(props) {
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);
  const isAuth = useSelector((state) => !!state.auth.token);
  console.log("props", props);
  return (
    <div className={`${styles["ph-header"]} pb-3`}>
      <Container fluid>
        <Row>
          <Col md="4">
            <div className={`${styles["ph-header-logo"]} d-flex`}>
              <TiPlusOutline size={90} />
              <h3 className="mt-2 text-white">
                Numérique <br /> Pharma
              </h3>
            </div>
          </Col>
          <Col md="4">
            <HeaderIcons />
            <SearchInput placeholder="Recherche" />
          </Col>

          <Col md="4" className={` ${styles["ph-header-btns"]} text-right`}>
            {props.isAuth ? (
              <Button
                className="rounded-pill mt-2 text-white"
                onClick={() => dispatch(logout(props.router))}
              >
                Se Déconnecter
              </Button>
            ) : (
              <>
                <Link href="/login">
                  <Button className="rounded-pill mt-2 text-white">
                    Se connecter
                  </Button>
                </Link>
                <br />
                <Link href="/register">
                  <Button className="rounded-pill mt-2 text-white">
                    S'inscrire
                  </Button>
                </Link>
              </>
            )}
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Header;
