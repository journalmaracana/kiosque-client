import axios from "axios";
const Axios = axios.create({
  baseURL: "https://maracanafoot.online:10011/v1",
});
export default Axios;
