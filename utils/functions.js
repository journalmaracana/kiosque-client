import moment from "moment";

// export const setCookie = (name, value, days, path = '/') => {
//   let expires = '';
//   if (days) {
//     let date = new Date();
//     date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
//     expires = `; expires=${date.toUTCString()};`;
//   }
//   document.cookie = `${name}=${value}${expires}; path=${path}`;
// };

// export const getCookie = (cookieName) => {
//   if (document.cookie.length > 0) {
//     let cookieStart = document.cookie.indexOf(cookieName + '=');
//     if (cookieStart !== -1) {
//       cookieStart = cookieStart + cookieName.length + 1;
//       let cookieEnd = document.cookie.indexOf(';', cookieStart);
//       if (cookieEnd === -1) {
//           cookieEnd = document.cookie.length;
//       }
//       return window.unescape(document.cookie.substring(cookieStart, cookieEnd));
//     }
//   }
//   return '';
// };

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties,
  };
};

export const getYears = (def = 13) => {
  let dateYears = [];
  for (let index = moment().format("YYYY") - def; index >= 1930; index--) {
    dateYears.push({ value: index, label: index });
  }
  return dateYears;
};
export const getDays = (month, year) => {
  let dateDays = [];

  for (let index = 1; index <= 31; index++) {
    if (index < 10) {
      dateDays.push({ value: "0" + index, label: index });
    } else {
      dateDays.push({ value: index, label: index });
    }
  }

  if (month === "04" || month === "06" || month === "09" || month === "11") {
    dateDays = [];
    for (let index = 1; index <= 30; index++) {
      if (index < 10) {
        dateDays.push({ value: "0" + index, label: index });
      } else {
        dateDays.push({ value: index, label: index });
      }
    }
  } else if (month === "02") {
    dateDays = [];
    if (moment(year).format("YYYY") % 4 === 0) {
      for (let index = 1; index <= 29; index++) {
        if (index < 10) {
          dateDays.push({ value: "0" + index, label: index });
        } else {
          dateDays.push({ value: index, label: index });
        }
      }
    } else {
      for (let index = 1; index <= 28; index++) {
        if (index < 10) {
          dateDays.push({ value: "0" + index, label: index });
        } else {
          dateDays.push({ value: index, label: index });
        }
      }
    }
  }

  return dateDays;
};
export const getMonth = () => {
  let dateMonth = [];
  for (let index = 1; index <= 12; index++) {
    if (index < 10) {
      dateMonth.push({ value: "0" + index, label: index });
    } else {
      dateMonth.push({ value: index, label: index });
    }
  }
  return dateMonth;
};
//--------- Function powred By Madjid --------
export const webSite = 'http://localhost:3000/articles/'
import 'moment/locale/fr'  //pour la langue 
moment.locale('fr')
export const calculdatePublié = (date) => {
  if (date) {
    let date_debut = moment(date).format('YYYY-MM-DD H:mm:ss')
    let date_fin = moment(date).add(1, "days").format('YYYY-MM-DD H:mm:ss')
    // }else if(moment(date).isBetween(moment().subtract(-1, 'day'),moment().subtract(-2, 'day'))){
    if (moment(date).fromNow() === 'il y a un jour') {
        return 'hier à ' + moment(date).format('HH') + 'H' + moment(date).format('MM')
    } else if (moment().isBetween(date_debut, date_fin)) {
        return moment(date).fromNow()
    } else {
        return 'le ' + moment(date).format('DD/MM/YYYY') + ' à ' + moment(date).format('HH') + 'H' + moment(date).format('MM')
    }
  }
}
export const isEmpty = (obj) =>{
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      return false;
    }
  }
  return JSON.stringify(obj) === JSON.stringify({});
}
export const checkData = (data) => {
  if (data) {
    return true;
  } else {
    return false;
  }
};