import React, { Component, Fragment } from "react";
import { Row, Col, Container } from "reactstrap";
import { API_LINK } from "../../utils/constant";
import { ImpulseSpinner } from "react-spinners-kit";
import Pagination from "../../components/Pagination";
import AuteurPreview from "../../components/Auteur";
import ArticlePreviewMedium from "../../components/articlesPreview/mediumArticle";
import LivrePreview from "../../components/Livre";
import styles from "../../styles/Threads.module.css";
import Header from "../../components/publicPages/Header";
import NavBar from "../../components/publicPages/Navbar";
import { SideBarAct } from "../../components/Sidebar";
import Select from "react-select";
import axios from "axios";
import Preloader from "../../components/ui/Prealoder.js";
import { calculdatePublié } from "../../utils/functions";
import { withRouter } from "next/router";
import { isEmpty } from "../../utils/functions";

class Auteur extends Component {
  constructor() {
    super();
    this.state = {
      auteur: {},
      articles: [],
      livres: [],

      loading: true,
      loadingAuteur: true,
      loadingArticles: true,
      loadingLivres: true,
    };
  }
  componentDidMount = () => {
    if (this.props.router.query?.id) {
      const name = this.props.router.query?.id.split("-")[0];
      const lastname = this.props.router.query?.id.split("-")[1];

      this.getIdByName(name, lastname);
    } else {
      this.props.router.push("/404");
    }
  };
  getIdByName = (name, lastName) => {
    //test sur type user after  redux
    let urlGet = "v1/userbyname/" + name + "/" + lastName + "/" + null;
    this.setState({ loading: true });
    axios
      .get(API_LINK + urlGet)
      .then((response) => {
        this.getAuteur(response.data);
        this.getArticleByAuteur("FR", response.data);
        this.getLivresByAuteur("FR", response.data);
      })
      .catch((err) => {
        console.log(err),
          this.setState({
            loading: false,
            loadingAuteur: false,
            loadingArticles: false,
            loadingLivres: false,
          });
      });
  };
  getAuteur = (ID) => {
    this.setState({ loadingAuteur: true });
    axios
      .get(API_LINK + "v1/Auteur/" + ID)
      .then((response) => {
        this.setState({
          auteur: response.data,
          loadingAuteur: false,
        });
      })
      .catch((err) => {
        console.log(err), this.setState({ loadingAuteur: false });
      });
  };
  getArticleByAuteur = (Lang, ID) => {
    this.setState({ loadingArticles: true });
    axios
      .get(API_LINK + "v1/articlesClientSplice/language/" + Lang + "/" + ID)
      .then((response) => {
        this.setState({
          articles: response.data,
          loadingArticles: false,
        });
      })
      .catch((err) => {
        console.log(err), this.setState({ loadingArticles: false });
      });
  };

  getLivresByAuteur = (Lang, ID) => {
    this.setState({ loadingLivres: true });
    axios
      .get(API_LINK + "v1/livreClientSplice/language/" + Lang + "/" + ID)
      .then((response) => {
        this.setState({
          livres: response.data,
          loadingLivres: false,
        });
      })
      .catch((err) => {
        console.log(err), this.setState({ loadingLivres: false });
      });
  };
  render = () => {
    const {
      auteur,
      loadingAuteur,
      articles,
      loadingArticles,
      livres,
      loadingLivres,
      loading,
    } = this.state;

    return (
      <div>
        <div className="header">
          <Header />
          <NavBar />
        </div>
        <div className="threads-content">
          <Container>
            <Row className="text-center">
              <Col md="8">
                <a href="#">
                  <img src="img/pub.jpg" alt="" />
                </a>
              </Col>
              <Col md="4">
                <a href="#">
                  <img src="img/newmap.jpg" alt="" />
                </a>
              </Col>
            </Row>
            <Row>
              <Col md="8" className="partie-home-gauche">
                {!isEmpty(auteur) ? (
                  <AuteurPreview
                    //link={"/Actualite/"}
                    image="../public/images/slide2.jpg"
                    title={auteur.auteur?.name + " " + auteur.auteur?.lastname}
                    Articles={auteur.nbrArticle}
                    Livres={auteur.nbrLivre}
                    Followers="99"
                  />
                ) : loadingAuteur ? (
                  <div className="text-center">
                    <Preloader />
                  </div>
                ) : (
                  <>
                    <div className="text-danger text-center font-weight-bold">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  </>
                )}

                <div className={styles["titre-ligue1-div"]}>
                  <Row>
                    <Col md="8">
                      {"LES ARTICLES DE " +
                        auteur.auteur?.name +
                        " " +
                        auteur.auteur?.lastname}
                    </Col>
                    <Col md="4" className="text-right">
                      Tout voir{" "}
                    </Col>
                  </Row>
                </div>
                {articles.length ? (
                  articles.map((article) => {
                    return (
                      <>
                        <ArticlePreviewMedium
                          link={
                            "/articles/" +
                            article.category[0].label +
                            "/" +
                            article.title
                          }
                          image={API_LINK + article.image}
                          title={article.title}
                          category={
                            article.category.length
                              ? article.category[0].label
                              : null
                          }
                          date={calculdatePublié(article.date)}
                          ID={article._id}
                        />
                      </>
                    );
                  })
                ) : loadingArticles ? (
                  <div className="text-center">
                    <Preloader />
                  </div>
                ) : (
                  <>
                    <div className="text-danger text-center font-weight-bold">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  </>
                )}

                <div className={styles["titre-ligue1-div"]}>
                  <Row>
                    <Col md="8">
                      {"LES LIVRES PUBLIÉS PAR " +
                        auteur.auteur?.name +
                        " " +
                        auteur.auteur?.lastname}{" "}
                    </Col>
                    <Col md="4" className="text-right">
                      Tout voir{" "}
                    </Col>
                  </Row>
                </div>
                <Row>
                  {livres.length ? (
                    livres.map((livre) => {
                      return (
                        <>
                          <LivrePreview
                            imageURL="../public/images/slide2.jpg"
                            NomAuteur={
                              livre.author?.name + " " + livre.author?.lastname
                            }
                            NomLivre={livre.name}
                            Cat={livre.categorie?.entitled}
                          />
                        </>
                      );
                    })
                  ) : loadingLivres ? (
                    <div className="text-center">
                      <Preloader />
                    </div>
                  ) : (
                    <>
                      <div className="text-danger text-center font-weight-bold">
                        {" "}
                        {"Information indisponible"}{" "}
                      </div>
                    </>
                  )}
                </Row>
              </Col>

              <Col md="4">
                <SideBarAct />
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  };
}
export default withRouter(Auteur);
