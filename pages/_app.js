import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import authReducer from "../store/reducers/auth";
import thunk from "redux-thunk";
import { autoSignin } from "../store/actions/Auth";
import { useDispatch, useSelector } from "react-redux";
import Cookies from "js-cookie";

function MyApp({ Component, pageProps }) {
  const token = Cookies.get("_token");
  const id = Cookies.get("_id");
  const type = Cookies.get("_type");
  console.log("type", type);

  const rootReducer = combineReducers({
    auth: authReducer,
  });
  const store = createStore(rootReducer, applyMiddleware(thunk));
  console.log("_app");
  return (
    <Provider store={store}>
      <Component {...pageProps} isAuth={!!token} userId={id} type={"Auteur"} />
    </Provider>
  );
}

export default MyApp;
