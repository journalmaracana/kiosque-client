import React, { Component, Fragment } from 'react';
import { Row, Col, Container, Input, Button, Modal, ModalBody, Form, FormGroup, ButtonGroup, Label } from 'reactstrap';
import DashboardLayout from "../../../components/dashbord/Layout";
import Swal from 'sweetalert2'
import axios from 'axios'
import { API_LINK } from '../../../utils/constant'
import { connect } from 'react-redux';
import Cookies from "js-cookie";
import { withRouter } from 'next/router';
import { logout } from '../../../store/actions/index';
import * as actions from "../../../store/actions";

class Compte extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }

    }

    componentDidMount() {
        Swal.fire({
            title: 'Voulez vous vraiment supprimrer votre compte?',
            //text: 'You will not be able to recover this imaginary file!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Oui',
            cancelButtonText: 'Non'
        }).then((result) => {
            if (result.isConfirmed) {
                this.deleteAccount()
                this.props.logout(this.props.router)
        
                Swal.fire(
                    'Supprimé!',
                    'Votre compte a été supprimé.',
                    'success'
                )
            }
        })

    }
    deleteAccount = () => {
        axios.put(API_LINK + 'v1/deleteAccount', { id: this.props.userId }).then(res => {
            console.log('user deleted')
        }).catch(err => {
            console.log('err', err)
        })
    }


    render() {
        return (
            <div>
                <DashboardLayout />

            </div>
        )
    }
}
function mapDispatchToProps(dispatch) {
    return {
        // dispatching plain actions
        logout: (router) => dispatch(actions.logout(router)),
        
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Compte))
