import React, { Component } from "react";
import { Container } from "reactstrap";
import Layout from "../../components/dashbord/Layout";
import ProtectedRoute from "../../components/ui/routes/ProtectedRoute";
import { useRouter } from 'next/router'

const DashboardHome = (props) => {
  const router = useRouter()
    

  return (
    <ProtectedRoute {...props} >
      <Layout {...router}>
        <Container>
          <div className="text-center my-5">Dashbord Home page </div>
        </Container>
      </Layout>
    </ProtectedRoute>
  );
};

export default DashboardHome;
