import React, { Component } from "react";
import {
  Container,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Input,
  Button,
  Row,
  Col,
  FormGroup,
} from "reactstrap";
import Axios from "../../../utils/axios_config";

import Link from "next/link";
import { alert } from "../../../components/ui/alertSwal/alertSwal";
import { VALIDATOR_MESSAGES } from "../../../utils/constants";
import SimpleReactValidator from "simple-react-validator";
import SetPwInput from "../../../components/setpassword";
import Preloader from "../../../components/ui/Prealoder";
class SetPassword extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  state = {
    currentpassword: "",
    password: "",
    confirmPassword: "",
    showCurrentpassword: false,
    showPassword: false,
    showConfirmPassword: false,
    laoding: false,
  };

  handelChange = (e) => {
    if (e) {
      this.setState({
        [e.target.name]: e.target.value,
      });
    }
  };

  setPassword = () => {
    console.log("setPassword");
    if (this.validator.allValid()) {
      this.setState({ laoding: true });
      const { currentpassword, password, confirmPassword } = this.state;
      const newPassword = { currentpassword, password, confirmPassword };
      console.log("password", newPassword);
      Axios.put("/UpdatePassWord/" + this.props.userId, newPassword)
        .then((res) => {
          console.log("res", res);
          alert(
            "Confirmation",
            "Votre mot de passe à été modifié avec succès",
            "success"
          );
          this.setState({ laoding: false });
        })

        .catch((err) => {
          if (err.response.status === 404) {
            alert("Error", "oops..! " + err.response.data.message, "error");
          } else {
            alert("Error", "Oops..! " + err, "error");
          }
          this.setState({ laoding: false });
        });
    } else {
      console.log("else validator");
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  render() {
    if (this.state.laoding) {
      return <Preloader />;
    } else {
      return (
        <div>
          <Container className="py-5">
            <h2>Mot de passe</h2>
            <p>
              Votre Mot de passe doit contenir au moins 12 caractères, une
              minuscule, une majuscule, un chiffre et un caractère
              spécial($#%*+@!?&).
            </p>
            <Row>
              <Col md="6">
                <Form>
                  <SetPwInput
                    label={" Mot de passe actuel"}
                    inputName={"currentpassword"}
                    inputValue={this.state.currentpassword}
                    show={this.state.showCurrentpassword}
                    handelChange={this.handelChange}
                    clicked={() =>
                      this.setState({
                        showCurrentpassword: !this.state.showCurrentpassword,
                      })
                    }
                    validator={this.validator.message(
                      "currentpassword",
                      this.state.currentpassword,
                      "required"
                    )}
                  >
                    <Link href="/password_recovery">
                      <a className="text-primary" style={{ fontSize: "13px" }}>
                        {" "}
                        Mot de pass oublié
                      </a>
                    </Link>
                  </SetPwInput>
                  <SetPwInput
                    label={"Nouveau mot de passe"}
                    inputName={"password"}
                    inputValue={this.state.password}
                    show={this.state.showPassword}
                    handelChange={this.handelChange}
                    clicked={() =>
                      this.setState({ showPassword: !this.state.showPassword })
                    }
                    validator={this.validator.message(
                      "password",
                      this.state.password,
                      [
                        "required",
                        {
                          regex: `^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$#%*+@!?&])[A-Za-z\\d$#%*+@!?&]{12,}$`,
                        },
                      ]
                    )}
                  ></SetPwInput>
                  <SetPwInput
                    label={" Confirmer votre nouveau mot de passe"}
                    inputName={"confirmPassword"}
                    inputValue={this.state.confirmPassword}
                    show={this.state.showConfirmPassword}
                    handelChange={this.handelChange}
                    clicked={() =>
                      this.setState({
                        showConfirmPassword: !this.state.showConfirmPassword,
                      })
                    }
                    validator={this.validator.message(
                      "confirmpassword",
                      this.state.confirmPassword,
                      "required|in:" + this.state.password + ""
                    )}
                  ></SetPwInput>
                  <InputGroup>
                    <Button color="primary" onClick={this.setPassword}>
                      Confirmer le changement
                    </Button>
                  </InputGroup>
                </Form>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default SetPassword;
