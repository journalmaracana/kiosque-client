import React, { Component } from "react";
import { Row, Col, Container } from "reactstrap";
import SettingsLanguage from "../../../components/setting/Langue";
import SettingsDarkMode from "../../../components/setting/DarkMode";
import SettingsNotifications from "../../../components/setting/Notifications";
import SettingsAccount from "../../../components/setting/Account";
import SettingsCV from "../../../components/setting/CV";
import ProtectedRoute from "../../../components/ui/routes/ProtectedRoute";
import {
  AiOutlineArrowRight,
  AiOutlineCheck,
  AiOutlineArrowLeft,
} from "react-icons/ai";
import "../../../styles/DashSettings.module.css";
import Layout from "../../../components/dashbord/Layout";
import {
  SettingElm,
  ConfidentialElm,
} from "../../../components/setting/Settings";
import styles from "../../../styles/DashHome.module.css";

class DashboardSettings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeView: "Paramètres",
    };
  }

  settingItems = [
    "paramètres et confidentialité",
    "Mode nuit",
    "Contactez Nous",
  ];
  confidentialItems = [
    { text: "Paramètres", link: "#/todo" },
    { text: "Mot de passe", link: "/set_password" },
    { text: "langue", link: "#/todo" },
  ];
  render() {
    const s = this.state;
    console.log(s.activeView === "Contactez Nous");
    return (
      <ProtectedRoute {...this.props}>
        <Layout>
          <Container>
            <div
              className={`${styles["mydashboard-right"]} float-right bg-white`}
            >
              <Row className="mx-0">
                <Col md="3">
                  {s.activeView !== "Paramètres" ? (
                    <AiOutlineArrowLeft
                      style={{ cursor: "pointer" }}
                      onClick={() =>
                        this.setState({ activeView: "Paramètres" })
                      }
                      className="text-blue mt-3"
                      size={30}
                    />
                  ) : null}
                </Col>
                <Col md="6" className="text-center text-my-5">
                  <h2 className="text-center text-blue mt-3">{s.activeView}</h2>
                </Col>
                <Col></Col>

                {/* <Col
                md="6"
                className="offset-md-3 text-my-5 text-center"
                style={{ color: "#007c92", marginTop: "100px" }}
              >
                {s.activeView}
              </Col> */}

                <Col md="6" className="offset-md-3 text-my-5">
                  {s.activeView === "paramètres et confidentialité" ? (
                    this.confidentialItems.map((item, index) => (
                      <ConfidentialElm
                        key={index + "conf"}
                        text={item.text}
                        link={item.link}
                      />
                    ))
                  ) : s.activeView === "Mode nuit" ? (
                    <ConfidentialElm
                      key={"darkMode"}
                      text={"mode Nuit"}
                      link={"#"}
                    />
                  ) : s.activeView === "Contactez Nous" ? (
                    <ConfidentialElm
                      key={"contactUs"}
                      text={"Contactez Nous"}
                      link={"#"}
                    />
                  ) : (
                    <div>
                      {this.settingItems.map((item, index) => (
                        <SettingElm
                          key={index}
                          text={item}
                          clicked={(active) =>
                            this.setState({ activeView: active })
                          }
                        />
                      ))}
                      <SettingElm
                        key={"logout"}
                        text={"Se déconnecter"}
                        clicked={() => {
                          console.log("dd");
                        }}
                      />
                    </div>
                  )}
                </Col>
              </Row>
            </div>
          </Container>
        </Layout>
      </ProtectedRoute>
    );
  }
}
export default DashboardSettings;
