import React from "react";
import LecteurProfile from "../../../components/profile/lecteur";
import AuteurProfile from "../../../components/profile/auteur";
import EditeurProfile from "../../../components/profile/editeur";
import ProtectedRoute from "../../../components/ui/routes/ProtectedRoute";
import Layout from "../../../components/dashbord/Layout";

function Profile(props) {
  const { type } = props;
  switch (type) {
    case "Lecteur":
      return (
        <ProtectedRoute {...props}>
          <LecteurProfile />
        </ProtectedRoute>
      );
    case "Auteur":
      return (
        <ProtectedRoute {...props}>
          <AuteurProfile />
        </ProtectedRoute>
      );
    case "Editeur":
      return (
        <ProtectedRoute {...props}>
          <EditeurProfile />
        </ProtectedRoute>
      );

    default:
      return null;
  }
}

export default Profile;
