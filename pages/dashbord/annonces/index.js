import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Container,
  Input,
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  Label,
} from "reactstrap";
import Preloader from "../../../components/ui/Prealoder";
import PhTable from "../../../components/ui/table";
import { AiFillEye } from "react-icons/ai";
import { FaPencilAlt, FaTrash } from "react-icons/fa";

import "../../../styles/DashAds.module.css";
import Layout from "../../../components/dashbord/Layout";
import ProtectedRoute from "../../components/ui/routes/ProtectedRoute";
const OptionButtons = (props) => {
  return (
    <Fragment>
      <Button color="warning" outline className="btn-sm mx-1">
        {" "}
        <AiFillEye />{" "}
      </Button>
      <Button color="success" outline className="btn-sm mx-1">
        {" "}
        <FaPencilAlt />{" "}
      </Button>
      <Button color="danger" outline className="btn-sm mx-1">
        {" "}
        <FaTrash />{" "}
      </Button>
    </Fragment>
  );
};

class DashboardAds extends Component {
  constructor(props) {
    super(props);

    // document.title = "Market";

    this.state = {
      langLoaded: false,
      currentTitle: "",

      headData: ["MALADE", "LIEU", "TYPE", "GROUPAGE", "TELEPHONE", "ACTIONS"],
      bodyData: [
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
      ],

      //  add donation modal states
      addBloodDonationModal: false,
      activeNeed: "",
    };
  }

  componentDidMount() {
    // window.scrollTo(0, 0);
    // // load language
    // i18n.loadNamespaces("translations", () => {
    //   this.setState({
    //     langLoaded: true,
    //     currentTitle: "dashboard.ads.filter1",
    //   });
    // });
  }

  render() {
    // state short
    const s = this.state;

    // check language if loaded for remove preloader
    if (s.langLoaded) {
      return <Preloader />;
    }

    const externalCloseBtn = (
      <button
        className="close"
        style={{
          position: "fixed",
          top: "15px",
          right: "15px",
          color: "white",
        }}
        onClick={() =>
          this.setState({ addBloodDonationModal: !s.addBloodDonationModal })
        }
      >
        &times;
      </button>
    );

    return (
      <ProtectedRoute {...props}>
        <Layout>
          <div className="mydashboard-right float-right bg-white">
            <Row className="mx-0">
              <Col lg="2" md="3" className="mydashboard-filter">
                <ul className="mydashboard-filter-fixed text-center">
                  <li
                    className={`mb-3 mt-5 ${
                      s.currentTitle === "dashboard.ads.filter1"
                        ? "text-blue"
                        : ""
                    }`}
                    onClick={() =>
                      this.setState({ currentTitle: "dashboard.ads.filter1" })
                    }
                  >
                    {" "}
                    {"dashboard.ads.filter1"}{" "}
                  </li>
                  <li
                    className={`mb-3 ${
                      s.currentTitle === "dashboard.ads.filter2"
                        ? "text-blue"
                        : ""
                    }`}
                    onClick={() =>
                      this.setState({ currentTitle: "dashboard.ads.filter2" })
                    }
                  >
                    {" "}
                    {"dashboard.ads.filter2"}{" "}
                  </li>
                  <li
                    className={`mb-3 ${
                      s.currentTitle === "dashboard.ads.filter3"
                        ? "text-blue"
                        : ""
                    }`}
                    onClick={() =>
                      this.setState({ currentTitle: "dashboard.ads.filter3" })
                    }
                  >
                    {" "}
                    {"dashboard.ads.filter3"}{" "}
                  </li>
                  <li
                    className={`mb-3 ${
                      s.currentTitle === "dashboard.ads.filter4"
                        ? "text-blue"
                        : ""
                    }`}
                    onClick={() =>
                      this.setState({ currentTitle: "dashboard.ads.filter4" })
                    }
                  >
                    {" "}
                    {"dashboard.ads.filter4"}{" "}
                  </li>
                </ul>
              </Col>
              <Col lg="10" md="9" style={{ backgroundColor: "#f9f9f9" }}>
                <h2 className="text-center text-blue mt-3">
                  {" "}
                  {s.currentTitle}{" "}
                </h2>
                <br />
                <Container>
                  <Row>
                    <Col xs="8">
                      <Input
                        type="search"
                        name=""
                        className="shadow-sm border-0 form-control-sm w-50"
                        placeholder={"header.search"}
                      />
                    </Col>
                    <Col xs="4" className="text-right">
                      <Button
                        color="blue"
                        className="rounded-12 btn-sm px-5"
                        onClick={() =>
                          this.setState({
                            addBloodDonationModal: !s.addBloodDonationModal,
                          })
                        }
                      >
                        {" "}
                        {"buttons.add"}{" "}
                      </Button>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="12" lg="12">
                      <PhTable headData={s.headData} bodyData={s.bodyData} />
                    </Col>
                  </Row>
                </Container>
              </Col>
            </Row>

            {/* modals */}
            <Modal
              isOpen={s.addBloodDonationModal}
              size="lg"
              external={externalCloseBtn}
            >
              <ModalBody>
                <h2 className="text-blue text-center mt-5">
                  {" "}
                  {"dashboard.ads.filter1"}{" "}
                </h2>
                <br />
                <br />
                <Form className="register-patient-form">
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"dashboard.ads.blood_donation.form.label1"} :{" "}
                    </Label>
                    <Row className="register-gender-select">
                      <Col xs="4">
                        <Button
                          onClick={() =>
                            this.setState({
                              activeNeed:
                                "dashboard.ads.blood_donation.form.needed1",
                            })
                          }
                          type="button"
                          block
                          outline
                          className={`${
                            s.activeNeed ===
                            "dashboard.ads.blood_donation.form.needed1"
                              ? "active"
                              : ""
                          }`}
                        >
                          {" "}
                          {"dashboard.ads.blood_donation.form.needed1"}{" "}
                        </Button>
                      </Col>
                      <Col xs="4">
                        <Button
                          onClick={() =>
                            this.setState({
                              activeNeed:
                                "dashboard.ads.blood_donation.form.needed2",
                            })
                          }
                          type="button"
                          block
                          outline
                          className={`${
                            s.activeNeed ===
                            "dashboard.ads.blood_donation.form.needed2"
                              ? "active"
                              : ""
                          }`}
                        >
                          {" "}
                          {"dashboard.ads.blood_donation.form.needed2"}{" "}
                        </Button>
                      </Col>
                      <Col xs="4">
                        <Button
                          onClick={() =>
                            this.setState({
                              activeNeed:
                                "dashboard.ads.blood_donation.form.needed3",
                            })
                          }
                          type="button"
                          block
                          outline
                          className={`${
                            s.activeNeed ===
                            "dashboard.ads.blood_donation.form.needed3"
                              ? "active"
                              : ""
                          }`}
                        >
                          {" "}
                          {"dashboard.ads.blood_donation.form.needed3"}{" "}
                        </Button>
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"dashboard.ads.blood_donation.form.patient_name"} :{" "}
                    </Label>
                    <Input type="text" name="" />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {
                        "dashboard.ads.blood_donation.form.groupage_select"
                      } :{" "}
                    </Label>
                    <div className="text-center text-muted">
                      <Input type="checkbox" name="" />{" "}
                      {"dashboard.ads.blood_donation.form.select_all_groupage"}
                    </div>
                    <Row className="register-gender-select mt-1">
                      <Col className="pr-1">
                        <Button type="button" block outline>
                          A+
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          A-
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          B+
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          B-
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          AB+
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          AB-
                        </Button>
                      </Col>
                      <Col className="pr-1 pl-1">
                        <Button type="button" block outline>
                          O+
                        </Button>
                      </Col>
                      <Col className="pl-1">
                        <Button type="button" block outline>
                          O-
                        </Button>
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"dashboard.ads.blood_donation.form.place"} :{" "}
                    </Label>
                    <Input type="text" name="" />
                  </FormGroup>
                  <FormGroup>
                    <Row>
                      <Col xs="4">
                        <Label className="text-blue font-weight-bold">
                          {" "}
                          {"auth.common"} :{" "}
                        </Label>
                        <Input type="select">
                          <option value="">{"auth.common"}</option>
                        </Input>
                      </Col>
                      <Col xs="8">
                        <Label className="text-blue font-weight-bold">
                          {" "}
                          {"auth.wilaya"} :{" "}
                        </Label>
                        <Input type="select">
                          <option value="">{"auth.wilaya"}</option>
                        </Input>
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"dashboard.ads.blood_donation.form.donation_date"} :{" "}
                    </Label>
                    <div className="ml-4">
                      <Input type="radio" name="donation_status" /> Urgent
                    </div>
                    <div className="ml-4">
                      <Input type="radio" name="donation_status" /> à une date
                      précise
                    </div>
                    <Row>
                      <Col xs="4">
                        <Input
                          type="date"
                          name=""
                          size="sm"
                          className="border-blue rounded-12"
                        />
                      </Col>
                      <Col xs="3">
                        <Input
                          type="select"
                          size="sm"
                          className="border-blue rounded-12"
                        >
                          <option value="10:00">10:00</option>
                        </Input>
                      </Col>
                    </Row>
                  </FormGroup>
                  <br />
                  <h2 className="text-blue text-center mt-5">
                    {" "}
                    {"dashboard.ads.blood_donation.form.contact"}{" "}
                  </h2>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"auth.lastname"} :{" "}
                    </Label>
                    <Input type="text" name="" />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"auth.firstname"} :{" "}
                    </Label>
                    <Input type="text" name="" />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"auth.mobile"} :{" "}
                    </Label>
                    <Row>
                      <Col xs="3">
                        <Input type="select">
                          <option value="+213">+213</option>
                        </Input>
                      </Col>
                      <Col xs="9">
                        <Input type="text" name="mobile" />
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {" "}
                      {"dashboard.ads.blood_donation.form.parent_place"} :{" "}
                    </Label>
                    <Input type="select">
                      <option>Select...</option>
                    </Input>
                  </FormGroup>
                  <h2 className="text-blue text-center mt-5">
                    {" "}
                    {
                      "dashboard.ads.blood_donation.form.additional_information"
                    }{" "}
                  </h2>
                  <FormGroup>
                    <Input type="textarea" name="" rows="8" />
                  </FormGroup>
                  <FormGroup className="text-center">
                    <Button
                      color="blue"
                      className="font-weight-bold px-5 rounded-12"
                    >
                      {" "}
                      {"buttons.validate"}{" "}
                    </Button>
                  </FormGroup>
                </Form>
              </ModalBody>
            </Modal>
          </div>
        </Layout>
      </ProtectedRoute>
    );
  }
}

export default DashboardAds;
