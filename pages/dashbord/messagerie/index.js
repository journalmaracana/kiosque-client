import React, { Component, Fragment } from "react";
import { Row, Col, Button, Input } from "reactstrap";
import { FiPlus } from "react-icons/fi";
import {
  BsTrash,
  BsFillStarFill,
  BsFillEnvelopeFill,
  BsFileEarmarkText,
} from "react-icons/bs";
import { MdMoveToInbox, MdSend } from "react-icons/md";
import { RiSpam3Line } from "react-icons/ri";

import "../../../styles/DashInbox.module.css";
import {
  MessageView,
  NotificationView,
  NewMessageView,
  ReadMessageView,
} from "../../../components/inbox/Inbox";
import Layout from "../../../components/dashbord/Layout";
import ProtectedRoute from "../../components/ui/routes/ProtectedRoute";
class DashboardInbox extends Component {
  constructor(props) {
    super(props);

    //document.title = "Inbox";

    this.toMessages = this.toMessages.bind(this);

    this.state = {
      selectedView: "messages",
      newMessageBoxDisplay: true,

      // selectedMessageId: new URLSearchParams(this.props.location.search).get(
      //   "openid"
      // ),
      selectedMessageContent: null,
    };
  }

  componentDidMount() {
    //   window.scrollTo(0, 0);
    //   this.unlisten = this.props.history.listen((location, action) => {
    //     this.setState(
    //       {
    //         selectedMessageId: new URLSearchParams(location.search).get("openid"),
    //       },
    //       () => {
    //         // function here
    //       }
    //     );
    //   });
  }
  toMessages() {
    this.setState({
      selectedMessageId: null,
    });
  }

  render() {
    // short state
    const s = this.state;
    // lang
    const { lang } = this.props;

    return (
      <ProtectedRoute {...props}>
        <Layout>
          <div className="mydashboard-right float-right bg-white">
            <Row className="mx-0">
              <Col lg="2" md="3" className="mydashboard-filter mt-5">
                {s.selectedView === "messages" ? (
                  <ul className="mydashboard-filter-fixed inbox mt-5">
                    <li className="mb-3 pl-2">
                      {" "}
                      <Button
                        onClick={() =>
                          this.setState({
                            newMessageBoxDisplay: !s.newMessageBoxDisplay,
                          })
                        }
                        size="sm"
                        className="text-blue border-2 border-blue rounded-pill bg-white font-weight-bold mt-2"
                      >
                        {" "}
                        <FiPlus /> {"dashboard.inbox.new_message"}{" "}
                      </Button>{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold text-blue">
                      {" "}
                      <MdMoveToInbox /> {"dashboard.inbox.received"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <BsFillStarFill /> {"dashboard.inbox.bookmarks"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <BsFillEnvelopeFill /> {"dashboard.inbox.unread"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <MdSend /> {"dashboard.inbox.sended"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <BsFileEarmarkText /> {"dashboard.inbox.draft"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <RiSpam3Line /> {"dashboard.inbox.spam"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <BsTrash /> {"dashboard.inbox.trash"}{" "}
                    </li>
                  </ul>
                ) : s.selectedView === "notifications" ? (
                  <ul className="mydashboard-filter-fixed inbox mt-5">
                    <li className="mb-3 pt-1 pl-2 font-weight-bold text-blue">
                      {" "}
                      <MdMoveToInbox /> {"dashboard.inbox.received"}{" "}
                    </li>
                    <li className="mb-3 pl-2 font-weight-bold">
                      {" "}
                      <BsFillEnvelopeFill /> {"dashboard.inbox.unread"}{" "}
                    </li>
                  </ul>
                ) : (
                  ""
                )}
              </Col>
              <Col lg="10" md="9" className="px-0">
                <h2 className="text-center text-blue mt-3">
                  {" "}
                  {"dashboard.inbox.title"}{" "}
                </h2>
                <div className="dashboard-inbox-header mt-4">
                  <Row className="mx-0">
                    <Col xs="6" className="px-0">
                      <h4
                        className={`text-blue text-center mb-0 pb-2 ${
                          s.selectedView === "messages" ? "active" : ""
                        }`}
                        onClick={() =>
                          this.setState({ selectedView: "messages" })
                        }
                      >
                        {" "}
                        {"dashboard.inbox.messages"}{" "}
                      </h4>
                    </Col>
                    <Col xs="6" className="px-0">
                      <h4
                        className={`text-blue text-center mb-0 pb-2 ${
                          s.selectedView === "notifications" ? "active" : ""
                        }`}
                        onClick={() =>
                          this.setState({ selectedView: "notifications" })
                        }
                      >
                        {" "}
                        {"dashboard.inbox.notifications"}{" "}
                      </h4>
                    </Col>
                  </Row>
                </div>

                <div
                  className="dashboard-inbox-messages"
                  style={{
                    display: s.selectedView === "messages" ? "block" : "none",
                  }}
                >
                  {s.selectedMessageId ? (
                    <ReadMessageView returnFunction={this.toMessages} />
                  ) : (
                    <Fragment>
                      <div>
                        <Input type="checkbox" className="ml-2 mt-2" />
                      </div>
                      <br />
                      {[...Array(5)].map((data, key) => {
                        return <MessageView key={key} lang={lang} />;
                      })}
                    </Fragment>
                  )}
                </div>

                <div
                  className="dashboard-inbox-notifications"
                  style={{
                    display:
                      s.selectedView === "notifications" ? "block" : "none",
                  }}
                >
                  <div>
                    <Input type="checkbox" className="ml-3 mt-2" />
                  </div>
                  <br />
                  {[...Array(5)].map((data, key) => {
                    return <NotificationView key={key} lang={lang} />;
                  })}
                </div>
              </Col>
            </Row>

            <NewMessageView lang={lang} hidden={s.newMessageBoxDisplay} />
          </div>
        </Layout>
      </ProtectedRoute>
    );
  }
}

export default DashboardInbox;
