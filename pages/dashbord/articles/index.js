import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Container,
  Input,
  Button,
  Modal,
  ModalBody,
  Form,
  FormGroup,
  ButtonGroup,
  Label,
} from "reactstrap";
import Select from "react-select";
import Preloader from "../../../components/ui/Prealoder";
import PhTable from "../../../components/dashbord/Table";
import { AiFillEye } from "react-icons/ai";
import { FaPencilAlt, FaTrash } from "react-icons/fa";
import DashboardLayout from "../../../components/dashbord/Layout";
import { API_LINK } from "../../../utils/constant";
import Pagination from "../../../components//ui/Pagination";
import SimpleReactValidator from 'simple-react-validator';
import axios from "axios";
import { TagsInput } from "../../../components/TagsInput/TagsInput";
import moment from "moment";
import { Markup } from "interweave";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css";
import ProtectedRoute from "../../../components/ui/routes/ProtectedRoute";

const OptionButtons = (props) => {
  return (
    <Fragment>
      <Button
        color="warning"
        outline
        className="btn-sm mx-1"
        onClick={() => props.onView()}
      >
        {" "}
        <AiFillEye />{" "}
      </Button>
      <Button
        color="success"
        outline
        className="btn-sm mx-1"
        onClick={() => props.onEdit()}
      >
        {" "}
        <FaPencilAlt />{" "}
      </Button>
      <Button
        color="danger"
        outline
        className="btn-sm mx-1"
        onClick={() => props.onDelete()}
      >
        {" "}
        <FaTrash />{" "}
      </Button>
    </Fragment>
  );
};

class DashboardAds extends Component {
  constructor(props) {
    super(props);
		console.log('here in article',this.props.userId)
		this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    this.action = {
      user: this.props.userId,
      date: moment(),
    };

    this.state = {
      langLoaded: false,
      currentTitle: "Liste des articles",
      articles: [],
      title: "",
      articleID: "",
      state: "",
      publishedHour: "",
      categories: [],
      category: [],
      author: "",
      author: "",
      authorName: {},
      content: "",
      langue: "",
      keyWord: [],
      lng: "FR",
      articles: [],
      ID: this.props.userId,
      currentPage: 1,
      pagesCount: 0,
      pageSize: 15,
      checked: true,
      isLoading: true,
      headData: [
        "Titre de l’article",
        "Auteur",
        "Etat",
        "Date de publication",
        "Date de mise à jour ",
        "Catégorie",
        "ACTIONS",
      ],
      bodyData: [
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
        [
          "Mohamed Bouzroura",
          "Hopital beni messous - ALGER",
          "Plaquette",
          "AB+",
          "0553 10 12 08",
          <OptionButtons />,
        ],
      ],

      activeNeed: "",
      addProductModal: false,
      showProductModal: false,
      deleteProductModal: false,
      editProductModal: false,
    };

    this.viewProduct = this.viewProduct.bind(this);
  }

  selectPicture(inputID) {
    document.getElementById(inputID).click();
  }

  viewProduct(item) {
    this.setState({
      showProductModal: !this.state.showProductModal,
      articleID: item._id,
      title: item.title,
      content: item.content,
      category: item.category[0].label,
      date: item.date ? item.date.split("T")[0] : null,
      publishedHour: item.publishedHour
        ? item.publishedHour.split("T")[0]
        : null,
      author: item.author,
      keyWord: item.keyWord,
      image: item.image,
      checked: item.isActive ? item.isActive : null,
      state: item.state,
    });
  }
  resetState() {
    this.setState({
      title: "",
      content: "",
      category: [],
      author: "",
      keyWord: [],
      image: null,
      state: "",
      checked: true,
      date: "",
      publishedHour: "",
    });
  }
  deleteProduct() {
    this.setState({
      deleteProductModal: !this.state.deleteProductModal,
    });
  }

  editProduct(item) {
    console.log("edit", item);
    this.resetState();
    this.getAuthor();
    this.setState({
      editProductModal: !this.state.editProductModal,
      articleID: item._id,
      title: item.title,
      content: item.content,
      date: item.date ? item.date.split("T")[0] : null,
      publishedHour: item.publishedHour
        ? item.publishedHour.split("T")[0]
        : null,
      category: item.category,
      author: item.author._id,
      keyWord: item.keyWord,
      image: item.image,
      checked: item.isActive ? item.isActive : null,
      state: item.state,
    });
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getArticleByUser(
      this.state.lng,
      this.state.currentPage,
      this.state.pageSize,
      this.state.ID
    );
    this.getCategory();
  }
  getAuthor = () => {
    axios
      .get(API_LINK + "v1/user/" + this.state.ID)
      .then((response) => {
        console.log("here", response.data);
        this.setState({
          authorName: response.data,
        });
      })
      .catch((err) => console.log(err));
  };
  getCategory = () => {
    let URLGetCategory = API_LINK + "v1/categories";
    axios
      .get(URLGetCategory)
      .then((response) => {
        console.log("here", response.data);
        this.setState({
          categories: response.data,
        });
      })
      .catch((err) => console.log(err));
  };
  getArticleByUser = (lang, index, limit, id) => {
    let linkGet = "v1/articlespaginationClient/language/";
    axios
      .get(API_LINK + linkGet + lang + "/" + index + "/" + limit + "/" + id)
      .then((response) => {
        console.log("articlesDash", response.data);
        this.setState({
          articles: response.data.articles.map((item) => {
            let sorted = item.actions.sort((a, b) => {
              return b.date.localeCompare(a.date);
            });
            let update = sorted.map((action) => {
              if (action.action === "Update") {
                return action.user ? action.date : null;
              }
            });
            return [
              item.title,
              item.author.name + " " + item.author.lastname,
              item.state,
              item.date ? moment(item.date).format("DD-MM-YYYY") : null,
              update
                ? moment(update[0]).format("DD-MM-YYYY, HH:mm:ss")
                : "Aucune MAJ",
              item.category[0].label,
              <OptionButtons
                onEdit={() => this.editProduct(item)}
                onView={() => this.viewProduct(item)}
                onDelete={() => this.deleteProduct(item._id)}
              />,
            ];
          }),
          currentPage: parseInt(response.data.currentPage),
          pagesCount: response.data.totalPages,
          isLoading: false,
        });
      })
      .catch((err) => {
        this.setState({
          isLoading: false
        })
        console.log(err);
      });
  };
  handleClick = (index) => {
    // Handle Pagination
    if (index > 0) {
      this.setState(
        {
          currentPage: index,
        },
        () => {
          this.getArticleByUser(
            this.state.lng,
            this.state.currentPage,
            this.state.pageSize,
            this.state.ID
          );
        }
      );
    }
    window.scrollTo(0, 0);
  };
  addArticle = () => {
    let form_data = new FormData();
    if (this.state.image) {
      form_data.append("image", this.state.image, this.state.image.name);
    }
    form_data.append("author", this.state.ID);
    form_data.append("date", moment());
    form_data.append("publishedHour", this.state.publishedHour);
    form_data.append("category", JSON.stringify(this.state.category));
    form_data.append("keyWord", JSON.stringify(this.state.keyWord));
    form_data.append("state", this.state.state);
    form_data.append("title", this.state.title);
    form_data.append("content", this.state.content);
    form_data.append("lng", this.state.lng);
    form_data.append("isActive", this.state.checked);
    form_data.append("action", JSON.stringify(this.action));
    axios
      .post(API_LINK + "v1/articles", form_data, {
        headers: {
          "content-type": "multipart/form-data",
        },
      })
      .then((res) => {
        console.log('"Article');
        this.resetState();
        this.getArticleByUser(
          this.state.lng,
          this.state.currentPage,
          this.state.pageSize,
          this.state.ID
        );
        this.setState({
          addProductModal: false,
        });
      })
      .catch((err) => {
        console.log('"Article', err);
      });
  };
  editArticle = () => {
    let URLModifie = "v1/articles/";
    let form_data = new FormData();
    if (this.state.image) {
      form_data.append("image", this.state.image, this.state.image.name);
    }
    form_data.append("author", this.state.author);
    form_data.append("publishedHour", this.state.publishedHour);
    form_data.append("date", this.state.date);
    form_data.append("category", JSON.stringify(this.state.category));
    form_data.append("keyWord", JSON.stringify(this.state.keyWord));
    form_data.append("state", this.state.state);
    form_data.append("title", this.state.title);
    form_data.append("content", this.state.content);
    form_data.append("lng", this.state.lng);
    form_data.append("isActive", this.state.checked);
    form_data.append("action", JSON.stringify(this.action));

    let ID = this.state.articleID;
    axios
      .put(API_LINK + URLModifie + ID, form_data, {
        headers: {
          "content-type": "multipart/form-data",
        },
      })
      .then((res) => {
        console.log('"Article');
        this.resetState();
        this.getArticleByUser(
          this.state.lng,
          this.state.currentPage,
          this.state.pageSize,
          this.state.ID
        );
        this.setState({
          editProductModal: false,
        });
      })
      .catch((err) => {
        console.log('"Article', err);
      });
  };
  deleteArticle = async (id) => {
    await axios
      .delete(API_LINK + "v1/articles/" + id)
      .then((res) => {
        this.setState({
          deleteProductModal: false,
        });
        this.resetState();
        this.getArticleByUser(
          this.state.lng,
          this.state.currentPage,
          this.state.pageSize,
          this.state.ID
        );

        console.log("article à été supprimer avec succes");
      })
      .catch((err) => {
        console.log(
          ` Une erreur s'est produite lors de la suppression  de l'article `,
          err
        );
      });
  };
  SingleSelectCategory = () => {
    let options = this.state.categories.map((categorie) => {
      return { value: categorie._id, label: categorie.entitled };
    });
    return (
      <FormGroup>
        <Select
          isMulti
          className="basic-single"
          classNamePrefix="select"
          isClearable
          isSearchable
          name="categories"
          id="categories"
          options={options}
          value={this.state.category}
          getOptionLabel={({ label }) => label}
          getOptionValue={({ value }) => value}
          onChange={this.handleChangeSelectCategories}
          onBlur={() => this.validator.showMessageFor('category')} />
          <span className="text-danger">
            {this.validator.message('category', this.state.category, 'required', {
              messages: {
                required: 'Champ obligatoire'
              }
            })}</span>
      </FormGroup>
    );
  };
  handleChangeSelectCategories = (event) => {
    if (event) {
      this.setState({
        category: event,
      });
    } else {
      this.setState({
        category: [],
      });
    }
  };
  renderCommentaire = () => {
    return (
      <>
        <FormGroup>
          {" "}
          <ButtonGroup>
            <Button
              color="danger"
              className="btn-sm"
              onClick={() => this.setState({ checked: false })}
              active={this.state.checked === false}
            >
              {"Non"}
            </Button>
            <Button
              color="success"
              className="btn-sm"
              onClick={() => this.setState({ checked: true })}
              active={this.state.checked === true}
            >
              {"Oui"}
            </Button>
          </ButtonGroup>
        </FormGroup>
      </>
    );
  };
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value,
    });
  };
  handleImageChange = (e) => {
    // Pour type file 'Image'
    this.setState({
      image: e.target.files[0],
    });
  };
  removeTag = (i) => {
    const newTags = [...this.state.keyWord];
    newTags.splice(i, 1);
    this.setState({ keyWord: newTags });
  };

  inputKeyDown = (e) => {
    const val = e.target.value;
    if (e.key === "Enter" && val) {
      if (
        this.state.keyWord.find(
          (tag) => tag.toLowerCase() === val.toLowerCase()
        )
      ) {
        return;
      }
      this.setState({ keyWord: [...this.state.keyWord, val] }, () => {});
      e.target.value = null;
    } else if (e.key === "Backspace" && !val) {
      this.removeTag(this.state.keyWord.length - 1);
    }
  };
  checkValidationHandler = () => {

		return this.validator.allValid()
	}
  renderEtat = () => {
    return (
      <>
        <FormGroup>
          <Input
            type="select"
            name="state"
            id="state"
            onChange={this.handleChange}
            value={this.state.state}
          >
            <option value="u">--</option>
            <option value="Enregistrer comme brouillon">
              {"Enregistrer comme brouillon"}
            </option>
            <option value="Publié">
              {"Publié"}
            </option>
            <option value="Programmé">{"Programmé"}</option>
            <option value="Supprimé">{"Supprimé"}</option>
          </Input>
        </FormGroup>
      </>
    );
  };
  handleChangeSun = (content) => {
    console.log("onChangeSun1", content); //Get Content Inside Editor
    this.setState({
      content: content,
    });
  };
  handleDropSun(event) {
    //Get the drop event
    this.setState({
      content: event,
    });
  }
  render() {
    const s = this.state;
    

    const externalCloseBtn = (
      <button
        className="close"
        style={{
          position: "fixed",
          top: "15px",
          right: "15px",
          color: "white",
        }}
        onClick={() =>
          this.setState({
            addProductModal: false,
            showProductModal: false,
            editProductModal: false,
          })
        }
      >
        &times;
      </button>
    );
// 
    
    return (
      <ProtectedRoute {...this.props}>
        <div className="mydashboard-right bg-white">
          <DashboardLayout />
          {(s.isLoading) ?
    
  <Preloader />
     :
     <>
          <Row className="table">
            <Col>
              <h2 className="text-center text-blue mt-3"> {s.currentTitle} </h2>
              <br />
              <Container>
                <Row>
                  <Col xs="8">
                    <Input
                      type="search"
                      name=""
                      className="shadow-sm border-0 form-control-sm w-50"
                      placeholder={"Rechercher"}
                      style={{ margin: "auto" }}
                    />
                  </Col>
                  <Col xs="4" className="text-right">
                    <Button
                      style={{ margin: "auto" }}
                      color="blue"
                      className="rounded-12 btn-sm px-5"
                      onClick={() =>
                        this.setState(
                          { addProductModal: !s.addProductModal },
                          () => {
                            this.resetState(), this.getAuthor();
                          }
                        )
                      }
                    >
                      {" "}
                      {"Ajouter"}{" "}
                    </Button>
                  </Col>
                </Row>
                {/*close btn*/}

                {s.articles.length > 0 ? (
                  <PhTable headData={s.headData} bodyData={s.articles} />
                ) : (
                  <div className="text-danger text-center font-weight-bold">
                    {" "}
                    <br></br>
                    {"Liste des articles est vide!"}{" "}
                  </div>
                )}
              </Container>
            </Col>
          </Row>

          <Row>
            <Col md="8">
              {s.articles.length > 0 ? (
                <div className="d-flex justify-content-center">
                  <Pagination
                    currentPage={s.currentPage}
                    pagesCount={s.pagesCount}
                    handleClick={this.handleClick}
                  />
                </div>
              ) : null}
            </Col>
          </Row>

          {/* start modals */}
          <Modal
            isOpen={s.addProductModal}
            size="lg"
            external={externalCloseBtn}
          >
            <ModalBody>
              <br />
              <br />
              <Form className="register-patient-form">
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    Catégorie:{" "}
                  </Label>
                  {this.SingleSelectCategory()}
                </FormGroup>
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {"Etat"} :{" "}
                  </Label>
                  {this.renderEtat()}
                </FormGroup>
                {this.state.state === "Programmé" ? (
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {"Date de publication"} :{" "}
                    </Label>
                    <Input
                      type="date"
                      id="publishedHour"
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                ) : null}
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Titre de l'article"} :{" "}
                  </Label>
                  <Input type="text" id="title" onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('title')} />
				<span className="text-danger">
					{this.validator.message('title', this.state.title, 'required', {
						messages: {
							required: 'Champ obligatoire'
						}
					})}</span>
                </FormGroup>
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Contenu"} :{" "}
                  </Label>
                  <SunEditor
                    setOptions={{
                      buttonList: [
                        ["undo", "redo"],
                        ["font", "fontSize", "formatBlock"],
                        ["paragraphStyle", "blockquote"],
                        [
                          "bold",
                          "underline",
                          "italic",
                          "strike",
                          "subscript",
                          "superscript",
                        ],
                        ["fontColor", "hiliteColor", "textStyle"],
                        ["removeFormat"],
                        "/", // Line break
                        ["outdent", "indent"],
                        ["align", "horizontalRule", "list", "lineHeight"],
                        [
                          "table",
                          "link",
                          "image",
                          "video",
                          "audio" /** ,'math' */,
                        ], // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
                        /** ['imageGallery'] */ [
                          "fullScreen",
                          "showBlocks",
                          "codeView",
                        ],
                        ["preview", "print"],
                        ["save", "template"],
                      ],
                    }}
                    onChange={this.handleChangeSun}
                    onDrop={this.handleDropSun}
                    setContents={s.content}
                    height="300px"
                    onBlur={() => this.validator.showMessageFor('content')} />
                    <span className="text-danger">
                      {this.validator.message('content', this.state.content, 'required', {
                        messages: {
                          required: 'Champ obligatoire'
                        }
                      })}</span>
                </FormGroup>
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Image"} :{" "}
                  </Label>
                  <br />
                  <Button
                    color="blue"
                    className="rounded-12 px-4"
                    size="sm"
                    onClick={() => this.selectPicture("image")}
                  >
                    {" "}
                    {"Sélectionner une image"}{" "}
                  </Button>
                  <Input
                    type="file"
                    onChange={this.handleImageChange}
                    className="d-none"
                    id="image"
                    name="image"
                    accept="image/*"
                    onBlur={() => this.validator.showMessageFor('image')} />
                    <span className="text-danger">
                      {this.validator.message('image', this.state.image, 'required', {
                        messages: {
                          required: 'Champ obligatoire'
                        }
                      })}</span>
                </FormGroup>
                <br />
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Auteur"} :{" "}
                  </Label>
                  <Input
                    type="text"
                    readOnly
                    value={s.authorName.name + " " + s.authorName.lastname}
                  />
                </FormGroup>
                <br />
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Mots clés"} :{" "}
                  </Label>
                  <TagsInput
                    tags={s.keyWord}
                    removeTag={this.removeTag}
                    inputKeyDown={this.inputKeyDown.bind(this)}
                    name="tags"
                    id="tags"
                  />
                </FormGroup>

                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"L'état des commentaires"} :{" "}
                  </Label>
                  {this.renderCommentaire()}
                </FormGroup>
                <br />
                <FormGroup className="text-center">
                  <Button
                    color="blue"
                    className="font-weight-bold px-5 rounded-12"
                    onClick={() => this.addArticle()} disabled={!(this.checkValidationHandler())}
                  >
                    {" "}
                    {"Ajouter"}{" "}
                  </Button>
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>

          <Modal isOpen={s.showProductModal} external={externalCloseBtn}>
            {console.log("here")}
            <ModalBody>
              <Row>
                <Col md="12">
                  <img
                    alt=""
                    src={API_LINK + s.image}
                    width="100%"
                    height="auto"
                    className="rounded-12 border-blue border-3"
                  />
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <div className="market-product-info font-size-14 text-blue font-weight-bold">
                    {" "}
                    {"Titre de l'article"} :{" "}
                  </div>
                  <div className="market-product-info font-size-14 text-muted">
                    {" "}
                    {s.title}{" "}
                  </div>

                  <div className="market-product-info font-size-14 text-blue font-weight-bold">
                    {" "}
                    {"Etat"} :{" "}
                  </div>
                  <div className="market-product-info font-size-14 text-muted">
                    {s.state}
                  </div>

                  <div className="market-product-info font-size-14 text-blue font-weight-bold">
                    {" "}
                    {"Catégorie de l'article"} :{" "}
                  </div>
                  <div className="market-product-info font-size-14 text-muted">
                    {s.category}
                  </div>

                  <div className="market-product-info font-size-14 text-blue font-weight-bold">
                    {" "}
                    {"Auteur"} :{" "}
                  </div>
                  <div className="market-product-info font-size-14 text-muted">
                    {s.author.name + " " + s.author.lastname}
                  </div>

                  <div className="market-product-info font-size-14 text-blue font-weight-bold">
                    {" "}
                    {"Mots-Clés"} :{" "}
                  </div>
                  <div className="market-product-info font-size-14 text-muted">
                    {s.keyWord.map((item) => {
                      return <li>{item}</li>;
                    })}
                  </div>
                </Col>
              </Row>
              <div className="market-product-info font-size-14 text-blue font-weight-bold">
                {" "}
                {"Contenu"} :{" "}
              </div>
              <div className="market-product-info font-size-14 text-muted">
                <Markup
                  content={s.content ? s.content : "Information indisponible"}
                />
              </div>
              <br />
              <br />
            </ModalBody>
          </Modal>

          <Modal
            isOpen={s.editProductModal}
            external={externalCloseBtn}
            size="lg"
          >
            <ModalBody>
              <br />
              <br />
              <Form className="register-patient-form">
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    Catégorie:{" "}
                  </Label>
                  {this.SingleSelectCategory()}
                </FormGroup>
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {"Etat"} :{" "}
                  </Label>
                  {this.renderEtat()}
                </FormGroup>
                {this.state.state === "Programmé" ? (
                  <FormGroup>
                    <Label className="text-blue font-weight-bold">
                      {"Date de publication"} :{" "}
                    </Label>
                    <Input
                      type="date"
                      id="publishedHour"
                      onChange={this.handleChange}
                      value={s.publishedHour}
                    />
                  </FormGroup>
                ) : null}
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Titre de l'article"} :{" "}
                  </Label>
                  <Input
                    type="text"
                    id="title"
                    value={s.title}
                    onChange={this.handleChange}
                    onBlur={() => this.validator.showMessageFor('title')} />
                    <span className="text-danger">
                      {this.validator.message('title', this.state.title, 'required', {
                        messages: {
                          required: 'Champ obligatoire'
                        }
                      })}</span>
                </FormGroup>

                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Contenu"} :{" "}
                  </Label>
                  <SunEditor
                    setOptions={{
                      buttonList: [
                        ["undo", "redo"],
                        ["font", "fontSize", "formatBlock"],
                        ["paragraphStyle", "blockquote"],
                        [
                          "bold",
                          "underline",
                          "italic",
                          "strike",
                          "subscript",
                          "superscript",
                        ],
                        ["fontColor", "hiliteColor", "textStyle"],
                        ["removeFormat"],
                        "/", // Line break
                        ["outdent", "indent"],
                        ["align", "horizontalRule", "list", "lineHeight"],
                        [
                          "table",
                          "link",
                          "image",
                          "video",
                          "audio" /** ,'math' */,
                        ], // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
                        /** ['imageGallery'] */ [
                          "fullScreen",
                          "showBlocks",
                          "codeView",
                        ],
                        ["preview", "print"],
                        ["save", "template"],
                      ],
                    }}
                    onChange={this.handleChangeSun}
                    onDrop={this.handleDropSun}
                    setContents={s.content}
                    height="300px"
                    onBlur={() => this.validator.showMessageFor('content')} />
                    <span className="text-danger">
                      {this.validator.message('content', this.state.content, 'required', {
                        messages: {
                          required: 'Champ obligatoire'
                        }
                      })}</span>
                </FormGroup>
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Image"} :{" "}
                  </Label>
                  <div className="text-center">
                    <img
                      alt="image"
                      src={s.image ? API_LINK + s.image : null}
                      width="200px"
                      height="auto"
                      className="rounded-12 border-blue border-3"
                    />
                  </div>
                  <div
                    style={{ width: "200px", margin: "auto" }}
                    className="text-center text-blue font-size-12"
                  >
                    <span
                      style={{ cursor: "pointer" }}
                      onClick={() =>
                        this.setState({
                          deleteFile: true,
                          image: null,
                        })
                      }
                    >
                      {" "}
                      Supprimer{" "}
                    </span>
                    &nbsp;&nbsp;&nbsp;
                    <span
                      style={{ cursor: "pointer" }}
                      onClick={() => this.selectPicture("image")}
                    >
                      {" "}
                      {"Modifier"}{" "}
                    </span>
                    <Input
                      type="file"
                      onChange={this.handleImageChange}
                      className="d-none"
                      id="image"
                      name="image"
                      accept="image/*"
                    />
                  </div>
                </FormGroup>

                <br />
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Auteur"} :{" "}
                  </Label>
                  <Input
                    type="text"
                    readOnly
                    value={
                      s.authorName
                        ? s.authorName.name + " " + s.authorName.lastname
                        : null
                    }
                  />
                </FormGroup>
                <br />
                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"Mots clés"} :{" "}
                  </Label>
                  <TagsInput
                    tags={s.keyWord}
                    removeTag={this.removeTag}
                    inputKeyDown={this.inputKeyDown.bind(this)}
                    name="tags"
                    id="tags"
                  />
                </FormGroup>

                <FormGroup>
                  <Label className="text-blue font-weight-bold">
                    {" "}
                    {"L'état des commentaires"} :{" "}
                  </Label>
                  {this.renderCommentaire()}
                </FormGroup>
                <br />
                <FormGroup className="text-center">
                  <Button
                    color="blue"
                    className="font-weight-bold px-5 rounded-12"
                    onClick={() => this.editArticle()}
                  >
                    {" "}
                    {"Modifier"}{" "}
                  </Button>
                </FormGroup>
              </Form>
            </ModalBody>
          </Modal>

          <Modal isOpen={s.deleteProductModal} className="mt-5 pt-5">
            <ModalBody className="px-5 py-4">
              <div className="text-center font-size-18">
                {"Voulez vous vraiment supprimer cette ligne!"}
              </div>
              <br />
              <Row>
                <Col xs="6">
                  <Button
                    color="light"
                    block
                    className="rounded-12 shadow-sm"
                    onClick={() => this.setState({ deleteProductModal: false })}
                  >
                    {" "}
                    {"Annuler"}{" "}
                  </Button>
                </Col>
                <Col xs="6">
                  <Button
                    color="danger"
                    block
                    className="rounded-12 shadow-sm"
                    onClick={() => this.deleteArticle(s.articleID)}
                  >
                    {" "}
                    {"Supprimer"}{" "}
                  </Button>
                </Col>
              </Row>
            </ModalBody>
          </Modal>
</>}
          {/*  end modals  */}
        </div>
      </ProtectedRoute>
    );
 
}
}

export default DashboardAds;
