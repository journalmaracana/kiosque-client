import React, { Component, Fragment } from 'react';
import { Row, Col, Container, Input, Button, Modal, ModalBody, Form, FormGroup, ButtonGroup, Label } from 'reactstrap';
import Select from 'react-select'
import Preloader from '../../../components/ui/Prealoder';
import { API_LINK } from "../../../utils/constant";
import axios from "axios";
import { PAYS_OPTIONS_CODE } from '../../../utils/constant'
import DashboardLayout from "../../../components/dashbord/Layout";
import styles from '../../../styles/Pays.module.css'
import SimpleReactValidator from 'simple-react-validator';


class Country extends Component {
    constructor(props) {
        super(props);
        this.validator = new SimpleReactValidator({ autoForceUpdate: this });
        this.state = {
            pays: '',
            user: this.props.userId,
            countryOrigin: '',
            country:''
        }

    }

    componentDidMount() {
        //this.getLocation()
        this.getPaysUser()
    }
    getLocation() {
        fetch('https://extreme-ip-lookup.com/json/')
            .then(res => res.json())
            .then(response => {
                this.setState({
                    countryOrigin: {value:response.country,label:response.country,key:response.countryCode}
                }, () => {
                    if(this.state.countryOrigin.key){
                       this.setState({
                           country: PAYS_OPTIONS_CODE.map(x => {
                               if(x.key === this.state.countryOrigin.key)
                               return x
                            })
                        })
                    }
                })

            })
            .catch((data, status) => {
                console.log('Request failed');
            })
    }
    handleChangePays = (e) => {
        console.log('hereeeeeee', e)
        if (e) {
            this.setState({
                country: e
            })
        }
        else {
            this.setState({
                country: ''
            })
        }
    }
    addPaysUser = () => {
        let data = {
            pays: this.state.country,
            user: this.props.userId
        }
        axios.put(API_LINK + 'v1/paysUser/' + this.props.userId, data).then(res => {
            console.log('success')
            this.getPaysUser()
        })
            .catch(err => {
                console.log('err', err.response)
            })
    }
    getPaysUser = () => {
        axios.get(API_LINK + 'v1/paysUser/' + this.props.userId).then(res => {
            if(res.data === 'Country no detected'){
                this.getLocation()
            }
            else{
            this.setState({
                country: res.data.pays
            })
        }
        })
            .catch(err => {
                console.log('err', err.response)

            })
    }
   
    render() {
        return (
            <div>
                <DashboardLayout />

                <div className={styles.container}>
                    <h2 style={{ marginLeft: '15%', marginTop: '10%' }}>Séléctionner un pays:</h2>
                    <div style={{ marginLeft: '15%' }}>
                        {this.state.countryOrigin !== null ?
                            <>

                                <Select
                                    className="basic-single"
                                    classNamePrefix="select"
                                    isClearable
                                    isSearchable
                                    name="pays"
                                    id="pays"
                                    options={PAYS_OPTIONS_CODE}
                                    //defaultValue= {this.state.country}
                                    value={this.state.country}
                                    getOptionLabel={({ label }) => label}
                                    getOptionValue={({ value }) => value}
                                    onChange={this.handleChangePays}
                                />

                            </> : null}
                    </div>
                </div>

                <Button style={{ background: 'teal', marginTop: '5%', marginLeft: '15%' }} onClick={() => { this.addPaysUser() }} disabled={!this.validator.allValid()}>Confirmer le changement </Button>

            </div>
        )
    }
}

export default Country