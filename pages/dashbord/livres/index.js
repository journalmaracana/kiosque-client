import React, { Component, Fragment } from 'react';
import { Row, Col, Container, Input, Button, Modal, ModalBody, Form, FormGroup, ButtonGroup, Label } from 'reactstrap';
import Select from 'react-select'
import Preloader from '../../../components/ui/Prealoder';
import PhTable from '../../../components/dashbord/Table';
import { AiFillEye } from 'react-icons/ai';
import { FaPencilAlt, FaTrash } from 'react-icons/fa';
import DashboardLayout from '../../../components/dashbord/Layout';
import { API_LINK } from "../../../utils/constant";
import Pagination from "../../../components/ui/Pagination";
import SimpleReactValidator from 'simple-react-validator';
import axios from "axios";
import moment from 'moment'
import { Markup } from 'interweave';
const OptionButtons = (props) => {
	return (
		<Fragment>
			<Button color="warning" outline className="btn-sm mx-1" onClick={() => props.onView()}> <AiFillEye /> </Button>
			<Button color="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
			<Button color="danger" outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>
		</Fragment>
	);
}

class DashboardBook extends Component {
	constructor(props) {
		super(props);
		this.validator = new SimpleReactValidator({ autoForceUpdate: this }); //Pour la form validation
		this.action = {
			user: this.props.userId,
			date: moment()
		}
		this.state = {
			langLoaded: false,
			currentTitle: 'Liste des articles',
			livres: [],
			books: [],
			users: [],
			tableEditor: [{ label: 'Autre', value: 'Autre' }],
			tableLivre: [{ label: 'Autre', value: 'Autre' }],
			allCategories: [],
			allSousCategories: [],
			livre: '',
			title: '',
			name: '',
			bookID: '',
			author: '',
			editor: '',
			editornew: '',
			tome: '',
			idCategorie: '',
			resume: '',
			parution: '',
			pagenumber: '',
			category: '',
			souscategorie: '',
			langue: '',
			licence: '',
			laUne: null,
			loading: true,
			version: null,
			nbrpage: '',
			affichage: 'Auto',
			lecture: '',
			partage: false,
			telechargement: false,
			imprimer: false,
			commentaire: false,
			lng: "FR",
			ID: this.props.userId,
			currentPage: 1,
			pagesCount: 0,
			pageSize: 15,
			checked: true,
			headData: ['Nom', 'Auteur', 'Langue', 'Tome', 'Catégorie', 'Date de parution', 'ACTIONS'],
			bodyData: [
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />],
				['Mohamed Bouzroura', 'Hopital beni messous - ALGER', 'Plaquette', 'AB+', '0553 10 12 08', <OptionButtons />]
			],

			activeNeed: '',
			addProductModal: false,
			showProductModal: false,
			deleteProductModal: false,
			editProductModal: false
		}

		this.viewProduct = this.viewProduct.bind(this);
	}



	selectPicture(inputID) {
		document.getElementById(inputID).click();
	}
	selectEdition(inputID) {
		document.getElementById(inputID).click();
	}
	viewProduct(item) {
		console.log('itee', item)
		this.setState({
			showProductModal: !this.state.showProductModal,
			name: item.name,
			bookID: item._id,
			author: item.author,
			editor: item.editeur,
			editor: item.editeurnew,
			tome: item.tome,
			title: item.title,
			resume: item.resume,
			parution: item.parution,
			category: item.categorie,
			souscategorie: item.souscategorie,
			langue: item.langue,
			licence: item.licence,
			laUne: item.laUne,
			version: item.version,
			nbrpage: item.nbrpage,
			lng: item.lng,
			affichage: item.affichage,
			lecture: item.lecture,
			imprimer: item.imprimer,
			telechargement: item.telechargement,
			partage: item.partage,
			commentaire: item.commentaire,
		})
	}
	resetState() {
		this.setState({
			name: '',
			bookID: '',
			author: '',
			editor: '',
			tome: '',
			title: '',
			resume: '',
			parution: '',
			pagenumber: '',
			category: '',
			souscategorie: '',
			langue: '',
			licence: '',
			laUne: null,
			version: null,
			nbrpage: '',
			lng: "FR",
			affichage: 'Auto',
			lecture: '',
			imprimer: false,
			telechargement: false,
			partage: false,
			commentaire: false,
		})
	}
	deleteProduct(item) {
		this.setState({
			deleteProductModal: !this.state.deleteProductModal,
			bookID: item
		})
	}

	editProduct(item) {
		console.log('edrrr,', item)
		this.resetState()
		this.setState({
			editProductModal: !this.state.editProductModal,
			livre: item.livre ? item.livre._id : 'Autre',
			name: item.name,
			bookID: item._id,
			author: item.author._id,
			editor: item.editeur ? item.editeur._id : 'Autre',
			editornew: item.editeurnew,
			tome: item.tome,
			title: item.title,
			resume: item.resume,
			parution: item.parution ? item.parution.split('T')[0] : null,
			category: item.categorie,
			souscategorie: item.souscategorie,
			langue: item.langue,
			licence: item.licence,
			laUne: item.laUne,
			version: item.version,
			nbrpage: item.nbrpage,
			lng: item.lng,
			affichage: item.affichage,
			lecture: item.lecture,
			imprimer: item.imprimer,
			telechargement: item.telechargement,
			partage: item.partage,
			commentaire: item.commentaire,
		})
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		this.getBooks(this.state.lng, this.state.currentPage, this.state.pageSize, this.state.ID)
		this.getCategory()
		this.getEditors()
	}


	getEditors = () => {
		let URLGet = 'v1/user/type/'
		axios.get(API_LINK + URLGet + 'Editeur')
			.then(response => {

				this.setState({
					tableEditor: [...this.state.tableEditor, ...response.data.map((user) => {
						return (
							{ value: user._id, label: user.editor }
						)
					})]
				}, () => { console.log('userrr', this.state.tableEditor) })
			})
			.catch(err => { console.log(err) })

	}
	getBooks = (lang, index, limit, user) => {
		let linkGet = "v1/livrepaginationuser/"
		axios.get(API_LINK + linkGet + lang + "/" + index + "/" + limit + "/" + user)
			.then(response => {
				this.setState({
					loading: false,
					livres: response.data,
					currentPage: parseInt(response.data.currentPage),
					pagesCount: response.data.totalPages,
				}, () => {
					this.renderBook(),

					this.setState({
						tableLivre: [...this.state.tableLivre, ...this.state.livres.livre.map((livre) => {
							return (
								{ value: livre._id, label: livre.name }
							)
						})]
					})
				})

			})
			.catch((err) => {
				console.log('err', err);
				this.setState({
					loading: false,
				})
			});

	}
	renderBook = () => {
		this.setState({
			books: this.state.livres ? this.state.livres.livre.length > 0 ? this.state.livres.livre.map(item => {

				return [item.name,
				item.author.name + " " + item.author.lastname,
				item.langue,
				item.tome,
				item.categorie ? item.categorie.entitled : null,
				item.parution ? moment(item.parution).format('DD-MM-YYYY') : null,
				<OptionButtons onEdit={() => this.editProduct(item)} onView={() => this.viewProduct(item)} onDelete={() => this.deleteProduct(item._id)} />]
			}) : null : 'Liste vide',
		})
	}
	handleClick = (index) => {
		// Handle Pagination
		if (index > 0) {
			this.setState({
				currentPage: index,
			}, () => {
				this.getBooks(this.state.lng, this.state.currentPage, this.state.pageSize, this.state.ID);
			});
		}
		window.scrollTo(0, 0);
	};
	addBook = () => {
		let URLAjout = 'v1/livre';
		let SC = []
		this.state.souscategorie.map(s => {
			SC.push(s._id)
		})
		let new_book = new FormData();
		if (this.state.livre !== 'Autre') {
			new_book.append('livre', this.state.livre)
		}
		new_book.append('name', this.state.name);
		new_book.append('author', this.state.ID)
		if (this.state.editor !== 'Autre') {
			new_book.append('editor', this.state.editor)
		}
		if (this.state.editor === 'Autre') {
			new_book.append('editornew', this.state.editornew)
		}
		new_book.append('category', JSON.stringify(this.state.category._id));
		new_book.append('souscategorie', JSON.stringify(SC));
		new_book.append('resume', this.state.resume);
		new_book.append('parution', this.state.parution.split('T')[0]);
		new_book.append('tome', this.state.tome);
		new_book.append('title', this.state.title);
		new_book.append('licence', this.state.licence);
		new_book.append('nbrpage', this.state.nbrpage);
		new_book.append('langue', this.state.langue);
		new_book.append('lng', this.state.lng);
		new_book.append('image', this.state.laUne);
		new_book.append('edition', this.state.version);
		new_book.append('lecture', this.state.lecture);
		new_book.append('affichage', this.state.affichage);
		new_book.append('imprimer', this.state.imprimer);
		new_book.append('telechargement', this.state.telechargement);
		new_book.append('partage', this.state.partage);
		new_book.append('commentaire', this.state.commentaire);
		// //---------
		new_book.append('action', JSON.stringify(this.action));
		axios.post(API_LINK + URLAjout, new_book, {
			headers: {
				'content-type': 'multipart/form-data'
			}
		}).then(res => {
			console.log('Livre')
			this.resetState()
			this.getBooks(this.state.lng, this.state.currentPage, this.state.pageSize, this.state.ID)
			this.setState({
				addProductModal: false,
			})
		})
			.catch(err => {
				console.log("err", err)
				console.log('Livre failed')
			})

	};
	setBook = () => {
		let URLModifie = 'v1/livre/';
		let SC = []
		this.state.souscategorie.map(s => {
			SC.push(s._id)
		})
		let new_book = new FormData();
		if (this.state.livre !== 'Autre') {
			new_book.append('livre', this.state.livre)
		}
		new_book.append('name', this.state.name);
		new_book.append('author', this.state.author)
		if (this.state.editor !== 'Autre') {
			new_book.append('editor', this.state.editor)
		}
		if (this.state.editor === 'Autre') {
			new_book.append('editornew', this.state.editornew)
		}
		new_book.append('category', JSON.stringify(this.state.category));
		new_book.append('souscategorie', JSON.stringify(SC));
		new_book.append('resume', this.state.resume);
		new_book.append('parution', this.state.parution.split('T')[0]);
		new_book.append('tome', this.state.tome);
		new_book.append('title', this.state.title);
		new_book.append('licence', this.state.licence);
		new_book.append('nbrpage', this.state.nbrpage);
		new_book.append('langue', this.state.langue);
		new_book.append('lng', this.state.lng);
		new_book.append('image', this.state.laUne);
		new_book.append('edition', this.state.version);
		new_book.append('lecture', this.state.lecture);
		new_book.append('affichage', this.state.affichage);
		new_book.append('imprimer', this.state.imprimer);
		new_book.append('telechargement', this.state.telechargement);
		new_book.append('partage', this.state.partage);
		new_book.append('commentaire', this.state.commentaire);
		// //---------
		new_book.append('action', JSON.stringify(this.action));
		axios.put(API_LINK + URLModifie + this.state.bookID, new_book, {
			headers: {
				'content-type': 'multipart/form-data'
			}
		})

			.then(res => {
				console.log('modification Livre')
				this.resetState()
				this.getBooks(this.state.lng, this.state.currentPage, this.state.pageSize, this.state.ID)
				this.setState({
					editProductModal: false,
				})
			})
			.catch(err => {
				console.log("err", err)
				console.log('Livre failed')
			})

	}
	deleteBook = async () => {
		await axios.delete(API_LINK + 'v1/deletelivre/' + this.state.bookID)
			.then(res => {
				this.setState({
					deleteProductModal: false
				})
				this.resetState()
				this.getBooks(this.state.lng, this.state.currentPage, this.state.pageSize, this.state.ID)

				console.log('Livre à été supprimer avec succes')

			})
			.catch(err => {
				console.log(` Une erreur s'est produite lors de la suppression  du livre `, err)
			})

	}
	getCategory = () => {
		let URLGetCategory = API_LINK + 'v1/categoriesBook'
		axios.get(URLGetCategory)
			.then(response => {
				this.setState({
					allCategories: response.data,
				})
			})
			.catch(err => console.log(err))
	}
	SingleSelectCategory = () => {
		let options = this.state.allCategories.length > 0 ? this.state.allCategories.map((categorie) => {
			return categorie;
		}) : null
		return (
			<FormGroup>
				<Select
					className="basic-single"
					classNamePrefix="select"
					isClearable
					isSearchable
					name="category"
					id="category"
					options={options}
					value={this.state.category}
					getOptionLabel={({ entitled }) => entitled}
					getOptionValue={({ _id }) => _id}
					onChange={this.handleChangeSelectCategories}
					onBlur={() => this.validator.showMessageFor('category')} />
				<span className="text-danger">
					{this.validator.message('category', this.state.category, 'required', {
						messages: {
							required: 'Champ obligatoire'
						}
					})}</span>
			</FormGroup>
		)
	}
	handleChangeSelectCategories = (event) => {
		if (event) {
			this.setState({
				category: event,
				idCategorie: event._id
			}, () => {

				let CategoryFocused = this.state.allCategories.length > 0 ? this.state.allCategories.find(categorie => categorie._id == this.state.category._id) : [];
				this.setState({
					allSousCategories:
						CategoryFocused ? CategoryFocused.sousCategorie.map(sousCategorie => {
							return { entitled: sousCategorie.entitled, _id: sousCategorie._id };
						}) : []
				}, () => console.log('ssss', this.state.souscategorie))
			})
		} else {
			this.setState({
				category: '',
			})
		}
	}
	handleChangeSelectSousCategories = (event) => {
		if (event) {
			this.setState({
				souscategorie: event
			})
		} else {
			this.setState({
				souscategorie: '',
			})
		}
	}
	renderLangue() {
		return (
			<FormGroup>
				<Input type="select" name="langue" id="langue" onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('langue')}
					value={this.state.langue}>
					<option value=''>--</option>
					<option value="Arabe"> Arabe</option>
					<option value="Français">Français</option>
					<option value="Espagnole">Espagnole</option>
					<option value="Anglais">Anglais</option>
					<option value="Italien">Italien</option>
					<option value="Allemand">Allemand</option>
				</Input>
				<span className="text-danger">
					{this.validator.message('langue', this.state.langue, 'required', {
						messages: {
							required: 'Champ obligatoire'
						}
					})}</span>
			</FormGroup>
		)
	}

	SingleSelectEditor = () => {

		return (
			<FormGroup>
				<Select
					className="basic-single"
					classNamePrefix="select"
					isClearable
					isSearchable
					name="editor"
					id="editor"
					options={this.state.tableEditor}
					value={this.state.tableEditor.find(x => x.value == this.state.editor)}
					onChange={this.handleChangeSelectEditor}
				/>

			</FormGroup>
		)
	}
	SingleSelectLivre = () => {

		return (
			<FormGroup>
				<Select
					className="basic-single"
					classNamePrefix="select"
					isClearable
					isSearchable
					name="livre"
					id="livre"
					options={this.state.tableLivre}
					value={this.state.tableLivre.find(x => x.value == this.state.livre)}
					onChange={this.handleChangeSelectLivre}
				/>

			</FormGroup>
		)
	}

	handleChangeSelectAuthor = (event) => {
		if (event) {
			this.setState({
				author: event.value
			})
		} else {
			this.setState({
				author: ''
			})
		}
	}
	handleChangeSelectEditor = (event) => {
		if (event) {
			this.setState({
				editor: event.value
			}, () => {
				console.log('here', this.state.editor)
			})
		} else {
			this.setState({
				editor: ''
			})
		}
	}
	handleChangeSelectLivre = (event) => {
		console.log('livredf', event)
		if (event) {
			this.setState({
				livre: event.value
			}, () => {
				console.log('here', this.state.livre)
				if (this.state.livre !== 'Autre') {
					this.setState({
						name: event.label
					})
				}
			})
		} else {
			this.setState({
				livre: ''
			})
		}
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}
	handleImageChange = (e) => {        // Pour type file 'Image'
		this.setState({
			image: e.target.files[0]
		})
	}
	handleChangeEtat = (e) => {
		const { name, value } = e.target;
		this.setState({
			[name]: value
		});
	};

	checkValidationHandler = () => {
		if (this.state.livre !== 'Autre') {
			this.validator.fields['name'] = true
		}

		return this.validator.allValid()
	}
	renderPartage = () => {
		console.log('this.state.partage', this.state.partage)
		return <>
			<FormGroup>
				<ButtonGroup>
					<Button
						color="danger"
						className={this.state.partage == false ? "active " : "btn-sm"}
						onClick={() => this.setState({ partage: false })}
						active={this.state.partage == false}

					>
						{'Non'}
					</Button>
					<Button
						color="success"
						className={this.state.partage == true ? "active " : "btn-sm"}
						onClick={() => this.setState({ partage: true })}
						active={this.state.partage == true}

					>
						{'Oui'}
					</Button></ButtonGroup>
			</FormGroup>
		</>
	}
	renderImpression = () => {
		return <>
			<FormGroup>
				<ButtonGroup>
					<Button
						color="danger"
						className={this.state.imprimer == false ? "active " : "btn-sm"}
						onClick={() => this.setState({ imprimer: false })}
						active={this.state.imprimer === false}
					>
						{'Non'}
					</Button>
					<Button
						color="success"
						className={this.state.imprimer == true ? "active " : "btn-sm"}
						onClick={() => this.setState({ imprimer: true })}
						active={this.state.imprimer === true}
					>
						{'Oui'}
					</Button>
				</ButtonGroup>
			</FormGroup>
		</>
	}
	renderLoad = () => {
		return <>
			<FormGroup>
				<ButtonGroup>
					<Button
						color="danger"
						className={this.state.telechargement == false ? "active " : "btn-sm"}
						onClick={() => this.setState({ telechargement: false })}
						active={this.state.telechargement === false}
					>
						{'Non'}
					</Button>
					<Button
						color="success"
						className={this.state.telechargement == true ? "active " : "btn-sm"}
						onClick={() => this.setState({ telechargement: true })}
						active={this.state.telechargement === true}
					>
						{'Oui'}
					</Button>
				</ButtonGroup>
			</FormGroup>
		</>
	}
	renderComment = () => {
		return <>
			<FormGroup>
				<ButtonGroup>
					<Button
						color="danger"
						className={this.state.commentaire == false ? "active " : "btn-sm"}
						onClick={() => this.setState({ commentaire: false })}
						active={this.state.commentaire === false}
					>
						{'Non'}
					</Button>
					<Button
						color="success"
						className={this.state.commentaire == true ? "active " : "btn-sm"}
						onClick={() => this.setState({ commentaire: true })}
						active={this.state.commentaire === true}
					>
						{'Oui'}
					</Button>
				</ButtonGroup>
			</FormGroup>
		</>
	}

	SingleSelectSousCategory = () => {
		return (
			<FormGroup>
				<Select
					className="basic-single"
					classNamePrefix="select"
					isClearable
					isSearchable
					isMulti
					name="souscategorie"
					id="souscategorie"
					options={this.state.allSousCategories}
					value={this.state.souscategorie}
					getOptionLabel={({ entitled }) => entitled}
					getOptionValue={({ _id }) => _id}
					onChange={this.handleChangeSelectSousCategories}
					onBlur={() => this.validator.showMessageFor('souscategorie')} />
				<span className="text-danger">
					{this.validator.message('souscategorie', this.state.souscategorie, 'required', {
						messages: {
							required: 'Champ obligatoire'
						}
					})}</span>
			</FormGroup>
		)
	}
	render() {
		const s = this.state;
		if (this.state.loading) {
			return <Preloader />;
		}


		const externalCloseBtn = <button className="close" style={{ position: 'fixed', top: '15px', right: '15px', color: 'white' }} onClick={() => this.setState({ addProductModal: false, showProductModal: false, editProductModal: false })}>&times;</button>;

		return (

			<div className="mydashboard-right bg-white">
				<DashboardLayout />

				<Row className='table'>

					<Col >
						<h2 className="text-center text-blue mt-3"> {s.currentTitle} </h2>
						<br />
						<Container>
							<Row>
								<Col xs="8">
									<Input type="search" name="" className="shadow-sm border-0 form-control-sm w-50" placeholder={"Rechercher"} style={{ margin: 'auto' }} />
								</Col>
								<Col xs="4" className="text-right">
									<Button style={{ margin: 'auto' }} color="blue" className="rounded-12 btn-sm px-5" onClick={() => this.setState({ addProductModal: !s.addProductModal }, () => { this.resetState() })} > {"Ajouter"} </Button>
								</Col>
							</Row>
							{/*close btn*/}


							{s.books ?

								<PhTable
									headData={s.headData}
									bodyData={s.books}
								/>
								:
								<div className="text-danger text-center font-weight-bold">
									{" "}
									<br></br>
									{"Liste des livres est vide!"}{" "}
								</div>
							}
						</Container>
					</Col>

				</Row>


				<Row>
					<Col md="8">
						{s.books ?
							<div className="d-flex justify-content-center">
								<Pagination
									currentPage={s.currentPage}
									pagesCount={s.pagesCount}
									handleClick={this.handleClick}
								/>
							</div>
							: null}
					</Col>
				</Row>

				{ /* start modals */}
				<Modal isOpen={s.addProductModal} size="lg" external={externalCloseBtn}>
					{console.log('unnn', this.state.partage)}
					<ModalBody>
						<br /><br />
						<Form className="register-patient-form">
							<FormGroup>
								<Label className="text-blue font-weight-bold">Catégorie: </Label>
								{this.SingleSelectCategory()}
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold">{'Sous Catégorie'} : </Label>
								{this.SingleSelectSousCategory()}
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Livre'} : </Label>
								{this.SingleSelectLivre()}
							</FormGroup>
							{this.state.livre === 'Autre' ?
								<FormGroup>
									<Label className="text-blue font-weight-bold"> {'Nom du livre'} : </Label>
									<Input type="text" id="name" onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('name')}
									/>
									<span style={{ 'color': 'red' }} >{this.validator.message('name', this.state.name, 'required',
										{
											messages: {
												required: 'Champ obligatoire'
											}
										}
									)}</span>
								</FormGroup>
								: null}
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Titre'} : </Label>
								<Input type="text" id="title" onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Tome'} : </Label>
								<Input type="number" min="1" id="tome" onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('tome')}
								/>
								<span style={{ 'color': 'red' }} >{this.validator.message('tome', this.state.tome, ['required', { regex: '^[1-9]' }],
									{
										messages: {
											required: 'Champ obligatoire',
											regex: 'nombre de page ne doit pas être inférieur ou égal à zéro'
										}
									}
								)}</span>

							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Nom de l\'éditeur'} : </Label>
								{this.SingleSelectEditor()}
							</FormGroup>

							{this.state.editor === 'Autre' ?

								<FormGroup>
									<Label className="text-blue font-weight-bold"> {'Nouveau editeur'} : </Label>
									<Input type="text" id="editornew" onChange={this.handleChange} />
								</FormGroup>
								: null}
							<br />
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Langue'} : </Label>
								{this.renderLangue()}
							</FormGroup>

							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Date de parution'} : </Label>
								<Input type="date" id='parution' onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Nombre de page'} : </Label>
								<Input type='number' id='nbrpage' onChange={this.handleChange}
									onBlur={() => this.validator.showMessageFor('nbrpage')}
								/>
								<span style={{ 'color': 'red' }} >{this.validator.message('nbrpage', this.state.nbrpage, ['required', { regex: '^[1-9]' }],
									{
										messages: {
											required: 'Champ obligatoire',
											regex: 'nombre de page ne doit pas être inférieur ou égal à zéro'
										}
									}
								)}</span>
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Licence'} : </Label>
								<Input type='text' id='licence' onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Résumé'} : </Label>
								<Input type='textarea' id='resume' onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'La couverture'} : </Label>
								<br />
								<Button color="blue" className="rounded-12 px-4" size="sm" onClick={() => this.selectPicture('laUne')}> {"Sélectionner une image"} </Button>
								<Input type="file" onChange={(e) => { this.setState({ laUne: e.target.files[0] }) }}className="d-none" id="laUne" name="image" accept="image/*"
									onBlur={() => this.validator.showMessageFor('laUne')} />
								<span className="text-danger">
									{this.validator.message('laUne', this.state.laUne, 'required', {
										messages: {
											required: 'Champ obligatoire'
										}
									})}</span>
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Version numérique'} : </Label>
								<br />
								<Button color="blue" className="rounded-12 px-4" size="sm" onClick={() => this.selectEdition('version')}> {"Sélectionner un fichier"} </Button>
								<Input type="file" onChange={(e) => { this.setState({ version: e.target.files[0] }) }} className="d-none" id="version" name="edition" accept="image/*"
									onBlur={() => this.validator.showMessageFor('version')} />
								<span className="text-danger">
									{this.validator.message('version', this.state.version, 'required', {
										messages: {
											required: 'Champ obligatoire'
										}
									})}</span>
							</FormGroup>

							<br />
							<FormGroup>
								<Label htmlFor="sexe">{'L\'affichage'}: </Label>
								<br />
								<Row>
									<Col md="1"></Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Auto"
											value="Auto"
											checked={this.state.affichage === "Auto"}
											onChange={this.handleChangeEtat} />{"Auto"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Livre"
											value="Livre"
											checked={this.state.affichage === 'Livre'}
											onChange={this.handleChangeEtat} /> {"Livre"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Défiler"
											value="Défiler"
											checked={this.state.affichage === 'Défiler'}
											onChange={this.handleChangeEtat} /> {"Défiler"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Diapositive"
											value="Diapositive"
											checked={this.state.affichage === 'Diapositive'}
											onChange={this.handleChangeEtat} /> {"Diapositive"}
									</Col>
								</Row>
							</FormGroup>
							<FormGroup>
								<Label htmlFor="sexe">{'La lecture'}: </Label>
								<br />
								<Row>
									<Col md="1"></Col>
									<Col md="4">
										<Input type="radio"
											name="lecture"
											id="De gauche à droite"
											value="De gauche à droite"
											checked={this.state.lecture === "De gauche à droite"}
											onChange={this.handleChangeEtat} />{"De gauche à droite"}
									</Col>
									<Col md="4">
										<Input type="radio"
											name="lecture"
											id="De droite à gauche"
											value="De droite à gauche"
											checked={this.state.lecture === 'De droite à gauche'}
											onChange={this.handleChangeEtat} /> {"De droite à gauche"}
									</Col>
								</Row>
							</FormGroup>
							<br />
							<Row>
								<Label>{'Autoriser l\’impression'}: </Label>
								{' '}
								<Col md="12">
									{this.renderImpression()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser le Téléchargement'}: </Label>
								{' '}
								<Col md="12">
									{this.renderLoad()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser le partage'}: </Label>
								{' '}
								<Col md="12">
									{this.renderPartage()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser les commentaires'}: </Label>
								{' '}
								<Col md="12">
									{this.renderComment()}
								</Col>
							</Row>
							<FormGroup className="text-center">
								<Button color="blue" className="font-weight-bold px-5 rounded-12" onClick={() => this.addBook()} disabled={!(this.checkValidationHandler())} > {'Ajouter'} </Button>
							</FormGroup>
						</Form>
					</ModalBody>
				</Modal>

				<Modal isOpen={s.showProductModal} external={externalCloseBtn}>
					<ModalBody>
						<Row>
							<Col md="12">

								<img alt="" src={s.laUne ? API_LINK + s.laUne : null} width="100%" height="auto" className="rounded-12 border-blue border-3" />
							</Col></Row>
						<Row>
							<Col md="12">
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Nom du livre'} : </div>
								<div className="market-product-info font-size-14 text-muted"> {s.name} </div>
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Titre du livre'} : </div>
								<div className="market-product-info font-size-14 text-muted"> {s.title} </div>

								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Tome'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.tome}</div>

								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Auteur'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.author.name + " " + s.author.lastname}</div>
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Catégorie du livre'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.category ? s.category.entitled : '/'}</div>
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Les sous Catégories'} : </div>
								{s.souscategorie.length > 0 ? s.souscategorie.map(item => {
									return (
										<li>{item.entitled}</li>)
								}) : null}
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Langue de publication'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.langue}</div>

								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Date de parution'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.parution ? moment(s.parution).format('DD-MM-YYYY') : null}</div>

								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Nombre de page'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.nbrpage}</div>
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Licence'} : </div>
								<div className="market-product-info font-size-14 text-muted">{s.licence}</div>
								<div className="market-product-info font-size-14 text-blue font-weight-bold"> {'Résumé'} : </div>
								<Markup content={s.resume ? s.resume : 'Information indisponible'} />
							</Col>
						</Row>

						<br />
						<br />
					</ModalBody>

				</Modal>

				<Modal isOpen={s.editProductModal} external={externalCloseBtn} size="lg">
					<ModalBody>
						<br /><br />
						<Form className="register-patient-form">
							<FormGroup>
								<Label className="text-blue font-weight-bold">Catégorie: </Label>
								{this.SingleSelectCategory()}
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold">{'Sous Catégorie'} : </Label>
								{this.SingleSelectSousCategory()}
							</FormGroup>

							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Livre'} : </Label>
								{this.SingleSelectLivre()}
							</FormGroup>
							{this.state.livre === 'Autre' ?
								<FormGroup>
									<Label className="text-blue font-weight-bold"> {'Nom du livre'} : </Label>
									<Input type="text" id="name" value={this.state.name} onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('name')}
									/>
									<span style={{ 'color': 'red' }} >{this.validator.message('name', this.state.name, 'required',
										{
											messages: {
												required: 'Champ obligatoire'
											}
										}
									)}</span>
								</FormGroup>
								: null}
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Titre'} : </Label>
								<Input type="text" id="title" value={this.state.title} onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Tome'} : </Label>
								<Input type="number" id="tome" onChange={this.handleChange} value={this.state.tome}
									onBlur={() => this.validator.showMessageFor('tome')}
								/>
								<span style={{ 'color': 'red' }} >{this.validator.message('tome', this.state.tome, ['required', { regex: '^[1-9]' }],
									{
										messages: {
											required: 'Champ obligatoire',
											regex: 'nombre de page ne doit pas être inférieur ou égal à zéro'
										}
									}
								)}</span>
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Nom de l\'éditeur'} : </Label>
								{this.SingleSelectEditor()}
							</FormGroup>
							<br />
							{this.state.editor === 'Autre' ?
								<FormGroup>
									<Label className="text-blue font-weight-bold"> {'Nouveau editeur'} : </Label>
									<Input type="text" id="editornew" value={this.state.editornew} onChange={this.handleChange} />
								</FormGroup> : null}
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Langue'} : </Label>
								{this.renderLangue()}
							</FormGroup>

							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Date de parution'} : </Label>
								<Input type="date" id='parution' value={this.state.parution} onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Nombre de page'} : </Label>
								<Input type='number' id='nbrpage' value={this.state.nbrpage} onChange={this.handleChange}
									nBlur={() => this.validator.showMessageFor('nbrpage')}
								/>
								<span style={{ 'color': 'red' }} >{this.validator.message('nbrpage', this.state.nbrpage, ['required', { regex: '^[1-9]' }],
									{
										messages: {
											required: 'Champ obligatoire',
											regex: 'nombre de page ne doit pas être inférieur ou égal à zéro'
										}
									}
								)}</span>
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Licence'} : </Label>
								<Input type='text' id='licence' value={this.state.licence} onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Résumé'} : </Label>
								<Input type='textarea' id='resume' value={this.state.resume} onChange={this.handleChange} />
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'La couverture'} : </Label>
								<div className="text-center">
									<img alt="image" src={s.laUne ? API_LINK + s.laUne : null} width="200px" height="auto" className="rounded-12 border-blue border-3" />
								</div>
								<div style={{ width: '200px', margin: 'auto' }} className="text-center text-blue font-size-12">
									<span style={{ cursor: 'pointer' }} onClick={() => this.setState({
										deleteFile: true,
										laUne: null
									})}> Supprimer </span>
	          			&nbsp;&nbsp;&nbsp;
	          			<span style={{ cursor: 'pointer' }} onClick={() => this.selectPicture('laUne')}> {'Modifier'} </span>
									<Input type="file" onChange={(e) => { this.setState({ laUne: e.target.files[0] }) }} className="d-none" id="laUne" name="image" accept="image/*" />
								</div>
							</FormGroup>
							<FormGroup>
								<Label className="text-blue font-weight-bold"> {'Version numérique'} : </Label>
								<div className="text-center">
									<img alt="image" src={s.version ? API_LINK + s.version : null} width="200px" height="auto" className="rounded-12 border-blue border-3" />
								</div>
								<div style={{ width: '200px', margin: 'auto' }} className="text-center text-blue font-size-12">
									<span style={{ cursor: 'pointer' }} onClick={() => this.setState({
										deleteFile: true,
										version: null
									})}> Supprimer </span>
	          			&nbsp;&nbsp;&nbsp;
	          			<span style={{ cursor: 'pointer' }} onClick={() => this.selectEdition('version')}> {'Modifier'} </span>
									<Input type="file" onChange={(e) => { this.setState({ version: e.target.files[0] }) }} className="d-none" id="version" name="edition" accept="image/*" />
								</div></FormGroup>

							<br />
							<FormGroup>
								<Label htmlFor="sexe">{'L\'affichage'}: </Label>
								<br />
								<Row>
									<Col md="1"></Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Auto"
											value="Auto"
											checked={this.state.affichage === "Auto"}
											onChange={this.handleChangeEtat} />{"Auto"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Livre"
											value="Livre"
											checked={this.state.affichage === 'Livre'}
											onChange={this.handleChangeEtat} /> {"Livre"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Défiler"
											value="Défiler"
											checked={this.state.affichage === 'Défiler'}
											onChange={this.handleChangeEtat} /> {"Défiler"}
									</Col>
									<Col md="2">
										<Input type="radio"
											name="affichage"
											id="Diapositive"
											value="Diapositive"
											checked={this.state.affichage === 'Diapositive'}
											onChange={this.handleChangeEtat} /> {"Diapositive"}
									</Col>
								</Row>
							</FormGroup>
							<FormGroup>
								<Label htmlFor="sexe">{'La lecture'}: </Label>
								<br />
								<Row>
									<Col md="1"></Col>
									<Col md="4">
										<Input type="radio"
											name="lecture"
											id="De gauche à droite"
											value="De gauche à droite"
											checked={this.state.lecture === "De gauche à droite"}
											onChange={this.handleChangeEtat} />{"De gauche à droite"}
									</Col>
									<Col md="4">
										<Input type="radio"
											name="lecture"
											id="De droite à gauche"
											value="De droite à gauche"
											checked={this.state.lecture === 'De droite à gauche'}
											onChange={this.handleChangeEtat} /> {"De droite à gauche"}
									</Col>
								</Row>
							</FormGroup>
							<br />
							<Row>
								<Label>{'Autoriser l\’impression'}: </Label>
								{' '}
								<Col md="12">
									{this.renderImpression()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser le Téléchargement'}: </Label>
								{' '}
								<Col md="12">
									{this.renderLoad()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser le partage'}: </Label>
								{' '}
								<Col md="12">
									{this.renderPartage()}
								</Col></Row><br />
							<Row>
								<Label>{'Autoriser les commentaires'}: </Label>
								{' '}
								<Col md="12">
									{this.renderComment()}
								</Col>
							</Row>
							<FormGroup className="text-center">
								<Button color="blue" className="font-weight-bold px-5 rounded-12" onClick={() => this.setBook()} disabled={!this.checkValidationHandler()}> {'Modifier'} </Button>
							</FormGroup>
						</Form>
					</ModalBody>

				</Modal>

				<Modal isOpen={s.deleteProductModal} className="mt-5 pt-5">
					<ModalBody className="px-5 py-4">
						<div className="text-center font-size-18">{'Voulez vous vraiment supprimer cette ligne!'}</div>
						<br />
						<Row>
							<Col xs="6">
								<Button color="light" block className="rounded-12 shadow-sm" onClick={() => this.setState({ deleteProductModal: false })}> {'Annuler'} </Button>
							</Col>
							<Col xs="6">
								<Button color="danger" block className="rounded-12 shadow-sm" onClick={() => this.deleteBook(s.bookID)}> {'Supprimer'} </Button>
							</Col>
						</Row>
					</ModalBody>
				</Modal>

				{ /*  end modals  */}

			</div>
		);
	}
}

export default DashboardBook;