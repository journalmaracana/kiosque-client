
import React, { Component, Fragment } from 'react';
import { Row, Col, Label, Input, FormGroup, Button, Modal, Form, ButtonGroup, ModalBody, Container } from 'reactstrap';
import PhTable from '../../../../components/dashbord/Table';
import { AiFillEye } from 'react-icons/ai';
import { FaPencilAlt, FaTrash } from 'react-icons/fa';
import { API_LINK } from '../../../../utils/constant'
import SimpleReactValidator from 'simple-react-validator';
import axios from 'axios'
import moment from 'moment'
import Select from 'react-select'
import Pagination from '../../../../components/ui/Pagination'
const OptionButtons = (props) => {
  return (
    <Fragment>
      <Button color="warning" outline className="btn-sm mx-1" onClick={() => props.onView()}> <AiFillEye /> </Button>
      <Button color="success" outline className="btn-sm mx-1" onClick={() => props.onEdit()}> <FaPencilAlt /> </Button>
      <Button color="danger" outline className="btn-sm mx-1" onClick={() => props.onDelete()}> <FaTrash /> </Button>
    </Fragment>
  );
}

class NumberPress extends Component {
  constructor(props) {
    super(props);
    this.validator = new SimpleReactValidator({ autoForceUpdate: this });
    this.action = {
      user: this.props.userId,
      date: moment()
    }
    this.state = {
      headData: ['NOM', 'EDITION', 'NUMERO', 'DATE DE PARUTION ', 'LANGUE', 'ACTIONS'],
      modalOpen: false,
      presses: [],
      publications: [],
      allRegions: [],
      currentPage: 1,
      pagesCount: 0,
      totalItems: 0,
      addModalPub: false,
      pageSize: 50,
      modalView: false,
      modalDelete: false,
      modalEdit: false,
      loadingPress: false,
      namePub: '',
      numedition: '',
      parution: '',
      laUne: '',
      edition: '',
      page: '',
      user: '',
      lecture: '',
      affichage: 'Auto',
      commentaire: false,
      partage: false,
      region: '',
      imprimer: false,
      telechargement: false,
      userId: this.props.userId,
      pressId: '',

    }
  }
  componentDidMount() {
    this.getPressByUser()
    this.getPublications()
  }
  checkValidationHandler = () => {
    return this.validator.allValid()
  }

  selectPicture(inputID) {
    document.getElementById(inputID).click();
  }
  selectEdition(inputID) {
    document.getElementById(inputID).click();
  }
  deleteModalPress(item) {

    this.setState({
      pressId: item,
      modalDelete: !this.state.modalDelete
    })
  }
  resetState() {
    this.setState({
      namePub: '',
      numedition: '',
      parution: '',
      laUne: '',
      edition: '',
      page: '',
      user: '',
      lecture: '',
      affichage: 'Auto',
      commentaire: false,
      partage: false,
      region: '',
      imprimer: false,
      telechargement: false,
      pressId: '',
    })
  }
  editModalPress(item) {
    this.getRegion(item.namePub._id)
    this.setState({

      modalEdit: !this.state.modalEdit,
      pressId: item._id,
      parution: item.parution ? item.parution.split('T')[0] : null,
      namePub: item.namePub._id,
      affichage: item.affichage,
      commentaire: item.commentaire,
      partage: item.partage,
      lecture: item.lecture,
      imprimer: item.imprimer,
      numedition: item.numedition,
      laUne: item.laUne,
      edition: item.edition,
      user: item.user,
      region: item.region._id,
      telechargement: item.telechargement,
    })
    this.validator.hideMessageFor('numedition')
    this.validator.hideMessageFor('namePub')
    this.validator.hideMessageFor('parution')
  }
  viewModalPress(item) {
    console.log('itemnum', item.region)
    this.setState({
      modalView: !this.state.modalView,
      parution: item.parution ? item.parution.split('T')[0] : null,
      namePub: item.namePub,
      affichage: item.affichage,
      commentaire: item.commentaire,
      partage: item.partage,
      lecture: item.lecture,
      imprimer: item.imprimer,
      numedition: item.numedition,
      laUne: item.laUne,
      edition: item.edition,
      user: item.user,
      region: item.region,
      telechargement: item.telechargement,

    })
  }

  getPressByUser = async (index = this.state.currentPage, Limit = this.state.pageSize, ID = this.state.userId) => {
    await axios.get(API_LINK + 'v1/pressByuser/' + index + '/' + Limit + '/' + ID).then(res => {
      console.log('daymeeeeeeen', res.data)
      this.setState({
        presses: res.data.press.map(item => {
          return ([
            item.namePub.name, item.region?.entitled, item.numedition, item.parution ? moment(item.parution).format('DD-MM-YYYY') : null, item.namePub.langue,
            <OptionButtons onEdit={() => this.editModalPress(item)} onView={() => this.viewModalPress(item)} onDelete={() => this.deleteModalPress(item._id)} />
          ])
        }),
        loadingPress: false,
        pagesCount: res.data.totalPages,
        currentPage: res.data.currentPage
      })
    })
      .catch(err => {
        console.log(err)
        this.setState({
          loadingPublication: false,
        })
      })
  }
  deletePressNumber = async (id) => {
    await axios.delete(API_LINK + 'v1/deletepress/' + id).then(res => {
      this.setState({
        modalDelete: false
      })
      this.getPressByUser()
      console.log('numéro à été supprimer avec succes')

    })
      .catch(err => {
        console.log(` Une erreur s'est produite lors de la suppression de numéro de press `, err)
        this.setState({
          modalDelete: false
        })
      })

  }
  getPublications = async () => {
    console.log('here')
    let URLGet = 'v1/publication'
    await axios.get(API_LINK + URLGet)
      .then(response => {
        console.log('publication', response.data)
        this.setState({
          publications: response.data
        })
      })
      .catch(err => {
        console.log('err', err)
      })
  }
  getRegion = async (id) => {
    let URLGet = 'v1/pubRegion/'
    await axios.get(API_LINK + URLGet + id)
      .then(response => {
        this.setState({
          allRegions: response.data,
        },()=> {
            console.log('allRegions',this.state.allRegions)
            if(this.state.allRegions.length == 1 ) {
              this.setState({
                  region : this.state.allRegions[0]._id
              })
            }else{
              this.setState({
                region : ''
            })
            }
        })
      })
      .catch(err => {
        console.log('err', err)
      })
  }
  SingleSelectPub = () => {
    const options = this.state.publications.map((item) => {
      return { value: item._id + 'T' + item.page, label: item.name };
    })
    return (
      <FormGroup>
        <Select
          className="basic-single"
          classNamePrefix="select"
          isClearable
          isSearchable
          name="namePub"
          id="namePub"
          options={options}
          value={options.filter(({ value }) => value.split('T')[0] === this.state.namePub)}
          onChange={this.handleChangeSelectPub}
          onBlur={() => this.validator.showMessageFor('namePub')}
        />
          <span style={{ 'color': 'red' }} >  {this.validator.message('namePub', this.state.namePub, 'required',
            {
              messages: {
                required: 'Champ obligatoire'
              }
            })}</span>

      </FormGroup>
    )
  }
  SingleSelectRegion = () => {

    const options = this.state.allRegions.map(item => {
      return { label: item.entitled, value: item._id };
    })
    return (
      <FormGroup>
        <Select
          className="basic-single"
          classNamePrefix="select"
          isClearable
          isSearchable
          name="region"
          id="region"
          options={options}
          value={options.filter(({ value }) => value === this.state.region)}
          onChange={this.handleChangeRegion}
          onBlur={() => this.validator.showMessageFor('region')}
        />
          <span style={{ 'color': 'red' }} >  {this.validator.message('region', this.state.region, 'required',
            {
              messages: {
                required: 'Champ obligatoire'
              }
            })}</span>

      </FormGroup>
    )
  }
  handleChangeSelectPub = (event) => {
    if (event) {
      this.setState({
        namePub: event.value.split('T')[0],
        page: event.value.split('T')[1],
      }, () => {
        this.getRegion(this.state.namePub)
      })
    } else {
      this.setState({
        namePub: null,
        page: '',
      })
    }
  }
  handleChangeRegion = (event) => {
    if (event) {
      this.setState({
        region: event.value
      })
    } else {
      this.setState({
        region: ''
      })
    }
  }
  renderPartage = () => {
    return <>
      <FormGroup>
        {' '}
        <ButtonGroup>
          <Button
            color="danger"
            className="btn-sm"
            onClick={() => this.setState({ partage: false })}
            active={this.state.partage === false}
          >
            {'Non'}
          </Button>
          <Button
            color="success"
            className="btn-sm"
            onClick={() => this.setState({ partage: true })}
            active={this.state.partage === true}
          >
            {'Oui'}
          </Button>
        </ButtonGroup>
      </FormGroup>
    </>
  }
  renderImpression = () => {
    return <>
      <FormGroup>
        {' '}
        <ButtonGroup>
          <Button
            color="danger"
            className="btn-sm"
            onClick={() => this.setState({ imprimer: false })}
            active={this.state.imprimer === false}
          >
            {'Non'}
          </Button>
          <Button
            color="success"
            className="btn-sm"
            onClick={() => this.setState({ imprimer: true })}
            active={this.state.imprimer === true}
          >
            {'Oui'}
          </Button>
        </ButtonGroup>
      </FormGroup>
    </>
  }
  renderLoad = () => {
    return <>
      <FormGroup>
        {' '}
        <ButtonGroup>
          <Button
            color="danger"
            className="btn-sm"
            onClick={() => this.setState({ telechargement: false })}
            active={this.state.telechargement === false}
          >
            {'Non'}
          </Button>
          <Button
            color="success"
            className="btn-sm"
            onClick={() => this.setState({ telechargement: true })}
            active={this.state.telechargement === true}
          >
            {'Oui'}
          </Button>
        </ButtonGroup>
      </FormGroup>
    </>
  }
  handleChange = (e) => {
    if (e) {
      this.setState({
        [e.target.id]: e.target.value
      })
    }
  }
  renderComment = () => {
    return <>
      <FormGroup>
        {' '}
        <ButtonGroup>
          <Button
            color="danger"
            className="btn-sm"
            onClick={() => this.setState({ commentaire: false })}
            active={this.state.commentaire === false}
          >
            {'Non'}
          </Button>
          <Button
            color="success"
            className="btn-sm"
            onClick={() => this.setState({ commentaire: true })}
            active={this.state.commentaire === true}
          >
            {'Oui'}
          </Button>
        </ButtonGroup>
      </FormGroup>
    </>
  }
  addPress = () => {

    let URLAjout = 'v1/press';
    let form_data = new FormData();      // pour Ajout ou Edit des champs de type file
    form_data.append('namePub', this.state.namePub);
    form_data.append('region', this.state.region);
    form_data.append('numedition', this.state.numedition);
    form_data.append('parution', this.state.parution.split('T')[0]);
    form_data.append('user', this.state.userId);
    form_data.append('edition', this.state.edition);
    form_data.append('image', this.state.laUne);
    //form_data.append('page', this.state.page);
    form_data.append('lecture', this.state.lecture);
    form_data.append('telechargement', this.state.telechargement);
    form_data.append('affichage', this.state.affichage);
    form_data.append('partage', this.state.partage);
    form_data.append('commentaire', this.state.commentaire);
    form_data.append('imprimer', this.state.imprimer);
    form_data.append('action', JSON.stringify(this.action));
    axios.post(API_LINK + URLAjout, form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        this.resetState()
        this.getPressByUser()
        this.setState({
          addModalPub: false,
        })
      })
      .catch(err => {
        console.log("erreur", err.response)

      })

  };
  setPress= () => {

    let URLAjout = 'v1/press/';
    let form_data = new FormData();      // pour Ajout ou Edit des champs de type file
    form_data.append('namePub', this.state.namePub);
    form_data.append('region', this.state.region);
    form_data.append('numedition', this.state.numedition);
    form_data.append('parution', this.state.parution.split('T')[0]);
    form_data.append('user', this.state.userId);
    form_data.append('edition', this.state.edition);
    form_data.append('image', this.state.laUne);
    //form_data.append('page', this.state.page);
    form_data.append('lecture', this.state.lecture);
    form_data.append('telechargement', this.state.telechargement);
    form_data.append('affichage', this.state.affichage);
    form_data.append('partage', this.state.partage);
    form_data.append('commentaire', this.state.commentaire);
    form_data.append('imprimer', this.state.imprimer);
    form_data.append('action', JSON.stringify(this.action));
    axios.put(API_LINK + URLAjout + this.state.pressId, form_data, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
      .then(res => {
        this.resetState()
        this.getPressByUser()
        this.setState({
          modalEdit: false,
        })
      })
      .catch(err => {
        console.log("erreur", err.response)

      })

  };
  render() {
    const s = this.state;
    const p = this.props;
    const externalCloseBtn = <button className="close" style={{ position: 'fixed', top: '15px', right: '15px', color: 'white' }} onClick={() => this.setState({ modalView: false, modalEdit: false, addModalPub: false })}>&times;</button>;
    return (
      <div>
        {/*close btn*/}
        <Row>
          <Col lg="10" md="9" className="press">
            <h2 className="text-center text-blue mt-3"> {'MES ARCHIVES PRESSE'} </h2>
            <br />
            <Container>
              <Row>
                <Col xs="8">
                  <Input type="search" name="" className="shadow-sm border-0 form-control-sm w-50" placeholder={'Rechercher'} />
                </Col>
                <Col xs="4" className="text-right">
                  <Button color="blue" className="rounded-12 btn-sm px-5" onClick={() => this.setState({ addModalPub: !this.state.addModalPub }, () => { this.resetState() }) }> {"Ajouter"} </Button>
                </Col>
              </Row>

            </Container>
          </Col>
        </Row>
        {s.presses.length > 0 ?
          <PhTable
            headData={s.headData}
            bodyData={s.presses}
          />
          : <div className="text-danger text-center font-weight-bold">
            {" "}
            <br></br>
            {"Liste des numéros de presse est vide!"}{" "}
          </div>}
        <br ></br>
        {s.presses.length > 0 ?
          <div className="d-flex justify-content-center">
            <Pagination
              currentPage={s.currentPage}
              pagesCount={s.pagesCount}
              handleClick={this.handleClick}
            />
          </div>
          : null}
        <Modal isOpen={s.modalView} external={externalCloseBtn} size="lg">
          <ModalBody className="bg-ddd rounded-15 pb-4">

                  <h5 className="text-blue text-center">
                    {" "}
                    {"Numéro "}{" "}
                  </h5>

            <Row>
              <Col md={"12"}>

                  <div className="rounded-12 border-blue border-3" width="100%" height="auto">
                    <img src={this.state.laUne ? API_LINK + this.state.laUne : null} width="100%" height="auto" className="rounded-12 border-blue border-3" />
                  </div>


              </Col>
            </Row>
            <hr />
            <b className="text-blue font-size-14">
              {"Titre de Presse:"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.namePub?.name}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {"Edition :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.region?.entitled}
            </div>

            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Numéro de parution:"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.numedition}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Langue :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">
              {this.state.namePub?.langue}
            </div>

            <hr />

            <b className="text-blue font-size-14">
              {" "}
              {"Date de Parution:"}{" "}
            </b><div className="market-product-info font-size-14 text-muted">
              {moment(this.state.parution).format('DD-MM-YYYY')}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"L'affichage :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted"> {this.state.affichage}
            </div><hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Lecture :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted"> {this.state.lecture}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Autoriser le téléchargement :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.telechargement === false ? 'Non' : 'Oui'}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Autoriser le l'impression :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.imprimer === false ? 'Non' : 'Oui'}
            </div>
            <hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Autoriser les commentaires :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.commentaire === false ? 'Non' : 'Oui'}
            </div><hr />
            <b className="text-blue font-size-14">
              {" "}
              {"Autoriser le partage :"}{" "}</b>
            <div className="market-product-info font-size-14 text-muted">{this.state.partage === false ? 'Non' : 'Oui'}
            </div>
          </ModalBody>
        </Modal>


        <Modal isOpen={s.modalDelete} className="mt-5 pt-5">
          <ModalBody className="px-5 py-4">
            <div className="text-center font-size-18">{'Voulez vous vraiment supprimer cette ligne!'}</div>
            <br />
            <Row>
              <Col xs="6">
                <Button color="light" block className="rounded-12 shadow-sm" onClick={() => this.setState({ modalDelete: false })}> {'Annuler'} </Button>
              </Col>
              <Col xs="6">
                <Button color="danger" block className="rounded-12 shadow-sm" onClick={() => this.deletePressNumber(s.pressId)}> {'Supprimer'} </Button>
              </Col>
            </Row>
          </ModalBody>
        </Modal>

        <div>

       <Modal isOpen={s.modalEdit}   external={externalCloseBtn} size="lg">
       <ModalBody>
              <Form className="register-patient-form">
                <div className="bg-white rounded-15 p-5 shadow-sm mt-4">
                  <h3 className="text-center text-blue mb-4"> {('Ajouter un numéro de presse')} </h3>

                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Le titre de presse'} : </Label>
                    {this.SingleSelectPub()}
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Edition'} : </Label>
                    {this.SingleSelectRegion()}
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Date de parution'}</Label>
                    <Input type="date" id="parution" name="parution" value={this.state.parution} onChange={this.handleChange} />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Numéro de parution'}</Label>
                    <Input type='text' id='numedition' value={this.state.numedition} onChange={this.handleChange}
                    onBlur={() => this.validator.showMessageFor('numedition')}
                    />
                      <span style={{ 'color': 'red' }} >  {this.validator.message('numedition', this.state.numedition, 'required',
                        {
                          messages: {
                            required: 'Champ obligatoire'
                          }
                        })}</span>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'La couverture'} : </Label>
                    <div className="text-center">
									<img alt="image" src={s.laUne ? API_LINK + s.laUne : null} width="200px" height="auto" className="rounded-12 border-blue border-3" />
								</div>
								<div style={{ width: '200px', margin: 'auto' }} className="text-center text-blue font-size-12">
									<span style={{ cursor: 'pointer' }} onClick={() => this.setState({
										deleteFile: true,
										laUne: null
									})}> Supprimer </span>
	          			&nbsp;&nbsp;&nbsp;
	          			<span style={{ cursor: 'pointer' }} onClick={() => this.selectPicture('laUne')}> {'Modifier'} </span>
									<Input type="file" onChange={(e) => { this.setState({ laUne: e.target.files[0] }) }} className="d-none" id="laUne" name="image" accept="image/*" />
								</div>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{"Version numérique"}:</Label>
                    <br />
                    <div className="text-center">
									<img alt="image" src={s.edition ? API_LINK + s.edition : null} width="200px" height="auto" className="rounded-12 border-blue border-3" />
								</div>
								<div style={{ width: '200px', margin: 'auto' }} className="text-center text-blue font-size-12">
									<span style={{ cursor: 'pointer' }} onClick={() => this.setState({
										deleteFile: true,
										edition: null
									})}> Supprimer </span>
	          			&nbsp;&nbsp;&nbsp;
	          			<span style={{ cursor: 'pointer' }} onClick={() => this.selectPicture('edition')}> {'Modifier'} </span>
									<Input type="file" onChange={(e) => { this.setState({ edition: e.target.files[0] }) }} className="d-none" id="edition" name="edition" accept="image/*" />
								</div>

                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{('Affichage')}: </Label>

                    <Row>
                      <Col md="1"></Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Auto"
                          checked={this.state.affichage === "Auto"}
                          onChange={this.handleChange} />{"Auto"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Livre"
                          checked={this.state.affichage === 'Livre'}
                          onChange={this.handleChange} /> {"Livre"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Défiler"
                          checked={this.state.affichage === 'Défiler'}
                          onChange={this.handleChange} /> {"Défiler"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Diapositive"
                          checked={this.state.affichage === 'Diapositive'}
                          onChange={this.handleChange} /> {"Diapositive"}
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Lecture'}: </Label>
                    <br />
                    <Row>
                      <Col md="1"></Col>
                      <Col md="4">
                        <Input type="radio"
                          name="lecture"
                          id="lecture"
                          value="De gauche à droite"
                          checked={this.state.lecture === "De gauche à droite"}
                          onChange={this.handleChange} />{"De gauche à droite"}
                      </Col>
                      <Col md="4">
                        <Input type="radio"
                          name="lecture"
                          id="lecture"
                          value="De droite à gauche"
                          checked={this.state.lecture === 'De droite à gauche'}
                          onChange={this.handleChange} /> {"De droite à gauche"}
                      </Col>
                    </Row>
                  </FormGroup>
                  <br />

                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser le partage'} : </Label>
                    {this.renderPartage()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser l\'impression'} : </Label>
                    {this.renderImpression()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser le téléchargement'} : </Label>
                    {this.renderImpression()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser les commentaires'} : </Label>
                    {this.renderComment()}
                  </FormGroup>

                </div>

                <br />
                <FormGroup className="text-center">
                  <Button color="blue" className="px-5 font-weight-bold rounded-12" onClick={() => this.setPress()} > {'Modifier'} </Button>
                </FormGroup>



              </Form></ModalBody>

                  </Modal>
              </div>

        <div>
          <Modal isOpen={s.addModalPub} size="lg" external={externalCloseBtn}>
            <ModalBody>
              <Form className="register-patient-form">
                <div className="bg-white rounded-15 p-5 shadow-sm mt-4">
                  <h3 className="text-center text-blue mb-4"> {('Ajouter un numéro de presse')} </h3>

                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Le titre de presse'} : </Label>
                    {this.SingleSelectPub()}
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Edition'} : </Label>
                    {this.SingleSelectRegion()}
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Date de parution'}</Label>
                    <Input type="date" id="parution" name="parution" onChange={this.handleChange} />
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Numéro de parution'}</Label>
                    <Input type='text' id='numedition' onChange={this.handleChange} onBlur={() => this.validator.showMessageFor('numedition')}
                     />
                       <span style={{ 'color': 'red' }} >  {this.validator.message('numedition', this.state.numedition, 'required',
                         {
                           messages: {
                             required: 'Champ obligatoire'
                           }
                         })}</span>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'La couverture'} : </Label>
                    <Button color="blue" className="rounded-12 px-4" size="sm" onClick={() => this.selectPicture('laUne')}> {('Séléctionner la couverture')} </Button>
                    <Input type="file" className="d-none" name='image' id="laUne" accept="image/*" onChange={(e) => { this.setState({ laUne: e.target.files[0] }) }}
                    onBlur={() => this.validator.showMessageFor('laUne')}
                     />
                       <span style={{ 'color': 'red' }} >  {this.validator.message('laUne', this.state.laUne, 'required',
                         {
                           messages: {
                             required: 'Champ obligatoire'
                           }
                         })}</span>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{"Version numérique"}:</Label>
                    <br />
                    <Button color="blue" className="rounded-12 px-4" size="sm" onClick={() => this.selectEdition('edition')}> {('Séléctionner la couverture')} </Button>
                    <Input type="file" className="d-none" name='edition' id="edition" accept="image/*" onChange={(e) => { this.setState({ edition: e.target.files[0] }) }}
                     onBlur={() => this.validator.showMessageFor('edition')}
                     />
                       <span style={{ 'color': 'red' }} >  {this.validator.message('edition', this.state.edition, 'required',
                         {
                           messages: {
                             required: 'Champ obligatoire'
                           }
                         })}</span>

                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{('Affichage')}: </Label>

                    <Row>
                      <Col md="1"></Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Auto"
                          checked={this.state.affichage === "Auto"}
                          onChange={this.handleChange} />{"Auto"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Livre"
                          checked={this.state.affichage === 'Livre'}
                          onChange={this.handleChange} /> {"Livre"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Défiler"
                          checked={this.state.affichage === 'Défiler'}
                          onChange={this.handleChange} /> {"Défiler"}
                      </Col>
                      <Col md="2">
                        <Input type="radio"
                          name="affichage"
                          id="affichage"
                          value="Diapositive"
                          checked={this.state.affichage === 'Diapositive'}
                          onChange={this.handleChange} /> {"Diapositive"}
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label className="text-blue font-weight-bold" font-size-14>{'Lecture'}: </Label>
                    <br />
                    <Row>
                      <Col md="1"></Col>
                      <Col md="4">
                        <Input type="radio"
                          name="lecture"
                          id="lecture"
                          value="De gauche à droite"
                          checked={this.state.lecture === "De gauche à droite"}
                          onChange={this.handleChange} />{"De gauche à droite"}
                      </Col>
                      <Col md="4">
                        <Input type="radio"
                          name="lecture"
                          id="lecture"
                          value="De droite à gauche"
                          checked={this.state.lecture === 'De droite à gauche'}
                          onChange={this.handleChange} /> {"De droite à gauche"}
                      </Col>
                    </Row>
                  </FormGroup>
                  <br />

                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser le partage'} : </Label>
                    {this.renderPartage()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser l\'impression'} : </Label>
                    {this.renderImpression()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser le téléchargement'} : </Label>
                    {this.renderImpression()}
                  </FormGroup>
                  <br />
                  <FormGroup>
                    <Label className="font-size-14 text-blue font-weight-bold"> {'Autoriser les commentaires'} : </Label>
                    {this.renderComment()}
                  </FormGroup>

                </div>

                <br />
                <FormGroup className="text-center">
                  <Button color="blue" className="px-5 font-weight-bold rounded-12" onClick={() => this.addPress()} disabled={!this.checkValidationHandler()}> {'Ajouter'} </Button>
                </FormGroup>



              </Form></ModalBody>
          </Modal>
        </div>
      </div>
    );
  }
}


export default (NumberPress);