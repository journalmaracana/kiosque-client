import React, { Component } from 'react';
import { Row, Col, Container, Button, Input } from 'reactstrap';
//import Preloader from '../../../components/ui/Preloader';
import TitrePress from './TitrePress';
import NumberPres from './NumeroPress';
import DashLayout from '../../../components/dashbord/Layout'


import styles from '../../../styles/Press.module.css';

class DashboardPress extends Component {
	constructor(props) {
    super(props);
    console.log('joooood',this.props)


    this.state = {
      langLoaded: false,
      activeTab: 1,
      activeView: <TitrePress />,
      addModal: null,
	  addModalPress: false
    }
  }

  componentDidMount() {
    // load language
    // i18n.loadNamespaces('translations', () => {
    //   this.setState({
    //     langLoaded: true
    //   })
    // });
  }

	render() {
		// state short
    const s = this.state;
	const externalCloseBtn = <button className="close" style={{ position: 'fixed', top: '15px', right: '15px', color: 'white' }} onClick={() => this.setState({ addModalPress: false, addModal: null, activeView: s.activeView})}>&times;</button>;


    // check language if loaded for remove preloader
    // if(!s.langLoaded)
    // {
    //   return <Preloader />;
    // }

		return(   
            <div className={styles.container}>
			<div className="mydashboard-right float-right bg-white ">
               
              <DashLayout />  
				{/*close btn*/}
				
				<Row className="mx-0 press" >
					<Col lg="2" md="3" className="mydashboard-filter ">
						<ul className="mydashboard-filter-fixed text-center">
							<li className={`mb-3 mt-5 ${s.activeTab === 1 ? 'text-blue' : ''}`} onClick={() => this.setState({activeView: <TitrePress userId={this.props.userId}/>, activeTab: 1})}> {'Titre de presse'} </li>
							<li className={`mb-3 ${s.activeTab === 2 ? 'text-blue' : ''}`} onClick={() => this.setState({activeView: <NumberPres userId={this.props.userId}/>,addModal:'num', activeTab: 2})}> {'Numéro de presse'} </li>
						</ul>
					</Col>
					<Col lg="10" md="9" className="press">
						<br />
						<Container>
							<Row>
								<Col xs="8">
								</Col>
								<Col xs="4" className="text-right">
								</Col>
							</Row>
							<br />
							<Row>
								<Col xs="12" md="12" lg="12">
									{this.state.activeView}
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
				
			</div>
		</div>);
	}
}

export default DashboardPress;