import React, { Component, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Preloader from "../../components/ui/Prealoder";
import { useDispatch, useSelector } from "react-redux";
import { validateCompte } from "../../store/actions/Auth";

function CompteValidation(props) {
  const router = useRouter();

  const loading = useSelector((state) => state.auth.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!props.isAuth && router.query.id) {
      const token = router.query.id;
      console.log("routergggg", router);
      dispatch(validateCompte(router, token));
    }
  }, [router.query.id]);

  if (loading) {
    return <Preloader />;
  } else {
    return (
      <div className="text-danger text-center my-5 mx-5">
        <h4>Délai de confirmation expiré</h4>
      </div>
    );
  }
}

export default CompteValidation;
