import React, { Component, Fragment } from "react";
import { Row, Col, Container } from "reactstrap";
import { API_LINK } from "../../utils/constant";
import Pagination from "../../components/Pagination";
import ArticlePreviewMedium from "../../components/articlesPreview/mediumArticle";
import styles from "../../styles/Threads.module.css";
import Header from "../../components/publicPages/Header";
import NavBar from "../../components/publicPages/Navbar";
import { SideBarAct } from "../../components/Sidebar";
import axios from "axios";
import { Preloader } from "../../components/ui/Prealoder.js";
import { calculdatePublié } from "../../utils/functions";
import { withRouter } from "next/router";
class Tags extends Component {
  constructor() {
    super();
    this.state = {
      articles: [],
      loading: true,
      keyWord: "",

      currentPage: 1,
      pagesCount: 0,
      pageSize: 10,
    };
  }
  componentDidMount = () => {
    this.setState(
      {
        keyWord: this.props.router.query?.id,
      },
      () => {
        this.getArticlesByKey(
          this.state.keyWord,
          this.state.currentPage,
          this.state.pageSize
        );
      }
    );
  };
  //------------------ get -----------------

  getArticlesByKey = (key, Index, Limit) => {
    axios
      .get(API_LINK + "v1/articlesByKeyWord/" + Index + "/" + Limit + "/" + key)
      .then((response) => {
        this.setState({
          articles: response.data.articles,
          currentPage: parseInt(response.data.currentPage),
          pagesCount: response.data.totalPages,
          loading: false,
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };

  //------------------ END get -----------------
  //-----------------handle change --------------------

  handleClick = (index) => {
    // Handle Pagination
    if (index > 0) {
      this.setState(
        {
          currentPage: index,
          //currentPage: e,
        },
        () => {
          this.getArticlesByKey(
            this.state.keyWord,
            this.state.currentPage,
            this.state.pageSize
          );
        }
      );
    }
    window.scrollTo(0, 0);
  };
  //----------------- END handle change --------------------

  render = () => {
    const { articles, loading, keyWord } = this.state;
    return (
      <div>
        <div className="header">
          <Header />
          <NavBar />
        </div>
        <div className="threads-content">
          <Container>
            <Row className="text-center">
              <Col md="8">
                <a href="#">
                  <img src="img/pub.jpg" alt="" />
                </a>
              </Col>
              <Col md="4">
                <a href="#">
                  <img src="img/newmap.jpg" alt="" />
                </a>
              </Col>
            </Row>
            <Row>
              <Col md="8" className="partie-home-gauche">
                <div className={styles["titre-ligue1-div"]}>
                  <Row>
                    <Col md="12">{"ACTUALITÉ LIÉS AU SUJET " + keyWord}</Col>
                  </Row>
                </div>
                {articles.length ? (
                  articles.map((article) => {
                    return (
                      <ArticlePreviewMedium
                        link={"/Actualite/" + article._id}
                        image={API_LINK + article.image}
                        name={
                          article.author?.type == "Auteur"
                            ? article.author.lastname +
                              " " +
                              article.author.name
                            : null
                        }
                        title={article.title}
                        category={keyWord}
                        date={calculdatePublié(article.date)}
                        ID={article._id}
                      />
                    );
                  })
                ) : loading ? (
                  <div className="text-center">
                    <Preloader />
                  </div>
                ) : (
                  <>
                    <div className="text-danger text-center font-weight-bold">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  </>
                )}
              </Col>

              <Col md="4">
                <SideBarAct />
              </Col>
            </Row>
            <Row>
              <Col md="8">
                <div className="d-flex justify-content-center">
                  <Pagination
                    currentPage={this.state.currentPage}
                    pagesCount={this.state.pagesCount}
                    handleClick={this.handleClick}
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  };
}
export default withRouter(Tags);
