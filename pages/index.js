import Head from "next/head";
import styles from "../styles/Home.module.css";
import { Container, Row, Col, Button } from "reactstrap";
import Slider from "../components/Slider";
import Product from "../components/Product";
import Link from "next/link";

import {
  Testimony,
  PharmaRegister,
  Newsletter,
  Followus,
  Donations,
  Search,
  Donate,
  Recruturs,
  Candidats,
  PhSearch,
} from "../components/Sidebar";
import Article from "../components/Article";
import JobView from "../components/Job";
import DonationView from "../components/Donation";
import ExpView from "../components/Experience";
import VideoView from "../components/Video";
import Layout from "../components/publicPages/Layout";
import L from "../components/dashbord/Layout";
import { useRouter } from "next/router";

export default function Home(props) {
  console.log("props", props);
  const router = useRouter();
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>{" "}
      <Layout {...props} router={router}>
        <Container>
          <Row>
            <Col md="8">
              <section className="home-products mt-5">
                <h5 className="text-blue text-center">
                  {" "}
                  {"home.products.title"}{" "}
                </h5>
                <Row>
                  {[...Array(12)].map((data, key) => {
                    return (
                      <Col lg="3" md="4" xs="6" className="mb-2" key={key}>
                        <Product />
                      </Col>
                    );
                  })}
                </Row>
                <Row>
                  <Col className="text-center">
                    <Button
                      type="button"
                      className="rounded-pill bg-blue border-0 mt-3 px-4"
                    >
                      {" "}
                      {"buttons.more"}{" "}
                    </Button>
                  </Col>
                </Row>
              </section>

              <section className="home-threads mt-5">
                <h2 className="text-blue text-center">
                  {" "}
                  {"header.health_threads".toUpperCase()}{" "}
                </h2>
                <Row>
                  <Col md="12" className="mb-3">
                    <Article
                      lg
                      title="Accident vasculaire cérébral"
                      description="Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme"
                    />
                  </Col>
                  <Col md="6" className="mb-3">
                    <Article
                      sm
                      title="Accident vasculaire cérébral"
                      description="Le Lorem Ipsum est simplement du faux texte employé dans la"
                    />
                  </Col>
                  <Col md="6" className="mb-3">
                    <Article
                      sm
                      title="Accident vasculaire cérébral"
                      description="Le Lorem Ipsum est simplement du faux texte employé dans la"
                    />
                  </Col>
                </Row>
              </section>

              <section className="home-jobs mt-5">
                <h2 className="text-blue text-center">
                  {" "}
                  {"home.jobs.title".toUpperCase()}{" "}
                </h2>
                <Row>
                  <Col md="12">
                    {[...Array(5)].map((data, key) => {
                      return <JobView key={key} />;
                    })}
                  </Col>
                </Row>
                <Row>
                  <Col className="text-center">
                    <Button
                      type="button"
                      className="rounded-pill bg-blue border-0 mt-3 px-4"
                    >
                      {" "}
                      {"buttons.more"}{" "}
                    </Button>
                  </Col>
                </Row>
              </section>

              <section className="home-donations mt-5">
                <h2 className="text-blue text-center">
                  {" "}
                  {"home.donations.title".toUpperCase()}{" "}
                </h2>
                <Row>
                  {[...Array(8)].map((data, key) => {
                    return (
                      <Col xs="6" md="4" lg="3" className="mt-4" key={key}>
                        <DonationView />
                      </Col>
                    );
                  })}
                </Row>
                <Row>
                  <Col className="text-center">
                    <Button
                      type="button"
                      className="rounded-pill bg-blue border-0 mt-3 px-4"
                    >
                      {" "}
                      {"buttons.more"}{" "}
                    </Button>
                  </Col>
                </Row>
              </section>

              <section className="home-exp mt-5">
                <h2 className="text-blue text-center">
                  {" "}
                  {"home.exp.title".toUpperCase()}{" "}
                </h2>
                <Row>
                  {[...Array(4)].map((data, key) => {
                    return (
                      <Col md="12" key={key}>
                        <ExpView />
                      </Col>
                    );
                  })}
                </Row>
                <Row>
                  <Col className="text-center">
                    <Button
                      type="button"
                      className="rounded-pill bg-blue border-0 mt-3 px-4"
                    >
                      {" "}
                      {"buttons.more"}{" "}
                    </Button>
                  </Col>
                </Row>
              </section>

              <section className="home-videos mt-5">
                <h2 className="text-blue text-center">
                  {" "}
                  {"home.videos.title".toUpperCase()}{" "}
                </h2>
                <Row>
                  {[...Array(4)].map((data, key) => {
                    return (
                      <Col md="12" key={key}>
                        <VideoView />
                      </Col>
                    );
                  })}
                </Row>
                <Row>
                  <Col className="text-center">
                    <Button
                      type="button"
                      className="rounded-pill bg-blue border-0 mt-3 px-4"
                    >
                      {" "}
                      {"buttons.more"}{" "}
                    </Button>
                  </Col>
                </Row>
              </section>
            </Col>
            <Col md="4">
              <Testimony />
              <PhSearch />
              <PharmaRegister />
              <Donations />
              <Newsletter />
              <Followus />
              <Search />
              <Donate />
              <Recruturs />
              <Candidats />
            </Col>
          </Row>
        </Container>
      </Layout>
    </div>
  );
}
