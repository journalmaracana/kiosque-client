import React, { Component } from "react";
import { FormGroup, Label, Input, Row, Col, Button } from "reactstrap";
import LoginForm from "../../components/Auth/LoginForm";
import { SmallContainer } from "../../components/ui//Containers";
import { RegsterNavIcon } from "../../components/Auth/Register";
import { AiOutlineArrowLeft } from "react-icons/ai";
import UnprotectedRoute from "../../components/ui/routes/UnprotectedRoute";
import { withRouter } from "next/router";
import Axios from "../../utils/axios_config";
import SimpleReactValidator from "simple-react-validator";
import { VALIDATOR_MESSAGES } from "../../utils/constants";
import Head from "next/head";
import { alert } from "../../components/ui/alertSwal/alertSwal";
class PasswordRecoveryPage extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  state = {
    mail: "",
  };
  handelChange = (e) => {
    if (e) {
      this.setState({
        [e.target.name]: e.target.value,
      });
    }
  };

  onSubmit = () => {
    if (this.validator.allValid()) {
      const email = {
        mail: this.state.mail,
        lien: "http://localhost:3000/new_password",
      };
      Axios.post("forgot" + "/", email)
        .then((res) => {
          if (res.data.message) {
            alert("Error", "Oops..! " + res.data.message, "error");
          } else {
            alert(
              "Confirmation",
              "Veuillez consulter votre boite mail un message à été envoyé pour réinitialiser votre mot de passe",
              "success"
            );
          }
        })
        .catch((err) => {
          alert("Error", "Oops..! " + err, "error");
        });
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  render() {
    return (
      <UnprotectedRoute {...this.props}>
        <div>
          <Head>
            <title>password recovery</title>
          </Head>
          <RegsterNavIcon icon={<AiOutlineArrowLeft size={30} />} to="/login" />
          <br />
          <br />
          <br />
          <br />
          <br />
          <SmallContainer className="bg-white shadow-sm mt-5 p-5 rounded-12">
            <h3 className="text-blue text-center">Récupération de compte</h3>
            <FormGroup className="mt-3">
              <Label className="text-blue font-weight-bold">
                Entrer l'adress email
              </Label>
              <Row>
                <Col md="9">
                  <Input
                    type="email"
                    required
                    id="mail"
                    name="mail"
                    value={this.state.mail}
                    onChange={this.handelChange}
                  />
                  <span style={{ color: "red" }}>
                    {this.validator.message(
                      "mail",
                      this.state.mail,
                      "required|email"
                    )}
                  </span>
                </Col>
                <Col md="3">
                  <Button color="blue" block onClick={this.onSubmit}>
                    {" "}
                    {"Envoyer"}{" "}
                  </Button>
                </Col>
              </Row>
            </FormGroup>
          </SmallContainer>
        </div>
      </UnprotectedRoute>
    );
  }
}

export default PasswordRecoveryPage;
