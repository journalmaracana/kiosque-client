import { FormContainer } from "../../components/ui/Containers";
import { RegisterSelect, RegsterNavIcon } from "../../components/Auth/Register";
import { AiFillHome } from "react-icons/ai";
import Head from "next/head";
import Image from "next/image";
import UnprotectedRoute from "../../components/ui/routes/UnprotectedRoute";

const accountsType = [
  {
    name: "Editeur",
    icon: (
      <Image alt="editeur" src="/images/doctor.svg" width="70%" height="auto" />
    ),
    to: "/register/editeur",
  },
  {
    name: "Auteur",
    icon: (
      <Image
        alt="auteur"
        src={"/images/doctor.svg"}
        width="70%"
        height="auto"
      />
    ),
    to: "/register/auteur",
  },
  {
    name: "Lecteur",
    icon: (
      <Image
        alt="lecteur"
        src={"/images/doctor.svg"}
        width="70%"
        height="auto"
      />
    ),
    to: "/register/lecteur",
  },
];

const RegisterPage = (props) => {
  return (
    <UnprotectedRoute {...props}>
      <div>
        <Head>
          {" "}
          <title>register</title>
        </Head>
        <RegsterNavIcon icon={<AiFillHome size={30} />} to="/" />
        <FormContainer>
          <RegisterSelect data={accountsType} />
        </FormContainer>
      </div>
    </UnprotectedRoute>
  );
};

export default RegisterPage;
