import React, { Component } from "react";
import { Form, FormGroup, Button } from "reactstrap";
import { FormContainer } from "../../components/ui/Containers";
import { RegsterNavIcon } from "../../components/Auth/Register";
import Head from "next/head";
import { AiOutlineArrowLeft } from "react-icons/ai";
import {
  IdentifierInformation,
  PersonalInformation,
  TitleForm,
} from "../../components/Auth/RegisterForm";
import styles from "../../components/Auth/RegisterForm.module.css";
import Axios from "../../utils/axios_config";
import SimpleReactValidator from "simple-react-validator";
import {
  VALIDATOR_MESSAGES,
  GOOGLE_RECAPTCHA_KEY,
} from "../../utils/constants";
import { alert } from "../../components/ui/alertSwal/alertSwal";
import ReCAPTCHA from "react-google-recaptcha";
import UnprotectedRoute from "../../components/ui/routes/UnprotectedRoute";
import { withRouter } from "next/router";
class RegisterLecteurPage extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  recaptchaRef = React.createRef();
  state = {
    sexe: "",
    name: "",
    lastname: "",
    pseudo: "",
    day: "",
    month: "",
    year: "",

    mail: "",
    password: "",
    repassword: "",
  };
  handleChange = (e) => {
    console.log("handle change");
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleChangeSexe = (sexe) => {
    this.setState({
      sexe,
    });
  };
  handleChangeDate = (e, id) => {
    this.setState({
      [id]: e.value,
    });
  };
  onSubmit = async () => {
    if (this.validator.allValid()) {
      const token = await this.recaptchaRef.current.executeAsync();
      this.recaptchaRef.current.reset();
      const {
        sexe,
        name,
        lastname,
        pseudo,
        day,
        month,
        year,
        mail,
        password,
      } = this.state;
      const user = {
        sexe,
        name,
        lastname,
        pseudo,
        date: year + "-" + month + "-" + day,
        mail,
        password,
        type: "Lecteur",
        token,
        lien: "http://localhost:3000/email_validation",
      };
      console.log("user", user);
      const userData = new FormData();

      for (const [key, value] of Object.entries(user)) {
        userData.append(key, value);
      }

      Axios.post("/user", userData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
        .then((res) => {
          console.log("res", res.data);
          if (res.data.message) {
            alert("Error", "oops..! " + res.data.message, "error");
          } else {
            alert(
              "Confirmation",
              "Veuillez consulter votre boite mail un message à été envoyé pour valider votre compte",
              "success"
            );
          }
        })
        .catch((err) => {
          alert("Error", "Oops..! " + err, "error");
        });
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  render() {
    return (
      <UnprotectedRoute {...this.props}>
        <div>
          <Head>
            <title>Register lecteur</title>
          </Head>
          <RegsterNavIcon
            icon={<AiOutlineArrowLeft size={30} />}
            to="/register"
          />
          <FormContainer style={{ width: "60%" }} title="LECTEUR">
            <Form className={styles["register-form"]}>
              <TitleForm key="titulaire" title={"Titulaire"} />
              <PersonalInformation
                state={this.state}
                handleChange={this.handleChange}
                handleChangeSexe={this.handleChangeSexe}
                handleChangeDate={this.handleChangeDate}
                validator={this.validator}
              />

              <TitleForm key="identifiant" title={"Identifiant"} />
              <IdentifierInformation
                state={this.state}
                handleChange={this.handleChange}
                validator={this.validator}
              />
              <ReCAPTCHA
                sitekey={GOOGLE_RECAPTCHA_KEY}
                size="invisible"
                ref={this.recaptchaRef}
              />
              <FormGroup className="text-center">
                <Button
                  color="blue"
                  className="rounded-12 px-5 font-weight-bold"
                  onClick={this.onSubmit}
                >
                  {"Enregistrer"}{" "}
                </Button>
              </FormGroup>
            </Form>
          </FormContainer>
        </div>
      </UnprotectedRoute>
    );
  }
}

export default withRouter(RegisterLecteurPage);
