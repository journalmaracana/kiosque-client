import React, { Component } from "react";
import { FormGroup, Label, Input, Row, Col, Button } from "reactstrap";
import { SmallContainer } from "../../components/ui/Containers";
import { RegsterNavIcon } from "../../components/Auth/Register";
import { AiOutlineArrowLeft } from "react-icons/ai";
import Axios from "../../utils/axios_config";
import SimpleReactValidator from "simple-react-validator";
import { VALIDATOR_MESSAGES } from "../../utils/constants";
import { withRouter } from "next/router";
import Head from "next/head";
import UnprotectedRoute from "../../components/ui/routes/UnprotectedRoute";
import { alert } from "../../components/ui/alertSwal/alertSwal";
class NewPasswordPage extends Component {
  validator = new SimpleReactValidator({
    autoForceUpdate: this,
    messages: VALIDATOR_MESSAGES,
  });
  state = {
    password: "",
    repassword: "",
  };

  handelChange = (e) => {
    if (e) {
      this.setState({
        [e.target.name]: e.target.value,
      });
    }
  };

  onSubmit = () => {
    if (this.validator.allValid()) {
      const token = this.props.router.query.id;
      const newPassword = {
        password: this.state.password,
        token: token,
      };
      Axios.post("reset/" + token + "/", newPassword)
        .then((res) => {
          alert(
            "Confirmation",
            "Votre mot de passe à été modifié avec succès",
            "success"
          );
          this.props.router.push("/login");
        })
        .catch((err) => alert("Error", "Oops..! " + err, "error"));
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  render() {
    return (
      // <UnprotectedRoute {...this.props}>
      <div>
        <Head>
          <title>New password</title>
        </Head>
        <RegsterNavIcon icon={<AiOutlineArrowLeft size={30} />} to="/login" />
        <br />
        <br />
        <br />
        <br />
        <SmallContainer className="bg-white shadow-sm mt-5 p-5 rounded-12">
          <h3 className="text-blue text-center"> Nouveau mot de passe </h3>
          <FormGroup className="mt-4">
            <Label className="text-blue font-size-14 font-weight-bold">
              {" "}
              Saisissez votre nouveau mot de passe:{" "}
            </Label>
            <Input
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handelChange}
            />
            <span style={{ color: "red" }}>
              {this.validator.message("password", this.state.password, [
                "required",
                {
                  regex: `^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$#%*+@!?&])[A-Za-z\\d$#%*+@!?&]{12,}$`,
                },
              ])}
            </span>
          </FormGroup>
          <FormGroup className="mt-3">
            <Label className="text-blue font-size-14 font-weight-bold">
              {" "}
              Confirmez votre nouveau mot de passe:{" "}
            </Label>
            <Input
              type="password"
              name="repassword"
              value={this.state.repassword}
              onChange={this.handelChange}
            />
            <span style={{ color: "red" }}>
              {this.validator.message(
                "repassword",
                this.state.repassword,
                "required|in:" + this.state.password + ""
              )}
            </span>
          </FormGroup>
          <FormGroup className="text-center mt-1">
            <Button
              color="blue"
              className="font-weight-bold"
              onClick={this.onSubmit}
            >
              {" "}
              {"Enregistrer"}{" "}
            </Button>
          </FormGroup>
        </SmallContainer>
      </div>
      // </UnprotectedRoute>
    );
  }
}

export default withRouter(NewPasswordPage);
