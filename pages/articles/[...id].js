import React, { Component } from "react";
import { Container, Button, Row, Col } from "reactstrap";
import Preloader  from "../../components/ui/Prealoder.js";
import { FiShare } from "react-icons/fi";
import Layout from "../../components/publicPages/Layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowLeft,
  faArrowRight,
  faEye,
  faClock,
} from "@fortawesome/free-solid-svg-icons";
import { Markup } from "interweave";
import {
  EmailShareButton,
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon,
  EmailIcon,
  WhatsappIcon,
  WhatsappShareButton,
  FacebookShareCount,
  ViberIcon,
  ViberShareButton,
  FacebookMessengerShareButton,
  FacebookMessengerIcon,
  LinkedinIcon,
  LinkedinShareButton,
} from "react-share";
import { FaExternalLinkSquareAlt, FaShareAltSquare } from "react-icons/fa";
import { SideBarAct } from "../../components/Sidebar";
import { withRouter } from "next/router";
import axios from "axios";
import { API_LINK } from "../../utils/constant";
import { isEmpty, webSite } from "../../utils/functions";
import moment from "moment";
import "moment/locale/ar"; // Pour la langue ar
import { TwitterTweetEmbed } from "react-twitter-embed";
import InstagramEmbed from "react-instagram-embed";
import Link from "next/link";
import styles from "../../styles/Article.module.css";

moment.suppressDeprecationWarnings = true;
moment.locale("fr");
class ArticlePage extends Component {
  constructor(props) {
    super(props);
    this.Datepublié = "";
    this.DateMAJ = "";

    this.state = {
      shareUrl: "",
      idArticle: "",

      article: {},
      loading: true,
      keyWord: [],
      tempReading: 0,

      // for language
      langLoaded: false,
    };
  }
  componentDidMount = () => {
    if (this.props.router.query?.id) {
      if(this.props.router.query?.id.length == 2){
      const titleArticle = this.props.router.query?.id.length
        ? this.props.router.query?.id[1]
        : "";
      this.getIdByName(titleArticle);
    }
    } else {
      //routing to 404
      this.props.router.push('/404')
      // this.setState({
      //   loading: false
      // })
    }
  };
  getIdByName = (title) => {
    this.setState({ loading: true });
    axios
      .get(API_LINK + "v1/articles/title/" + title)
      .then((response) => {
        this.setState(
          {
            idArticle: response.data.id,
            shareUrl: webSite + this.props.router.query?.id[0] +'/'+ this.props.router.query?.id[1],
          },
          () => {
            this.getArticle(response.data.id, "FR");
            this.addCountVisit(response.data.id);
            this.tempReading(response.data.id);
            //this.getArticlesByKeyWord(response.data.id);
          }
        );
      })
      .catch((err) => {
        console.log(err), this.setState({ loading: false });
      });
  };

  getArticle = (ID, Lang) => {
    this.setState({ loading: true });
    axios
      .get(API_LINK + "v1/articles/" + ID + "/" + Lang)
      .then((response) => {
        this.setState({
          article: response.data,
          loading: false,
        });
      })
      .catch((err) => {
        console.log(err), this.setState({ loading: false });
      });
  };
  addCountVisit = (ID) => {
    axios
      .put(API_LINK + "v1/countVisit/" + ID, {})
      .then()
      .catch((err) => {
        console.log(err);
      });
  };
  tempReading = (ID) => {
    axios
      .get(API_LINK + "v1/articles-read/" + ID)
      .then((response) => {
        this.setState({ tempReading: response.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  getArticlesByKeyWord = (ID) => {
    axios
      .get(API_LINK + "v1/keyWordByArticle/" + ID)
      .then((response) => {
        this.setState({
          keyWord: response.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  addCountPartage = async () => {
    let URL = API_LINK + "v1/countPartage/" + this.state.idArticle + "/";
    await axios
      .put(URL, {})
      .then((response) => {
        this.getArticle();
      })
      .catch((err) => console.log(err));
  };

  spanDate = () => {
    if (this.state.article.date && this.state.article.dateUpdate) {
      return (
        <>
          <span
            style={{
              fontFamily: "droid arabic naskh,verdana,Times,serif",
              color: "#333333",
            }}
          >
            {"Publié "}
          </span>
          <span className={`${styles["temps-pub"]} font-weight-bold`}>
            <b>{this.Datepublié}</b>
            {" | "}
          </span>
          <span
            style={{
              fontFamily: "droid arabic naskh,verdana,Times,serif",
              color: "#333333",
            }}
          >
            {"Dernière mise à jour "}
          </span>
          <span className={`${styles["temps-pub"]} font-weight-bold`}>
            <b>{this.DateMAJ}</b>

            {/* <b className="ml-3 text-dark"><FontAwesomeIcon icon={faEye} /> {this.state.article.countVisit}</b> */}
            <b className="ml-3 text-dark">
              <FontAwesomeIcon icon={faEye} /> {this.state.article.countVisit}
            </b>

            <b className="ml-3 text-dark">
              <FontAwesomeIcon icon={faClock} />{" "}
              {this.state.tempReading + " minute"}
            </b>
          </span>
        </>
      );
    } else {
      return (
        <>
          <span style={{ color: "#333333" }}>{"Publié "}</span>
          <span className={`${styles["temps-pub"]} font-weight-bold`}>
            <b>{this.Datepublié}</b>
          </span>
          {/* <b className="ml-3" style={{ color: '#333333' }}><FontAwesomeIcon icon={faEye} /> <span style={{ color: '#000000' }}> {this.state.article.countVisit} </span></b> */}
          <b className="ml-3 text-dark">
            <FontAwesomeIcon icon={faEye} /> {this.state.article.countVisit}
          </b>

          <b className="ml-3 text-dark">
            <FontAwesomeIcon icon={faClock} />{" "}
            {this.state.tempReading + " minute"}
          </b>
        </>
      );
    }
  };
  calculdatePublié = () => {
    if (this.state.article.date) {
      let datePublié = this.state.article.date;

      if (moment(datePublié).fromNow() === "il y a un jour") {
        this.Datepublié =
          "hier à " +
          moment(datePublié).format("HH") +
          "H" +
          moment(datePublié).format("MM");
      } else if (
        moment(datePublié).isBetween(
          moment().subtract(24, "hours"),
          moment().subtract(-24, "hours")
        )
      ) {
        this.Datepublié = moment(datePublié).fromNow();
      } else {
        this.Datepublié =
          "le " +
          moment(datePublié).format("DD/MM/YYYY") +
          " à " +
          moment(datePublié).format("HH") +
          "H" +
          moment(datePublié).format("MM");
      }

      //-------------------
      let dateMAJ = this.state.article.dateUpdate;
      if (moment(dateMAJ).fromNow() === "il y a un jour") {
        this.DateMAJ =
          "hier à " +
          moment(dateMAJ).format("HH") +
          "H" +
          moment(dateMAJ).format("MM");
      } else if (
        moment(dateMAJ).isBetween(
          moment().subtract(24, "hours"),
          moment().subtract(-24, "hours")
        )
      ) {
        this.DateMAJ = moment(dateMAJ).fromNow();
      } else {
        this.DateMAJ =
          moment(dateMAJ).format("DD/MM/YYYY") +
          " à " +
          moment(dateMAJ).format("HH") +
          "H" +
          moment(dateMAJ).format("MM");
      }
    }
  };
  renderArticle = () => {
    const { article } = this.state;
    const Video = this.state.article.content
      .split("<iframe")[1]
      ?.split("></iframe>")[0]
      ?.split('src="')[1]
      ?.split('" width')[0];
    const width = this.state.article.content
      .split("<iframe")[1]
      ?.split("></iframe>")[0]
      ?.split('width="')[1]
      ?.split('" height')[0];
    const height = this.state.article.content
      .split("<iframe")[1]
      ?.split("></iframe>")[0]
      ?.split('height="')[1]
      ?.split('" allowfullscreen')[0];
    let checkPositionTop =
      this.state.article.content.split("<p><iframe src=")[0] === "";
    let Check = this.state.article.content.includes("</iframe>");
    return (
      <>
        <div className={styles["noselect"]}>
          {/* <img src={API_LINK + this.state.article.image} alt="Article Image" className="image-article" /> */}
          <img
            src={API_LINK + article.image}
            width="100%"
            className="border-2 border-blue rounded-15"
          />

          <div className="float-left">
            <span
              style={{
                fontFamily: "droid arabic naskh,verdana,Times,serif",
                color: "#333333",
                fontSize: "15px",
              }}
            >
              {"Par "}
            </span>
            <Link href={"/auteur/" + article.author?.name + '-' + article.author?.lastname}>
            <span
              className="font-weight-bold"
              style={{
                fontFamily: "droid arabic naskh,verdana,Times,serif",
                color: "black",
                fontSize: "15px",
              }}
            >  
              {article.author
                ? article.author.lastname + " " + article.author.name
                : null}
            </span>
            </Link>
            <br />
            <div
              className={`${styles["temps-pub"]} col-md-12 col-sm-12  col-xs-12  px-0 py-0`}
            >
              {this.calculdatePublié()}
              {this.spanDate()}
            </div>
          </div>

          <div className={styles["text-article"]}>
            <br /> <br /> <br />
            <span
              style={{
                fontFamily: "droid arabic naskh,verdana,Times,serif",
                color: "#333333",
                justifyContent: "start",
                marginTop: "10px",
              }}
            >
              {/* <Markup content={article.content} /> */}
            </span>
          </div>

          {checkPositionTop ? (
            <div className="text-article ">
              <p
                style={{
                  fontFamily: "The Antiqua B,Georgia,Droid-serif,serif",
                  fontSize: "20px",
                  color: "#333333",
                  justifyContent: "start",
                  marginTop: "10px",
                  fontWeight: "bold",
                }}
              >
                <Markup content={this.state.article.resumer} />
              </p>

              {Check ? (
                <p className="text-center">
                  <iframe
                    src={Video}
                    width={width}
                    height={height}
                    allowfullscreen="allowfullscreen"
                  >
                    {" "}
                  </iframe>
                </p>
              ) : null}
              <span
                style={{
                  fontFamily: "droid arabic naskh,verdana,Times,serif",
                  color: "#333333",
                  justifyContent: "start",
                  marginTop: "10px",
                }}
              >
                <Markup content={this.state.article.content} />
              </span>
            </div>
          ) : (
            <div className="text-article ">
              <p
                style={{
                  fontFamily: "The Antiqua B,Georgia,Droid-serif,serif",
                  fontSize: "20px",
                  color: "#333333",
                  justifyContent: "start",
                  marginTop: "10px",
                  fontWeight: "bold",
                }}
              >
                <Markup content={this.state.article.resumer} />
              </p>
              <span
                style={{
                  fontFamily: "droid arabic naskh,verdana,Times,serif",
                  color: "#333333",
                  justifyContent: "start",
                  marginTop: "10px",
                }}
              >
                <Markup content={this.state.article.content} />
              </span>
              {Check ? (
                <p className="text-center">
                  <iframe
                    src={Video}
                    width={width}
                    height={height}
                    allowfullscreen="allowfullscreen"
                  >
                    {" "}
                  </iframe>
                </p>
              ) : null}
            </div>
          )}

          <TwitterTweetEmbed />
          <InstagramEmbed />
        </div>
        {this.renderSocialButtons()}
      </>
    );
  };
  renderSocialButtons = () => {
    // onClick keyord
    const s = this.state;
    const { article } = this.state;
    return (
      <>
        <div className="text-left float-left mr-4">
          <div className={`${styles["social-buttons"]} font-weight-bold`}>
            <span style={{ marginTop: "12px" }}>
              <Markup
                style={{
                  fontFamily: "droid arabic naskh,verdana,Times,serif",
                  fontSize: "16px",
                }}
                content={"PARTAGER CET ARTICLE"}
              />
            </span>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <div className={styles["social-buttons"]}>
              <FacebookShareButton
                url={s.shareUrl}
                onClick={this.addCountPartage.bind(this)}
              >
                <FacebookIcon size={32} borderRadius="20%" color="black" />
              </FacebookShareButton>
              &nbsp;&nbsp;
              <FacebookMessengerShareButton
                url={s.shareUrl}
                appId="280079756797149"
                className="Demo__some-network__share-button"
              >
                <FacebookMessengerIcon size={32} borderRadius="20%" />
              </FacebookMessengerShareButton>
              &nbsp;&nbsp;
              <WhatsappShareButton
                url={s.shareUrl}
                title={this.state.article.title}
                onClick={this.addCountPartage.bind(this)}
                separator=":: "
                className="Demo__some-network__share-button"
              >
                <WhatsappIcon size={32} borderRadius="20%" />
              </WhatsappShareButton>
              &nbsp;&nbsp;
              <ViberShareButton
                url={s.shareUrl}
                // title={this.state.article.title}
                className="Demo__some-network__share-button"
              >
                <ViberIcon size={32} borderRadius="20%" />
              </ViberShareButton>
              &nbsp;&nbsp;
              <TwitterShareButton
                url={s.shareUrl}
                onClick={this.addCountPartage.bind(this)}
              >
                <TwitterIcon size={32} borderRadius="20%" />
              </TwitterShareButton>
              &nbsp;&nbsp;
              <LinkedinShareButton
                url={s.shareUrl}
                className="Demo__some-network__share-button"
              >
                <LinkedinIcon size={32} borderRadius="20%" />
              </LinkedinShareButton>
              &nbsp;&nbsp;
              <EmailShareButton
                url={s.shareUrl}
                subject={this.state.article.title}
                //onClick={this.addCountPartage.bind(this)}
                className="Demo__some-network__share-button"
              >
                <img
                  src="https://img.icons8.com/ios-filled/50/000000/apple-mail.png"
                  width="38"
                  height="38"
                />
              </EmailShareButton>
              &nbsp;&nbsp;
              <FaShareAltSquare
                style={{ cursor: "pointer" }}
                onClick={this.myFunction.bind(this, s.shareUrl)}
                style={
                  ({ cursor: "pointer" }, { width: "38" }, { height: "37" })
                }
                size={30}
                borderRadius="20%"
              >
                <img src="https://img.icons8.com/carbon-copy/100/000000/link.png" />
              </FaShareAltSquare>
            </div>
          </div>

          {/* //------------------------ KeyWord ----------------------- */}
          <p>
            {article.keyWord.length ? (
              <>
                <br />
                <span style={{ marginTop: "12px", fontWeight: "bold" }}>
                  <Markup content={"DANS LE MÊME SUJET"} />
                  <br />
                  {article.keyWord.map((word) => {
                    return (
                      <Link href={"/tag/" + word}>
                        <Button
                          className="mr-2 "
                          style={{ marginTop: "10px" }}
                          outline
                          color="dark"
                        >
                          {word}
                        </Button>
                      </Link>
                    );
                  })}
                </span>
              </>
            ) : null}
          </p>
        </div>
      </>
    );
  };
  commentaire = () => {
    //plugin facebook notYET
    return this.state.article.isActive ? (
      <>
        <div>
          <Row className="margin0 mt-4 w-100 text-center">
            <Col md="12">
              <div
                class="fb-comments"
                data-href={API_LINK + this.props.location.pathname}
                data-width=""
                data-numposts="5"
              ></div>
            </Col>
          </Row>
        </div>
      </>
    ) : (
      <Row>
        <br />
        <Col md="12" className="text-center text-danger">
          <br />
          <p> {"Les commentaires de cet article sont désactivés"} </p>
        </Col>
      </Row>
    );
  };
  //-----
  myFunction = (URL) => {
    //coppy path
    var copyText = URL;
    navigator.clipboard.writeText(copyText);
  };
  render() {
    // state short
    const { article, loading } = this.state;
    // check language if loaded for remove preloader
    // if (!s.langLoaded) {
    //   return <Preloader />;
    // }

    return (
      <Layout>
        {!isEmpty(article) ? (
          <Container>
            {
              <FontAwesomeIcon
                style={{ cursor: "pointer" }}
                onClick={() => this.props.history.goBack()}
                icon={faArrowLeft}
              />
            }
            <Row>
              <Col md="8">
                <div className={styles["noselect"]}>
                  <div className={styles["tete-article"]}>
                    {
                      <p
                        style={{ fontSize: "25px" }}
                        className={`${styles["titre-article"]} text-left`}
                      >
                        <b>{article.title}</b>
                      </p>
                    }
                  </div>
                </div>
                <br />
                {this.renderArticle()}
                {/* {this.commentaire()} */}
              </Col>
              <Col md="4">
                <SideBarAct />
              </Col>
            </Row>
          </Container>
        ) : loading ? (
          <div className="text-center">
            <Preloader />
          </div>
        ) : (
          <>
            <div className="text-danger text-center font-weight-bold">
              {" "}
              {"Information indisponible"}{" "}
            </div>
          </>
        )}
      </Layout>
    );
  }
}

export default withRouter(ArticlePage);
