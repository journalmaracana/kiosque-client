import React, { useState, useEffect } from "react";
import LoginForm from "../../components/Auth/LoginForm";
import { FormContainer } from "../../components/ui/Containers";
import { RegsterNavIcon } from "../../components/Auth/Register";
import { AiOutlineArrowLeft } from "react-icons/ai";

import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { auth } from "../../store/actions/Auth";
import Preloader from "../../components/ui/Prealoder";
import UnprotectedRoute from "../../components/ui/routes/UnprotectedRoute";

const LoginPage = (props) => {
  const [mail, setMail] = useState("");
  const [password, setPassword] = useState("");
  const loading = useSelector((state) => state.auth.loading);

  const router = useRouter();
  const dispatch = useDispatch();

  const onSubmit = () => {
    const userData = { mail, password };
    dispatch(auth(userData, router));
  };

  if (loading) {
    return <Preloader />;
  }
  return (
    <UnprotectedRoute {...props}>
      <div>
        <RegsterNavIcon icon={<AiOutlineArrowLeft size={30} />} to="/" />
        <FormContainer>
          <LoginForm
            setMail={setMail}
            setPassword={setPassword}
            onSubmit={onSubmit}
          />
        </FormContainer>
      </div>
    </UnprotectedRoute>
  );
};

export default LoginPage;
