import React, { Component, Fragment } from "react";
import { Row, Col, Container } from "reactstrap";
import { API_LINK } from "../../utils/constant";
import { ImpulseSpinner } from "react-spinners-kit";
import Pagination from "../../components//ui/Pagination";
import ArticlePreviewMedium from "../../components/articlesPreview/mediumArticle";
import styles from "../../styles/Threads.module.css";
import Header from "../../components/publicPages/Header";
import NavBar from "../../components/publicPages/Navbar";
import { SideBarAct } from "../../components/Sidebar";
import Select from "react-select";
import axios from "axios";
import  Preloader  from "../../components/ui/Prealoder.js";
import { calculdatePublié } from "../../utils/functions";
class Threads extends Component {
  constructor() {
    super();
    this.customStyles = {
      option: (provided, state) => ({
        ...provided,
        borderBottom: "1px dotted pink",
        color: state.isSelected ? "white" : "blue",
        //padding: 20,
      }),
      singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = "opacity 300ms";

        return { ...provided, opacity, transition };
      },
    };
    this.state = {
      options: [],
      category: "Actualité",
      articles: [],
      loading: true,

      currentPage: 1,
      pagesCount: 0,
      pageSize: 10,
    };
  }
  componentDidMount = () => {
    this.getCategories();
  };
  //------------------ get -----------------
  getCategories = () => {
    axios
      .get(API_LINK + "v1/categories")
      .then((response) => {
        this.setState(
          {
            options: response.data.length
              ? response.data.map((cat) => {
                  return { value: cat._id, label: cat.entitled };
                })
              : [],
          },
          () => {
            if (this.state.options.length) {
              this.getActualiteByCat(
                "FR",
                this.state.options[0].value,
                this.state.currentPage,
                this.state.pageSize
              );
            } else {
              this.setState({
                loading: false,
              });
            }
          }
        );
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };
  getActualiteByCat = (Lang, ID, Index, Limit) => {
    axios
      .get(
        API_LINK +
          "v1/articlespagination/language/" +
          Lang +
          "/" +
          ID +
          "/" +
          Index +
          "/" +
          Limit
      )
      .then((response) => {
        this.setState({
          articles: response.data.article,
          currentPage: parseInt(response.data.currentPage),
          pagesCount: response.data.totalPages,
          loading: false,
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };

  //------------------ END get -----------------
  //-----------------handle change --------------------
  handleChangeSelect = (e) => {
    if (e) {
      this.setState(
        {
          category: e.value,
          loading:true,
        },
        () => {
          this.getActualiteByCat(
            "FR",
            this.state.category,
            this.state.currentPage,
            this.state.pageSize
          );
        }
      );
    }
  };
  handleClick = (index) => {
    // Handle Pagination
    if (index > 0) {
      this.setState(
        {
          currentPage: index,
          //currentPage: e,
        },
        () => {
          this.getActualiteByCat(
            "FR",
            this.state.category,
            this.state.currentPage,
            this.state.pageSize
          );
        }
      );
    }
    window.scrollTo(0, 0);
  };
  //----------------- END handle change --------------------

  render = () => {
    const { options, articles, loading } = this.state;
    return (
      <div>
        <div className="header">
          <Header />
          <NavBar />
        </div>
        <div className="threads-content">
          <Container>
            <Row className="text-center">
              <Col md="8">
                <a href="#">
                  <img src="img/pub.jpg" alt="" />
                </a>
              </Col>
              <Col md="4">
                <a href="#">
                  <img src="img/newmap.jpg" alt="" />
                </a>
              </Col>
            </Row>
            <Row>
              <Col md="8" className="partie-home-gauche">
                <div className={styles["titre-ligue1-div"]}>
                  <Row>
                    <Col md="4">ACTUALITÉ</Col>
                    {options.length ? (
                      <Col md="8" style={{ marginTop: "-20px" }}>
                        <Select
                          defaultValue={options[0]}
                          options={options}
                          onChange={this.handleChangeSelect}
                          styles={this.customStyles}
                        />
                      </Col>
                    ) : null}
                  </Row>
                </div>
                {articles.length ? (
                  articles.map((article) => {
                    return (
                      <ArticlePreviewMedium
                        link={"/articles/"+ article.category[0].label +'/' + article.title}
                        image={API_LINK + article.image}
                        name={
                          article.author?.type == "Auteur"
                            ? article.author.lastname +
                              " " +
                              article.author.name
                            : null
                        }
                        title={article.title}
                        category={
                          article.category.length
                            ? article.category[0].label
                            : ""
                        }
                        date={calculdatePublié(article.date)}
                        ID={article._id}
                      />
                    );
                  })
                ) : loading ? (
                  <div className="text-center">
                    <Preloader />
                  </div>
                ) : (
                  <>
                    <div className="text-danger text-center font-weight-bold">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  </>
                )}
              </Col>

              <Col md="4">
                <SideBarAct />
              </Col>
            </Row>
            <Row>
              <Col md="8">
                <div className="d-flex justify-content-center">
                  <Pagination
                    currentPage={this.state.currentPage}
                    pagesCount={this.state.pagesCount}
                    handleClick={this.handleClick}
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  };
}
export default Threads;
