import React, { useEffect } from "react";
import Link from "next/link";
import Layout from "../components/publicPages/Layout";
import { useRouter } from "next/router";

const Error = () => {
  const router = useRouter();

  useEffect(() => {
    setTimeout(() => {
      router.push("/");
    }, 3000);
  }, []);

  return (
    <Layout>
      <div className="text-center mt-4">
        <h1>404 - Page not found</h1>
        <Link href="/">
          <a className="btn btn-dark mt-2">BACK TO HOME</a>
        </Link>
      </div>
    </Layout>
  );
};

export default Error;
