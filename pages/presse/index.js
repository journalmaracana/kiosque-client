import React, { Component } from "react";
import { Row, Col, Container, Input, Button } from "reactstrap";
import { API_LINK } from "../../utils/constant";
import Pagination from "../../components/ui/Pagination";
import Header from "../../components/publicPages/Header";
import NavBar from "../../components/publicPages/Navbar";
import Select from "react-select";
import axios from "axios";
import Preloader from "../../components/ui/Prealoder.js";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { InputDatePicker } from "../../components/datePicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PressPreview from "../../components/Press";
import moment from "moment";

class Threads extends Component {
  constructor() {
    super();
    this.customStyles = {
      option: (provided, state) => ({
        ...provided,
        borderBottom: "1px dotted pink",
        color: state.isSelected ? "white" : "blue",
        //padding: 20,
      }),
      singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = "opacity 300ms";

        return { ...provided, opacity, transition };
      },
    };
    this.state = {
      options: [],
      category: "",
      date: "",
      title: "",
      press: [],
      loading: true,

      currentPage: 1,
      pagesCount: 0,
      pageSize: 20,
    };
  }
  componentDidMount = () => {
    this.getCateegory();
    this.getPress(this.state.currentPage, this.state.pageSize);
  };
  //------------------ get -----------------
  getCateegory = () => {
    axios
      .get(API_LINK + "v1/categoriesPress")
      .then((response) => {
        this.setState({
          options: response.data.length
            ? response.data.map((cat) => {
                return { value: cat._id, label: cat.entitled };
              })
            : [],
        });
      })
      .catch((err) => console.log(err));
  };
  getPress = (Index, Limit) => {
    this.setState({
      loading: true,
    });
    axios
      .get(API_LINK + "v1/presspagination/" + Index + "/" + Limit)
      .then((Response) => {
        let NamePub = new Set();
        let region = new Set();
        let allPress = [
          {
            NamePub: "",
            region: "",
            idRegion: "",
            dateParution: "",
            image: "",
            category: "",
            NameCat: "",
            numedition: "",
            laUne: "",
            idPress: "",
          },
        ];
        Response.data.press.map((press) => {
          NamePub.add(press.namePub.name),
            region.add(press.region.entitled),
            allPress.push({
              NamePub: press.namePub.name,
              region: press.region.entitled,
              idRegion: press.region._id,
              dateParution: press.parution,
              category: press.namePub.category._id,
              NameCat: press.namePub.category.entitled,
              numedition: press.numedition,
              laUne: press.laUne,
              idPress: press._id,
            });
        });
        // .find(allPress => NamePub.has(allPress.NamePub) && region.has(allPress.region))
        let allPressSorted = allPress.sort((a, b) =>
          b.dateParution.localeCompare(a.dateParution)
        );
        let pressFinal = [];
        allPressSorted.map((all, i) => {
          if (i == 0) {
            pressFinal.push(all);
          } else {
            if (
              (allPressSorted[i].NamePub !== allPressSorted[i - 1].NamePub ||
                allPressSorted[i].region !== allPressSorted[i - 1].region) &&
              !this.Exists(
                pressFinal,
                allPressSorted[i].NamePub,
                allPressSorted[i].region
              )
            ) {
              pressFinal.push(all);
            }
          }
        });
        this.setState({
          generalPress: pressFinal,
          press: pressFinal,
          currentPage: parseInt(Response.data.currentPage),
          pagesCount: Response.data.totalPages,
          loading: false,
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };
  Exists = (Arrayy, NamePub, region) => {
    return Arrayy.some(function (el) {
      return el.NamePub === NamePub && el.region === region;
    });
  };
  searchPress = (search) => {
    this.setState({
      loading: true,
      press: [],
    });
    axios
      .get(API_LINK + "v1/searchpress/" + search)
      .then((Response) => {
        // this.setState({
        //   press: Response.data.press,
        //   currentPage: parseInt(Response.data.currentPage),
        //   pagesCount: Response.data.totalPages,
        //   loading: false,
        // });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          loading: false,
        });
      });
  };
  getPressByCat = (category) => {
    this.setState({
      press: this.state.generalPress.length
        ? this.state.generalPress.filter((press) => press.category == category)
        : [],
      loading: false,
    });
  };
  //------------------ END get -----------------
  //-----------------handle change --------------------
  handleDateChange = (date) => {
    this.setState({
      date: date,
      title: "",
    });
  };
  handleTitleChange = (e) => {
    if (e) {
      this.setState({
        title: e.target.value,
        date: "",
      });
    }
  };
  handleClick = (index) => {
    // Handle Pagination
    if (index > 0) {
      this.setState(
        {
          currentPage: index,
          //currentPage: e,
        },
        () => {
          this.getPress(this.state.currentPage, this.state.pageSize);
        }
      );
    }
    window.scrollTo(0, 0);
  };
  handleChangeSelect = (e) => {
    this.setState({
      loading: true,
    });
    if (e) {
      this.setState(
        {
          category: e.value,
        },
        () => {
          this.getPressByCat(this.state.category);
        }
      );
    } else {
      this.setState({
        press: this.state.generalPress,
      });
      //get generale
    }
  };
  //----------------- END handle change --------------------

  render = () => {
    const { press, loading, title, date, options } = this.state;
    return (
      <div>
        <div className="header">
          <Header />
          <NavBar />
        </div>
        <div className="threads-content">
          <Container>
            <Row>
              <Col md="12" className="col-center">
                <form className="my-4">
                  <Row>
                    <Col md="4">
                      <Row>
                        <FontAwesomeIcon
                          icon={faCalendarAlt}
                          size="fa-sm fa-lg"
                          className="journal-search-icon"
                        />
                        &nbsp;&nbsp;
                        <Col md="10">
                          <InputDatePicker
                            onChange={this.handleDateChange}
                            selected={this.state.date}
                          />
                        </Col>
                      </Row>
                    </Col>
                    <Col md="3">
                      <Input
                        size="sm"
                        type="text"
                        id="title"
                        placeholder="Titre de presse"
                        onChange={this.handleTitleChange}
                        value={this.state.title}
                      />
                    </Col>
                    <Col md="5">
                      <Button
                        onClick={() => this.searchPress(title ? title : date)}
                        size="sm"
                        size="small"
                        style={{ background: "var(--dark)", width: "49%" }}
                        white
                        variant="contained"
                        color="primary"
                        disabled={!title && !date}
                        // endIcon={
                        //   <SearchIcon style={{ fontSize: "24px" }}></SearchIcon>
                        // }
                      >
                        {" "}
                        {"RECHERCHER"}{" "}
                      </Button>
                      <span style={{ padding: "1%" }}></span>
                      <Button
                        onClick={() => this.getPress(1, this.state.pageSize)}
                        size="sm"
                        size="small"
                        style={{ background: "var(--dark)", width: "49%" }}
                        white
                        variant="contained"
                        color="primary"
                        // endIcon={<CachedIcon style={{ fontSize: "24px" }} />}
                      >
                        {" "}
                        {"REINITIALISER"}{" "}
                      </Button>
                    </Col>
                  </Row>
                </form>
                <br />
                {options.length ? (
                  <Row>
                    <Col md="7"></Col>
                    <Col md="1">Categories</Col>

                    <Col md="4" style={{ marginTop: "-10px" }}>
                      <Select
                        //defaultValue={options[0]}
                        options={options}
                        onChange={this.handleChangeSelect}
                        styles={this.customStyles}
                        isClearable
                      />
                    </Col>
                  </Row>
                ) : null}
                <div className="pt-4">
                  <Row>
                    {press.length ? (
                      press.map((press) => {
                        if (press.NamePub)
                          return (
                            <>
                              <PressPreview
                                imageURL={API_LINK + press.laUne}
                                Titre={
                                  press.region !== "nationnal"
                                    ? press.NamePub + " - " + press.region
                                    : press.NamePub
                                }
                                date={moment(press.dateParution).format(
                                  "DD/MM/YYYY"
                                )}
                                link={
                                  "presse/" +
                                  press.NameCat +
                                  "/" +
                                  press.NamePub +
                                  "/" +
                                  press.numedition
                                }
                                idRegion={press.idRegion}
                                idPress={press.idPress}
                                // link={"presse/NameCat/NamePub/numedition"}
                              />
                            </>
                          );
                      })
                    ) : loading ? (
                      <div className="text-center">
                        <Preloader />
                      </div>
                    ) : (
                      <>
                        <div className="text-danger text-center font-weight-bold">
                          {" "}
                          {"Information indisponible"}{" "}
                        </div>
                      </>
                    )}
                  </Row>
                </div>
              </Col>
            </Row>
            <Row>
              <Col md="8">
                <div className="d-flex justify-content-center">
                  <Pagination
                    currentPage={this.state.currentPage}
                    pagesCount={this.state.pagesCount}
                    handleClick={this.handleClick}
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    );
  };
}
export default Threads;
