import React, { Component } from "react";
import { Row, Col, Container, Input, Button } from "reactstrap";
import { API_LINK } from "../../utils/constant";
import Pagination from "../../components/ui/Pagination";
import Header from "../../components/publicPages/Header";
import NavBar from "../../components/publicPages/Navbar";
import Select from "react-select";
import axios from "axios";
import Preloader from "../../components/ui/Prealoder.js";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { InputDatePicker } from "../../components/datePicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PressNumPreview from "../../components/Press/NumPress";
import PressPreview from "../../components/Press/index";
import { withRouter } from "next/router";
import moment from "moment";
import { isEmpty } from "../../utils/functions";

class PressNum extends Component {
  constructor() {
    super();
    this.customStyles = {
      option: (provided, state) => ({
        ...provided,
        borderBottom: "1px dotted pink",
        color: state.isSelected ? "white" : "blue",
        //padding: 20,
      }),
      singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = "opacity 300ms";

        return { ...provided, opacity, transition };
      },
    };
    this.state = {
      press: {},
      Allpress: [],
    };
  }
  componentDidMount = () => {
    if (this.props.router.query?.idPress) {
      this.getPess(this.props.router.query?.idPress);
    }
  };
  //------------------ get -----------------
  getPess = (ID) => {
    axios
      .get(API_LINK + "v1/pressByNumEdition/" + ID)
      .then((Response) => {
        this.setState(
          {
            press: Response.data,
          },
          () => {
            this.getPressesByRegionAndPub(
              this.state.press.namePub._id,
              this.state.press.region._id,
              "did"
            );
          }
        );
      })
      .catch((err) => console.log(err));
  };
  getPressesByRegionAndPub = (idPub, idRegion, etat) => {
    axios
      .get(API_LINK + "v1/pressByPubAndRegion/" + idPub + "/" + idRegion)
      .then((response) => {
        if (etat === "did") {
          this.setState(
            {
              testAllpress: response.data.sort((a, b) =>
                b.parution.localeCompare(a.parution)
              ),
            },
            () => {
              this.setState({
                Allpress: this.state.testAllpress.slice(1),
              });
            }
          );
        } else {
          this.setState(
            {
              press: {},
              testAllpress: [],
              Allpress: [],
            },
            () => {
              this.setState(
                {
                  testAllpress: response.data.sort((a, b) =>
                    b.parution.localeCompare(a.parution)
                  ),
                },
                () => {
                  this.setState({
                    press: this.state.testAllpress[0],
                    Allpress: this.state.testAllpress.slice(1),
                  });
                }
              );
            }
          );
        }
      })
      .catch((err) => console.log(err));
  };
  //------------------ END get -----------------
  //-----------------handle change --------------------

  //----------------- END handle change --------------------

  render = () => {
    const { Allpress, press } = this.state;
    return (
      <div>
        <div className="header">
          <Header />
          <NavBar />
        </div>
        <div className="threads-content">
          <Container>
            {!isEmpty(press) && press && Allpress.length ? (
              <>
                <Row>
                  <Col md="4"></Col>
                  {!isEmpty(press) && press ? (
                    <Col md="4">
                      <PressNumPreview
                        press={press}
                        getPressesByRegionAndPub={this.getPressesByRegionAndPub}
                        namePub={press.namePub._id}
                      />
                    </Col>
                  ) : (
                    <div className="text-danger text-center font-weight-bold mt-4">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  )}

                  <Col md="4"></Col>
                </Row>
                <br />
                <Row>
                  <Col md="4">
                    <hr />
                  </Col>
                  <Col md="4" className="text-center">
                    Les archives de ce titre
                  </Col>
                  <Col md="4">
                    <hr />
                  </Col>
                </Row>
                <Row>
                  {Allpress.length ? (
                    Allpress.map((press) => {
                      return (
                        <PressPreview
                          imageURL={API_LINK + press.laUne}
                          Titre={press.namePub.name}
                          date={moment(press.parution).format("DD-MM-YYYY")}
                        />
                      );
                    })
                  ) : (
                    <div className="text-danger text-center font-weight-bold mt-4">
                      {" "}
                      {"Information indisponible"}{" "}
                    </div>
                  )}
                </Row>
              </>
            ) : (
              <div className="text-danger text-center font-weight-bold mt-4">
                {" "}
                {"Liste des archives est vide"}{" "}
              </div>
            )}
          </Container>
        </div>
      </div>
    );
  };
}
export default withRouter(PressNum);
