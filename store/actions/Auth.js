import * as actionTypes from "./actionTypes";
import Axios from "../../utils/axios_config";
import Cookies from "js-cookie";
import { alert } from "../../components/ui/alertSwal/alertSwal";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START,
  };
};

export const authSuccess = (token, userId, type) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token,
    userId,
    type,
  };
};
export const authFail = (error) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error,
  };
};
export const logout = (router) => {
  console.log('router',router)
  Cookies.remove("_token");
  Cookies.remove("_id");
  Cookies.remove("_type");

  router.replace("/login");

  return {
    type: actionTypes.AUTH_LOGOUT,
  };
};
export const auth = (idetifiant, router) => {
  return (dispatch) => {
    dispatch(authStart());

    Axios.post("/login", idetifiant)
      .then(({ data }) => {
        if (data.message === "utilisateur non valide") {
          alert("Error", "Oops..! " + data.message, "error");
          dispatch(authFail("compte invalide"));
        } else {
          Cookies.set("_token", data.token);
          Cookies.set("_id", data.user._id);
          Cookies.set("_type", data.user.type);

          dispatch(authSuccess(data.token, data.user._id, data.user.type));
          router.replace("/dashbord");
        }
      })
      .catch((err) => {
        if (err.response.status === 404) {
          alert("Error", "Oops..! " + err.response.data.message, "error");
        } else {
          alert("Error", "Oops..! " + err, "error");
        }
        dispatch(authFail(err));
      });
  };
};

export const autoSignin = () => {
  return (dispatch) => {
    const token = Cookies.get("_token");
    if (!token) {
      dispatch(logout());
    } else {
      const userId = Cookies.get("_id");
      const type = Cookies.get("_type");
      dispatch(authSuccess(token, userId, type));
    }
  };
};

export const validateCompte = (router, id) => {
  console.log("validat router", router);
  console.log("id", id);
  return (dispatch) => {
    dispatch(authStart());
    Axios.put("/confirm/" + id, { isvalid: true })
      .then(({ data }) => {
        Cookies.set("_token", data.token);
        Cookies.set("_id", data.user._id);
        Cookies.set("_type", data.user.type);

        dispatch(authSuccess(data.token, data.user._id, data.user.type));
        router.replace("/");
      })
      .catch((err) => {
        alert("Error", "Oops..! " + err, "error");
        dispatch(authFail(err));
      });
  };
};
